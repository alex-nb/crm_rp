<?php
/*
 * Родительский класс контроллеров.
 */

abstract class Controller {
    protected $view;
    protected $userAuth;
    protected $newPass;
    protected $premissions;
    protected $code;
    protected $create;
    protected $read;
    protected $update;
    protected $delete;

    //создание представления с шаблоном
    function __construct() {
        $auth = new UserAuth;
        $this->userAuth = $auth->GetUser();
        $this->premissions = GetPremissions($this->userAuth['id']);
        $this->view = new View();
        $this->read = 1;
        $this->update = 2;
        $this->delete = 3;
        $this->create = 4;
    }

// действие (action), вызываемое по умолчанию
    abstract function index();
}
