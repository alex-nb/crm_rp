<?php
# подключение к базе
# https://habrahabr.ru/post/137664/
# http://phpfaq.ru/pdo
# настройки

function LogToFile($text){
    $f = fopen("core_log.txt", 'a+') or die("Totally fuck up!");
    fwrite($f, "{$text}\n");
    fclose($f);
}#LogToFile

function StrGen($length = 16){
    $chars = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789";
    $numChars = strlen($chars);
    $string = '';
    for ($i = 0; $i < $length; $i++) {
      $string .= substr($chars, rand(1, $numChars) - 1, 1);
    }
    return $string;
}#StrGen

function ToNum($v){
    return (int)$v;
}

function MakePass($password) {
      $salt='64b0a5ae92b498f';
      return  hash('sha512', $password.$salt);
}
  
function CheckAction ($object_code, $action_code, $premissions) {
    foreach ($premissions as $premiss) {
        if (isset($premiss["objects"][$object_code]["actions"][$action_code])) {
            return true;
        }
    }
    return false;
}

function GetPremissions($id) {
    //file_put_contents("text.txt", "IN ACTION \n", FILE_APPEND);
    $db = new DB();
    $user_id = $id;

    $premissions = array();
    $diss = array();
    //выбираем все роли, которые присвоенны пользователю с их наименованиями
    $ROLES = $db->Get("SELECT `acl_premission_user`.`role_id`, `acl_roles`.`title` FROM crm_rp.acl_premission_user, crm_rp.acl_roles "
            . "WHERE `acl_premission_user`.`user_id` = ".$user_id." AND `acl_premission_user`.`deleted`= 0 "
            . "AND `acl_premission_user`.`role_id` = `acl_roles`.`id`");
    //выбираем отдельные ограничения наложенные на данного пользователя
    $DISSALLOW = $db->Get("SELECT `acl_dissallow`.`object_code`, `acl_object`.`title` as obj_title, `acl_action`.`title` as act_title, `acl_dissallow`.`action_code` "
            . "FROM `acl_dissallow`, `acl_object`, `acl_action` WHERE `acl_dissallow`.`user_id` = ".$user_id." AND "
            . "`acl_object`.`id` = `acl_dissallow`.`object_code` AND `acl_action`.`id`= `acl_dissallow`.`action_code` AND `acl_dissallow`.`deleted`= 0");
    //записываем их в массив для удобства
    while ($dissal = $DISSALLOW->fetch()) {
        //file_put_contents("text.txt", "IN DISSALLOW OC: ".$dissal->object_code."\n", FILE_APPEND);
        //file_put_contents("text.txt", "IN DISSALLOW AC: ".$dissal->action_code."\n", FILE_APPEND);
        $diss[$dissal->object_code] = $dissal->action_code;
        $premissions['dissallow'][$dissal->object_code]['title'] = $dissal->obj_title;
        //file_put_contents("text.txt", "IN DISSALLOW OT: ".$premissions['dissallow'][$dissal->object_code]['title']."\n", FILE_APPEND);
        $premissions['dissallow'][$dissal->object_code]['actions'][$dissal->action_code]['title'] = $dissal->act_title;
        //file_put_contents("text.txt", "IN DISSALLOW AT: ".$premissions['dissallow'][$dissal->object_code]['actions'][$dissal->action_code]['title']."\n", FILE_APPEND);
    }
    //проходимся по ролям пользователя
    while($rol = $ROLES->fetch()) {
        $PARRENTS = $db->Get("SELECT `acl_child_roles`.`parrent_code`, `acl_roles`.`title` FROM crm_rp.acl_child_roles, crm_rp.acl_roles  "
                . "WHERE `acl_child_roles`.`childe_code` = ".$rol->role_id." AND `acl_child_roles`.`deleted`=0"
                . " AND `acl_roles`.`id` = `acl_child_roles`.`parrent_code`");
        while ($parr = $PARRENTS->fetch()) {
            $premissions[$parr->parrent_code]['role_id'] = $parr->parrent_code;
            $premissions[$parr->parrent_code]['role_title'] = $parr->title;
            $ALLOW = $db->Get("SELECT `acl_premission`.`object_code`, `acl_premission`.`action_code`, `acl_object`.`title` as title_obj, `acl_action`.`title` as title_act  "
                . "FROM crm_rp.acl_premission, crm_rp.acl_object, crm_rp.acl_action WHERE `acl_premission`.`role_code` = ".$parr->parrent_code." AND `acl_premission`.`deleted` = 0 "
                . "AND `acl_premission`.`object_code` = `acl_object`.`id` AND `acl_premission`.`action_code` = `acl_action`.`id`");
            while($obj = $ALLOW->fetch()) {
        //проверяем есть ли данное право в ограничениях, если да, не записываем в массив, если нет - записываем данные 
                if (isset($diss[$obj->object_code]) && $diss[$obj->object_code] == $obj->action_code) {
                    continue;
                }
                else {
                    $premissions[$parr->parrent_code]['objects'][$obj->object_code]['obj_id'] = $obj->object_code;
                    $premissions[$parr->parrent_code]['objects'][$obj->object_code]['obj_title'] = $obj->title_obj;
                    $premissions[$parr->parrent_code]['objects'][$obj->object_code]['actions'][$obj->action_code]['act_id'] = $obj->action_code;
                    $premissions[$parr->parrent_code]['objects'][$obj->object_code]['actions'][$obj->action_code]['act_title'] = $obj->title_act;
                }
            }
        }
    //записываем в массив ИД роли и ее наименование
        $premissions[$rol->role_id]['role_id'] = $rol->role_id;
        $premissions[$rol->role_id]['role_title'] = $rol->title;
    //выбираем все права пользователя (объект доступа + действие с ним) с наименованиями
        $ALLOW = $db->Get("SELECT `acl_premission`.`object_code`, `acl_premission`.`action_code`, `acl_object`.`title` as title_obj, `acl_action`.`title` as title_act  "
                . "FROM crm_rp.acl_premission, crm_rp.acl_object, crm_rp.acl_action WHERE `acl_premission`.`role_code` = ".$rol->role_id." AND `acl_premission`.`deleted` = 0 "
                . "AND `acl_premission`.`object_code` = `acl_object`.`id` AND `acl_premission`.`action_code` = `acl_action`.`id`");
        while($obj = $ALLOW->fetch()) {
    //проверяем есть ли данное право в ограничениях, если да, не записываем в массив, если нет - записываем данные 
            if (isset($diss[$obj->object_code]) && $diss[$obj->object_code] == $obj->action_code) {
                continue;
            }
            else {
                $premissions[$rol->role_id]['objects'][$obj->object_code]['obj_id'] = $obj->object_code;
                $premissions[$rol->role_id]['objects'][$obj->object_code]['obj_title'] = $obj->title_obj;
                $premissions[$rol->role_id]['objects'][$obj->object_code]['actions'][$obj->action_code]['act_id'] = $obj->action_code;
                $premissions[$rol->role_id]['objects'][$obj->object_code]['actions'][$obj->action_code]['act_title'] = $obj->title_act;
            }
        }
    }
    return $premissions;
}

function GetUsersByRole($id) {
    $db = new DB();
    $role_id = $id;
    $users = array();
    $USER = $db->Get("SELECT `user`.`id`, `user`.`last_name`, `user`.`first_name`, `user`.`second_name` FROM crm_rp.user, crm_rp.acl_premission_user "
            . "WHERE `acl_premission_user`.`role_id` = ".$role_id." AND `acl_premission_user`.`deleted`= 0  AND `user`.`deleted`= 0 "
            . "AND `user`.`id` = `acl_premission_user`.`user_id`");

    while($row = $USER->fetch()) {
      $users[$row->id]['id'] = $row->id;
      $fio = $row->last_name." ";
      $fio .= $row->first_name." ";
      $fio .= $row->second_name;
      $users[$row->id]['fio'] = $fio;
    }
    return $users;
}  

class DB {
    private function Connect(){
        $CONF['DB_Driver']    = 'mysql';
        $CONF['DB_Path']      = '/Bases';
        $CONF['DB_Host']      = '192.168.1.221';
        $CONF['DB_Port']      = '3306';
        $CONF['DB_Name']      = 'crm_rp';
        $CONF['DB_Login']     = 'testuser';
        $CONF['DB_Password']  = 'password';
        $CONF['TimeZone']     = '+3';
        try {
          $db = new PDO("{$CONF['DB_Driver']}:host={$CONF['DB_Host']};dbname={$CONF['DB_Name']};charset=UTF8", $CONF['DB_Login'], $CONF['DB_Password'], [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
        }catch(PDOException $e){
          LogToFile($e->getMessage());
        }#try/catch
        return $db;
    }#Connect
  
    private function ConnectSchema(){
        $CONF['DB_Driver']    = 'mysql';
        $CONF['DB_Path']      = '/Bases';
        $CONF['DB_Host']      = '192.168.1.221';
        $CONF['DB_Port']      = '3306';
        $CONF['DB_Name']      = 'information_schema';
        $CONF['DB_Login']     = 'testuser';
        $CONF['DB_Password']  = 'password';
        $CONF['TimeZone']     = '+3';
        try {
          $db = new PDO("{$CONF['DB_Driver']}:host={$CONF['DB_Host']};dbname={$CONF['DB_Name']};charset=UTF8", $CONF['DB_Login'], $CONF['DB_Password'], [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
        }catch(PDOException $e){
          LogToFile($e->getMessage());
        }#try/catch
        return $db;
    }#Connect

    public function Write ($sql, $values){ #($sql,...$values)
    # $db->write('INSERT INTO `users` (`login`, `password`) VALUES (?,?)', 'cheburek', 'Cheburek');
    # Настоятельно прошу не посылать "готовый" запрос с вложеными переменными как значение, так как это открытая дверь для зловреда
        try {
            $db = $this->Connect();
            $prepare = $db->prepare($sql);
            $prepare->execute($values);
            $id = $db->lastInsertId();
            if (!empty($id)) {
                return $id;
            }
            else {
                return true;
            }
        } catch (Exception $ex) {
            if ($ex->getCode() == 23000){
                $message = 'Запись или поле уже существует';
            }
            else {
                $message = $ex->getMessage();
            }
            return $message;
        }
    }#Write
  
    public function Get ($sql){ #($sql,...$values)
    # $db->Get('SELECT ID, username, password FROM crm_rp.user');
    # Настоятельно прошу не посылать "готовый" запрос с вложеными переменными как значение, так как это открытая дверь для зловреда
    # http://php.net/manual/ru/pdostatement.execute.php
        try {
          $STH = $this->Connect()->query($sql);
          $STH->setFetchMode(PDO::FETCH_OBJ);
          return $STH;
        } catch (Exception $ex) {
          $message = $ex->getMessage();
          return $message;
        }
    }#Get

    public function GetForeignKey ($table_name) {
        $tables = $this->ConnectSchema()->query("SELECT `INNODB_SYS_FOREIGN`.`ID`, `INNODB_SYS_FOREIGN`.`FOR_NAME`, `INNODB_SYS_FOREIGN_COLS`.`FOR_COL_NAME`  "
                . "FROM `INNODB_SYS_FOREIGN`, `INNODB_SYS_FOREIGN_COLS` WHERE `INNODB_SYS_FOREIGN`.`REF_NAME`='crm_rp/".$table_name."' "
                . "AND `INNODB_SYS_FOREIGN`.`ID` = `INNODB_SYS_FOREIGN_COLS`.`ID`");
        $tables->setFetchMode(PDO::FETCH_OBJ);
        return $tables;
    }
            
    function __destruct(){$this->db = null;}
}#DB

class UserAuth {
    protected $CookieLifeTime=28800;
    protected $User=NULL;
    public $Out='';
    
    function __construct(){
      if( (!empty($_POST['login']) and !empty($_POST['password'])) || (isset($_COOKIE["token"])) ){
        if(!empty($_POST['login'])){
          $this->User = $this->GetUserInfo(htmlspecialchars($_POST['login']));
          if(isset($this->User['pass'])){
            $pass = MakePass(htmlspecialchars($_POST['password']));
            if($pass==$this->User['pass']){
              $token = StrGen(256);
              $db = new DB();
              $v[0] = $token; $v[1]=$this->User['id'];
              $db->write('UPDATE user SET token =? WHERE `id`=?', $v); # $token пишем в базу пользователя
              setcookie('token',$token,time()+$this->CookieLifeTime);
              header("Location: {$_SERVER['HTTP_REFERER']}");
            }else{
              $this->Out .= 'WRONG PASSWORD';
            }
          }else{
            $this->Out .= 'YOU ARE NOT EXIST';
          }
        }else{
          $this->User = $this->GetUserInfo($_COOKIE["token"]);
        }
      }
    }#__construct

    function GetUserInfo($in){
      $db = new DB();
      $in = htmlspecialchars($in);
      $STH = $db->Get("SELECT `id`, `username`, `password`, `token`, `first_name` FROM crm_rp.user WHERE `username` = \"{$in}\" OR `token` = \"{$in}\" LIMIT 1");
      while($row = $STH->fetch()) {
        $this->User['id']     = $row->id;
        $this->User['login']  = $row->username;
        $this->User['pass']   = $row->password;
        $this->User['token']    = $row->token;
        $this->User['f_name'] = $row->first_name;
      }
      if(!empty($this->User)){
        return $this->User;
      }else{
        return false;
      }
    }#GetUserInfo

    function AuthForm(){#{$_SERVER['REQUEST_URI']}
        return "<!DOCTYPE html><html><head>
                <meta charset='UTF-8'>
                <title>Авторизация</title>
                <script type='text/javascript' src='/core/web/js/jquery.min.js'></script>
                <link rel='stylesheet' type='text/css' href='/core/web/css/custom.css'/>
                <link rel='stylesheet' type='text/css' href='/core/web/css/bootstrap.min.css'/>
                <link rel='stylesheet' type='text/css' href='/core/web/css/bootstrap-theme.min.css'/>
                <script type='text/javascript' src='/core/web/js/bootstrap.min.js'></script>
              </head><body class='background'>
                <form action='{$_SERVER['PHP_SELF']}'  class='' method='POST' name='AuthForm' >
                  <div class='block'>
                    <h3>Авторизуйтесь, сударь! Введите логин и пароль.</h3>
                    <div class='input-group'>
                      <span class='input-group-addon'><span class='glyphicon glyphicon-user'></span></span>
                      <input type='text' id='login' name ='login' class='form-control' placeholder='Логин' required autofocus />
                    </div>
                    <br/>
                    <div class='input-group'>
                      <span class='input-group-addon'><span class='glyphicon glyphicon-lock'></span></span>
                      <input type='password' id='password' name ='password' class='form-control' placeholder='Пароль' required />
                    </div>
                    <br/>
                    <input type='submit' class='btn btn-labeled btn-primary' value='Войти'/>
                  </div>
                </form>
              </body></html>";
    }#AuthForm

    function GetUser(){
        $db = new DB();
        $STH = $db->Get("SELECT `role_id` FROM `crm_rp`.`acl_premission_user`  WHERE `user_id`=".$this->User['id']." AND `deleted` = 0");
        if (!is_string($STH)) {
            while($row = $STH->fetch()) {
                $this->User['role'][] = $row->role_id;
            }
        }
        
        unset($this->User['login']);
        unset($this->User['pass']);
        return $this->User;
    }
}#UserAuth

