<?php

/**
 * Содержит метод для выборки данных, перекрывающийся в классах потомках.
 * Родительский класс моделей.
 * @author aleks
 */
abstract class Model {

    protected $db;
    protected $table;
    protected $userAuth;
	
    public function __construct() {
        // объект бд коннекта
        $this->db = new DB();

        // имя таблицы
        $modelName = get_class($this);
        $tableName = substr($modelName, 0, -5);
        $this->table = $tableName;
        
        //залогининный пользователь
        $auth = new UserAuth;
        $this->userAuth = $auth->GetUser();
    }
    
    abstract function fieldsTable(); //все поля таблицы
    abstract function checkFields(); //поля, по которым идет проверка удаленных записей при создании новой
    abstract function constrainsTable(); //внешние ключи в формате table=>id (где используется текущая таблица)

    // получить имя таблицы
    public function GetTableName() {
        return $this->table;
    }
    
    // запись в базу данных
    public function Save() {
        $arrayAllFields = array_keys($this->fieldsTable()); //получаем все поля в таблице
        $arraySetFields = array(); //массив для наименования полей, которые вставляем
        $arrayData = array(); //массив для значений полей, которые вставляем    
        $arrayCheckFields = array_keys($this->checkFields()); //получаем наименования полей для сравнения
//проверка старых записей с аналогичными заполненными полями в удаленке
        $flag = false;
        $deleted = $this->GetDeleted(); //получаем удаленные записи из таблицы
        if (!empty($deleted)) {
            foreach ($deleted as $del_row) {
                foreach ($arrayAllFields as $field){
                    if(!empty($this->$field)){ //если пользователь дал нам значения для данного поля
                        if (in_array($field, $arrayCheckFields)) { //если текущее поле находитися в массиве для сравнения
                            if ($del_row->$field === $this->$field) { //если значение поля совпадает с текущим
                                $id_old = $del_row->id;
                                $flag = true;
                             }  
                            else {$flag = false;} //если значение какого-то поля не совпадает с текущим
                        }
                    }               
                }
                if ($flag) break; //если мы уже нашли запись, выходим из цикла
            }
            if ($flag) { //если найдена такая же запись в удаленке, удаляем навсегда
                $this->DeleteRowForever($id_old);
            }
        } 
        foreach($arrayAllFields as $field){
            if(!empty($this->$field)){ //если пользователь дал нам значения для данного поля
                $arraySetFields[] = $field;
                $arrayData[] = $this->$field;
            }
        }
        $forQueryFields =  implode(', ', $arraySetFields);
        $rangePlace = array_fill(0, count($arraySetFields), '?');
        $forQueryPlace = implode(', ', $rangePlace);
        $result = $this->db->write("INSERT INTO $this->table ($forQueryFields) VALUES ($forQueryPlace)", $arrayData);
        $userAuth = $this->userAuth;
        $date = date('Y-m-d\TH:i:sP');
        $values = [$userAuth['id'], $this->table, 'create', $result, $date];
        $logs = $this->db->write("INSERT INTO `logs` (`id_user`, `name_table`, `action`, `id_in_table`, `time`) VALUES (?, ?, ?, ?, ?)", $values);
        if (is_string($result) && strlen($result)>10) {
            $locate = $_SERVER['HTTP_REFERER'];
            echo "<script>alert('".$result."');window.location.href='".$locate."'; console.log('OOPS');</script>";
        }
        else {
            return $result;
        }
    }
    
    // обновление записи. Происходит по ID
    public function Update($id){
        $arrayAllFields = array_keys($this->fieldsTable());
        $arraySetFields = array();
        $arrayForSet = array();
        $old_value = array();
        foreach($arrayAllFields as $field){
            if(!empty($this->$field) && strtoupper($field) != 'ID'){
                $arraySetFields[] = $field . ' = ? ';
                $arrayForSet[] = $this->$field;
                $result = $this->db->get("SELECT ".$field." FROM ".$this->table." WHERE id=".$id);
                foreach ($result as $row) {
                    $old_value[] = $row->$field;
                }
            }
        }
        if(!isset($arrayForSet) OR empty($arrayForSet)){
            echo "Array data table `$this->table` empty!";
            exit;
        }
        $arrayForSet[]= $id;
        $strForSet = implode(', ', $arraySetFields);
        $result = $this->db->write("UPDATE $this->table SET $strForSet WHERE `id` = ?", $arrayForSet);
        $userAuth = $this->userAuth;
        $date = date('Y-m-d\TH:i:sP');
        foreach ($old_value as $row) {
            $values = [$userAuth['id'], $this->table, 'update', $id, $row, $date];
            $logs = $this->db->write("INSERT INTO `logs` (`id_user`, `name_table`, `action`, `id_in_table`, `old_value`, `time`) VALUES (?, ?, ?, ?, ?, ?)", $values);
        }
        
         if (is_string($result)) {
            $locate = $_SERVER['HTTP_REFERER'];
            echo "<script>alert('".$result."');window.location.href='".$locate."';</script>";
        }
        else {
            return $result;
        }
    }
    
    // удаление строки из базы данных
    public function DeleteRow($id, string $condition = NULL) {
        if ($condition != NULL) {
            $result = $this->db->write("DELETE FROM $this->table WHERE ".$condition, NULL);
            if (is_string($result)) {
                $locate = $_SERVER['HTTP_REFERER'];
                echo "<script>alert('".$result."');window.location.href='".$locate."';</script>";
            }
            else {
                return true;
            }
        }
        else {
            if ($this->CheckOtherTable($id, 'from_class', $this->constrainsTable())) {
                $array = [0=>$id];
                $result = $this->db->write("UPDATE $this->table SET `deleted` = '1'  WHERE `id` = ?", $array);
                $userAuth = $this->userAuth;
                $date = date('Y-m-d\TH:i:sP');
                $values = [$userAuth['id'], $this->table, 'delete', $id, $date];
                $logs = $this->db->write("INSERT INTO `logs` (`id_user`, `name_table`, `action`, `id_in_table`, `time`) VALUES (?, ?, ?, ?, ?)", $values); 
                if (is_string($result)) {
                    $locate = $_SERVER['HTTP_REFERER'];
                    echo "<script>alert('".$result."');window.location.href='".$locate."';</script>";
                }
                else {
                    return true;
                }      
            }
            else {
                return false;
            }
        }
    }
    
    public function DeleteRowForever($id) {
        $array = [0=>$id];
        $result = $this->db->write("DELETE FROM $this->table WHERE `id` = ?", $array); 
        $userAuth = $this->userAuth;
        $date = date('Y-m-d\TH:i:sP');
        $values = [$userAuth['id'], $this->table, 'delete_forever', $id, $date];
        $logs = $this->db->write("INSERT INTO `logs` (`id_user`, `name_table`, `action`, `id_in_table`, `time`) VALUES (?, ?, ?, ?, ?)", $values); 
         if (is_string($result)) {
            $locate = $_SERVER['HTTP_REFERER'];
            echo "<script>alert('".$result."');window.location.href='".$locate."';</script>";
        }
        else {
            return $result;
        }
    }
    
    // получить все записи
    public function GetAllRows(string $condition = null, $flag = TRUE) {
        $dop = '';
        if ($flag) {
            $dop = 'deleted = 0';
        }
        $arrayAllFields = array_keys($this->fieldsTable());
        $atribute  = implode(" , ",$arrayAllFields);
        $select = 'SELECT '.$atribute.' FROM '.$this->table.' WHERE '.$dop;
        if (!empty($condition)) {
            $select .= " AND ".$condition;
        }
        $result = $this->db->get($select);
         if (is_string($result)) {
            $locate = $_SERVER['HTTP_REFERER'];
            echo "<script>alert('".$result."');window.location.href='".$locate."';</script>";
        }
        else {
            return $result;
        }
    }
 
    // получить запись по id
    public function GetRowById($id=NULL){
        if ($id !== NULL) {
            $arrayAllFields = array_keys($this->fieldsTable());
            $atribute  = implode(" , ",$arrayAllFields);
            $select = 'SELECT '.$atribute.' FROM '.$this->table.' WHERE id='.$id;    
            $result = $this->db->get($select); 
        }
        else {
            $result = $this->GetAllRows();
        }
         if (is_string($result)) {
            $locate = $_SERVER['HTTP_REFERER'];
            echo "<script>alert('".$result."');window.location.href='".$locate."';</script>";
        }
        else {
            return $result;
        }
    }
    
   // получить удаленные записи
    public function GetDeleted(){
        $arrayAllFields = array_keys($this->fieldsTable());
        $atribute  = implode(" , ",$arrayAllFields);
        $select = 'SELECT '.$atribute.' FROM '.$this->table.' WHERE deleted = 1';
        $result = $this->db->get($select); 
         if (is_string($result)) {
            $locate = $_SERVER['HTTP_REFERER'];
            echo "<script>alert('".$result."');window.location.href='".$locate."';</script>";
        }
        else {
            return $result;
        }
    }
    
    function CheckOtherTable($id, $method, $all_tables = NULL) {
        if ($method === "from_class") {
            if ($all_tables !== NULL) {
                foreach ($all_tables as $table=>$key){
                    //file_put_contents("text.txt", $table."\n", FILE_APPEND);
                    //file_put_contents("text.txt", $key."\n", FILE_APPEND);
                    $select = 'SELECT `id` FROM '.$table.' WHERE '.$key.'='.$id;
                    //file_put_contents("text.txt", $select."\n", FILE_APPEND);
                    $result = $this->db->Get($select);
                    /*if (!empty($result)) {
                        return false; //нельзя удалять
                    }*/
                    foreach ($result as $res) {
                        if (isset($res->id)) {
                            return false; //нельзя удалять
                        }
                    }
                }
            }
            return true; // можно удалять
        }
        if ($method === "from_schema") {
            $flag = true; // можно удалять
            $object = $this->db->GetForeignKey($this->table);
            foreach ($object as $row) {
                $table = substr(stristr($row->FOR_NAME, '/'), 1);
                $key = $row->FOR_COL_NAME;
                $select = 'SELECT `id` FROM '.$table.' WHERE '.$key.'='.$id;
                $result = $this->db->Get($select);
                foreach ($result as $res) {
                    if (isset($res->id)) {
                        $flag = false; //нельзя удалять
                        break 2;
                    }
                }
            }
            return $flag;
        }
    }
}
