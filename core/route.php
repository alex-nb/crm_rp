 <?php

/*
Класс-маршрутизатор для определения запрашиваемой страницы.
-цепляет классы контроллеров и моделей;
-создает экземпляры контролеров страниц и вызывает действия этих контроллеров.
*/

class Route {
    static function start() {

// контроллер и действие по умолчанию
        $modul_name = 'site';
        $controller_n = 'site';
        $action_name = 'index';
//разделение адреса по которому обратился пользователь
        $routes = explode('/', $_SERVER['REQUEST_URI']); 

        
// получаем имя модуля
        if ( !empty($routes[1]) ) {	
                $modul_name = $routes[1];
        }        
        
// получаем имя контроллера
        if ( !empty($routes[2]) ) {	
                $controller_n = $routes[2];
        }

// получаем имя действия
        if ( !empty($routes[3]) ) {
                $action_name = $routes[3];
        }
        
        //file_put_contents("text.txt", $action_name);
        
// получаем значения
        if ( !empty($routes[4]) ) {
                $parameter = $routes[4];
        }

        $model_name = $controller_n.'Model';
        $controller_name = $controller_n.'Controller';
        //$action_name = $action_name;

// подцепляем файл с классом модели (файла модели может и не быть)
        $model_path = ($_SERVER['DOCUMENT_ROOT']."/modules/common/models/".$model_name.".php");
        if(file_exists($model_path)) {
            require_once ($_SERVER['DOCUMENT_ROOT']."/modules/common/models/".$model_name.".php");
        }

// подцепляем файл с классом контроллера
        $controller_path = ($_SERVER['DOCUMENT_ROOT']."/modules/".$modul_name."/controllers/".$controller_name.".php");

        if(file_exists($controller_path)) {
            require_once ($_SERVER['DOCUMENT_ROOT']."/modules/".$modul_name."/controllers/".$controller_name.".php");
        }
        else {
                Route::ErrorPage404();
        }
        
        

// создаем контроллер
        $controller = new $controller_name;

        if(method_exists($controller, $action_name)) {
// вызываем действие контроллера
            if (!empty($parameter)) {
                $controller->$action_name($parameter);
            }
            else {
                $controller->$action_name();
                if ( empty($routes[3]) ) {
                    echo "<script>history.pushState(null, null, '".$controller_n."/index');</script>";
                }
            }  
        }
        else {
                Route::ErrorPage404();
        }
    }

    function ErrorPage404() {
         header('Location: /site');
        /*$host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:'.$host.'404');*/
        //echo "OLOLOLO 404";
    }
    
    
}

