<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?=$modul_name?></title>
        <link rel="stylesheet" type="text/css" href="/core/web/css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="/core/web/css/bootstrap-theme.min.css"/>

        <link rel="stylesheet" type="text/css" href="/core/web/css/custom.css"/>
        <link rel="stylesheet" type="text/css" href="/core/web/css/datatables.min.css"/>
        <link rel="stylesheet" type="text/css" href="/core/web/css/chosen.min.css"/>
        <link rel="stylesheet" type="text/css" href="/core/web/css/bootstrap-datetimepicker.min.css"/>

        <script type="text/javascript" src="/core/web/js/jquery.min.js"></script>
        <script type="text/javascript" src="/core/web/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/core/web/js/picklist.js"></script>
        <script type="text/javascript" src="/core/web/js/datatables.min.js"></script>
        <script type="text/javascript" src="/core/web/js/randomColor.js"></script>
        <script type="text/javascript" src="/core/web/js/control-modal.min.js"></script>
        <script type="text/javascript" src="/core/web/js/chosen.jquery.min.js"></script>
        <script type="text/javascript" src="/core/web/js/moment.min.js"></script>

        <script type="text/javascript" src="/core/web/js/bootstrap-datetimepicker.min.js"></script>
        <script type="text/javascript" src="/core/web/js/bootstrap-datetimepicker.ru.js"></script>
    </head>
    <body>
      <ul class="nav nav-pills"> <!-- Тег закрывается во вьюшках, не паникуйте.-->
        <li role="presentation"><a href="/site/site/out">Выход</a></li>
        <?php 
        require_once ($_SERVER['DOCUMENT_ROOT']."/modules/".$modul_name."/views/".$content_view); ?>
    </body>
</html>
