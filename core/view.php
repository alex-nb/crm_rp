<?php
/**
 * Родительский класс представлений.
 * @author aleks
 */

class View {

//Формирование вида    
     function generate($modul_name, $content_view, $data = NULL, $data2 = NULL, $data3 = NULL) {
//подключение общего шаблона
        include ($_SERVER['DOCUMENT_ROOT']."/core/template.php");
    } 

//Формирование вида без шаблона  
     function generateForm($modul_name, $block, $name_form, $data = NULL, $data2 = NULL, $data3 = NULL) {
        include ($_SERVER['DOCUMENT_ROOT']."/modules/$modul_name/views/$block/$name_form.php");
    } 
    
    
}
