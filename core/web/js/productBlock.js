function start() {
    var showModal2 = false;
    //скрываем группы/классы, если там нет товаров во всех таблицах
    var classes = ['table_offer', 'table_take_us', 'table_take_other', 'table_dont_take'];
    var colors = randomColor({luminosity: 'light', count: 30});

    var editProduct = new ModalApp.ModalProcess({ id: 'editProduct'});
    editProduct.init();

    var newFirm = new ModalApp.ModalProcess({ id: 'newFirm', title:'Новый конкурент'});
        newFirm.init();

    var parrent = new Array();

    for (var j=0; j<classes.length; j++) {
        parrent[classes[j]] = new Array();
        parrent[classes[j]]['i'] = 0;
        var row = $('tbody#'+classes[j]).find('tr');
        var id_tr = 0;
        $(row).each(function(indx, element) {
            id_tr = $(element).attr('id');
           if (!$(element).hasClass("prod")) {
               var all_tr = $('tbody#'+classes[j]).find("#"+id_tr+" ~ tr."+id_tr+", tr.row_"+id_tr)
                if (!$(all_tr).hasClass("prod")) {
                    $(element).remove(); //удаляем текущий элемент
                    $(all_tr).remove(); //удаляем дочерние
                }
            }
        });
    }

    $(".printButton").on('click', function () {
        var id_firm = $('#id_firm').attr('value');
        var id_product = {};
        $('div.tab-content').find('table').each(function() {
            var id = $(this).attr('id');
            var i = 0;
            id_product[id] = {};
            $(this).find("input:checkbox[name ^= prod]").val(function(index, x){
                id_product[id][i] = x;
                i++;
                return x;
            });
        });
        /*
        $(this).parents(".tab-pane").find("input:checkbox[name ^= prod]").val(function(index, x){
            id_product[i] = x;
            i++;
            return x;
        });
         */
        $.ajax({
            url : "printProduction",
            type: "POST",
            data: {id_firm:id_firm,id_product:id_product},
            success: function(html){
                var WinPrint = window.open('','','left=50,top=50,width=800,height=640,toolbar=0,scrollbars=1,status=0'); 
                WinPrint.document.write('<html><head><link rel="stylesheet" type="text/css" href="/core/web/css/custom.css"/>'); 
                WinPrint.document.write('</head><body >'); 
                WinPrint.document.write(html); 
                WinPrint.document.write('</body></html>'); 
                WinPrint.document.close(); 
                WinPrint.focus(); 
                WinPrint.print(); 
                WinPrint.close();
                /*editProduct.changeTitle("Печать");
                editProduct.changeBody(html);
                editProduct.showModal();*/
            }
        });
    });

    //действия при редактировании товаров
    $(".glyphicon-pencil").on('click', function() {
        var where = '';
        var title = '';
        if ($(this).hasClass('table_offer')) { where = 'offer'; title = 'Предложение';}
        if ($(this).hasClass('table_take_us')) { where = 'takeus'; title = 'Берут у нас';}
        if ($(this).hasClass('table_dont_take')) { where = 'donttake'; title = 'Не берут';}
        if ($(this).hasClass('table_take_other')) { where = 'takeother'; title = 'Берут не у нас';}
        var id_prod =  $(this).attr('id');
        var id_firm = $('#id_firm').attr('value');
        $.ajax({
            url : "formCheckOut",
            type: "POST",
            data: {id_prod:id_prod, id_firm:id_firm, where:where},
            success: function(html){
                editProduct.changeTitle(title);
                editProduct.changeBody(html);
                editProduct.changeFooter('<button type="button" class="btn btn-default" data-dismiss="modal" id="closeEditProduct">Закрыть</button><button type="submit" class="btn btn-primary" id="editSubmit">Сохранить изменения</button>');
                editProduct.showModal();
            }
        });
    });

    //действия при переносе товаров
    $(".out").on('click', function() {
        var from = $(this).parents(".tab-pane").find("form").attr("id");
        var where = $(this).attr("id");
        var id_firm = $("#id_firm").attr("value");
        $.ajax({
            url : "formCheckOut",
            type: "POST",
            data: {where:where, id_firm:id_firm,from:from},
            success: function(html){
                editProduct.changeTitle("Перенос товара");
                editProduct.changeBody(html);
                editProduct.changeFooter('<button type="button" class="btn btn-default" data-dismiss="modal" id="closeEditProduct">Закрыть</button><button type="submit" class="btn btn-primary" id="editSubmit">Сохранить изменения</button>');
                editProduct.showModal();
            }
        });   
    });        

    $('.modal-body').on('click', 'button#add_firm', function(e) {
        e.preventDefault();
        $.ajax({
                url : "formNewFirm",
                type: "POST",
                data: {},
                success: function(html){
                    showModal2 = true;
                    newFirm.changeBody(html);
                    newFirm.changeFooter('<button type="button" class="btn btn-default" data-dismiss="modal" id="closeNewFirm">Закрыть</button><button type="submit" class="btn btn-primary" id="firmSubmit">Сохранить изменения</button>');
                    editProduct.hideModal();
                }
            });
    });

    $('#editProduct').on('hidden.bs.modal', function (e) {
        if (showModal2) {
            showModal2 = false;
            window.setTimeout(function () {
                newFirm.showModal();
            }, 200);
        }
        else {
            editProduct.changeBody('');
            editProduct.changeFooter('');
        }
    });

    $('#newFirm').on('hidden.bs.modal', function (e) {
        window.setTimeout(function () {
            editProduct.showModal();
        }, 200);
        newFirm.changeBody('');
        newFirm.changeFooter('');
    });


    //при клике на чекбокс
    $("input:checkbox").on('change', function() {
        var this_class = $(this).attr("class");
        var this_tr = $(this).parents("tr");
        var id = $(this_tr).attr("id");
        var tr_in = $('tbody#'+this_class).find("#"+id+" ~ tr."+id+", tr.row_"+id);
        var check_in = $(tr_in).find("input:checkbox");
        if ($(this).prop("checked") == true){ $(check_in).prop({"checked":true, "indeterminate":false});} 
        else { $(check_in).prop({"checked":false, "indeterminate":false}); }
        var check_class = '';

        $.each($(this_tr).attr('class').split(/\s+/), function(index, item) {
            if (~item.indexOf("row_")) { check_class = check_class+"tr#"+item.slice(4)+", tr."+item.slice(4)+", tr.row_"+item.slice(4)+", "; }
            else {
                if (item !== "prod") { check_class = check_class+"tr#"+item+", tr."+item+", tr.row_"+item+", ";}
            }
        });
        check_class = check_class.slice(0, -2);

        var all_tr = jQuery.makeArray($('tbody#'+this_class).find(check_class)).reverse();
        $.each(all_tr, function(index, item) {
            if (!$(item).hasClass("prod")) {
                var id_item = $(item).attr("id");
                var tr_in_item = $('tbody#'+this_class).find("tr#"+id_item+" ~ tr."+id_item+", tr.row_"+id_item);
                var check_all = $(tr_in_item).find("input:checkbox");
                var checked = $(check_all).filter("input:checkbox:checked");
                if (checked.length !== 0 || check_all.length !== 0) {
                    if (checked.length === check_all.length) { //если кол-во чекнутых равно общему кол-ву чекбоксов, то чекаем и этот чекбокс
                        $(item).find("input:checkbox:first").prop({"indeterminate":false, "checked":true});
                    }
                    else if (checked.length === 0) { //если кол-во чекнутых равно 0, то отменяем чек у этого чекбокса
                        $(item).find("input:checkbox:first").prop({"indeterminate":false, "checked":false});
                    }
                    else if (checked.length < check_all.length) { //если чекнутые есть, но их меньше, чем всех чекбоксов, то делаем этот indeterminate
                        $(item).find("input:checkbox:first").prop({"indeterminate":true, "checked":false});
                    }
                }
            }
        });
    });

    //разворачивание/сворачивание строк
    $(".glyphicon-plus, .glyphicon-minus").on('click', function() {
        var this_class = $(this).attr("id");
        var this_tr = $(this).parents("tr");
        var id = $(this_tr).attr("id");
        var span = $(this_tr).find("span");

        if ($(span).hasClass('glyphicon-plus')) {
            if (parrent[this_class].length == 0) {
                $(this_tr).css("background", colors[parrent[this_class]['i']]);
                //console.log();
                var next_tr = $('tbody#'+this_class).find($("tr#"+id+" ~ tr:not(.row_"+id+", ."+id+", .prod)")).first();
                $(next_tr).css("background", colors[parrent[this_class]['i']]);
                parrent[this_class][parrent[this_class]['i']] = id;
                parrent[this_class]['i']++;
            }
            else {
                var flag = true;
                for (var j=0; j<parrent[this_class].length; j++) {
                    if($(this_tr).hasClass(parrent[this_class][j]) || $(this_tr).hasClass("row_"+parrent[this_class][j])) {
                        $(this_tr).css("background", colors[parrent[this_class]['i']]);
                        var next_tr = $('tbody#'+this_class).find($("tr#"+id+" ~ tr:not(.row_"+id+", ."+id+", .prod)")).first();
                        $(next_tr).css("background", colors[parrent[this_class]['i']]);
                        parrent[this_class][parrent[this_class]['i']] = id;
                        parrent[this_class]['i']++;
                        flag = false;
                        break;
                    }
                }
                if (flag) {
                    $('tbody#'+this_class).find($("tr")).css("background", "none");
                    parrent[this_class] =[];
                    parrent[this_class]['i'] = 0;
                    parrent[this_class][parrent[this_class]['i']] =[id];
                    $(this_tr).css("background", colors[parrent[this_class]['i']]);
                    var next_tr = $('tbody#'+this_class).find($("tr#"+id+" ~ tr:not(.row_"+id+", ."+id+", .prod)")).first();
                    $(next_tr).css("background", colors[parrent[this_class]['i']]);
                    parrent[this_class]['i']++;
                }    
            }
            $(span).toggleClass("glyphicon-plus glyphicon-minus");
            $('tbody#'+this_class).find('.row_'+id+'.unvisible').removeClass("unvisible");
        }
        else {
            $(span).toggleClass("glyphicon-plus glyphicon-minus");
            var tr_in = $('tbody#'+this_class).find("#"+id+" ~ tr."+id+", tr.row_"+id);
            $(tr_in).each(function(indx, element){ $(element).css("background", "none");});
            $(tr_in).find("span.glyphicon-minus").toggleClass("glyphicon-plus glyphicon-minus");
            $(tr_in).addClass('unvisible');
        }         
    });

    //кнопка завершить
    $("#cancel").on('click', function() { window.location.href="index";});
}
        