<?php

ini_set('display_errors', 1);
//подключение классов ядра
require_once $_SERVER['DOCUMENT_ROOT'].'/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/core/model.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/core/view.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/core/controller.php';
//подключение класса маршрутизатора и вызов его статического метода
require_once $_SERVER['DOCUMENT_ROOT'].'/core/route.php';

$Auth = new UserAuth();
/*  Авторизация
 *  Считать куку у пользователя,
 *  Если нет кинуть на авторизацию
 *  Если есть но такая отсутствует в базе то кинуть на авторизацию
 *  Если есть и совпадает с такой в базе тогда вытащить данные о пользователе по ключу и дать соответствующий доступ
 *
 *  Аутентификация
 *  Вытащить из базы информацию о пользователе если есть такой в базе
 *  Зашифровать введёный пароль и сравнить с тем что был в базе
 *  Если совпадают, то сгенерировать новую куку, записать её в базу и браузер, перенаправить на нужную пользователю страницу
 *  Если не совпадают кинуть на авторизацию
*/
$user = $Auth->GetUser();
if(!isset($_COOKIE["token"])){
    echo $Auth->AuthForm();
}elseif($_COOKIE["token"]!==$user['token']){
    echo $Auth->AuthForm();
}elseif($_COOKIE["token"]==$user['token']){
    Route::start();
}
echo $Auth->Out;
