<?php
/*
 * Контроллер для работы с пользователями системы.
 * Код объекта 
 * @author aleks
 */
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/userModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/acl_actionModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/acl_child_rolesModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/acl_dissallowModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/acl_objectModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/acl_premissionModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/acl_premission_userModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/acl_rolesModel.php";

class premissionController extends Controller {
    protected $user;
    protected $acl_action;
    protected $acl_child_roles;
    protected $acl_dissallow;
    protected $acl_object;
    protected $acl_premission;
    protected $acl_premission_user;
    protected $acl_roles;
    
    function __construct() {
        parent::__construct();
        $this->user = new userModel();
        $this->acl_action = new acl_actionModel();
        $this->acl_child_roles = new acl_child_rolesModel();
        $this->acl_dissallow = new acl_dissallowModel();
        $this->acl_object = new acl_objectModel();
        $this->acl_premission = new acl_premissionModel();
        $this->acl_premission_user = new acl_premission_userModel();
        $this->acl_roles = new acl_rolesModel();
        $this->code = 7;
    }
    
//метод, генерирующий страницу для работы с правами пользователя    
    function index() {
        if ( !CheckAction($this->code, $this->read, $this->premissions)) {
            $locate = $_SERVER['HTTP_REFERER'];
            echo "<script>alert('Доступ запрещен'); window.location.href='".$locate."';</script>";
        }
        else {
            $user = array ();
            $roles = array();
            $parrents = array();

            $data = $this->user->GetAllRows();
            foreach ($data as $row) {
                $user[$row->id]['id'] = $row->id;
                $fio = $row->last_name." ";
                $fio .= $row->first_name." ";
                $fio .= $row->second_name;
                $user[$row->id]['fio'] = $fio;
            } //End foreach

            $roles_d = $this->acl_roles->GetAllRows();
            $parrents_d = $this->acl_child_roles->GetAllRows();
            foreach ($parrents_d as $par) {
                $parrents[$par->childe_code]['id_childe'] =  $par->id;
                $parrents[$par->childe_code]['parrents_id'][$par->parrent_code] =  $par->parrent_code;
            }
            foreach ($roles_d as $row) {
                $roles[$row->id]['id'] =$row->id;
                $roles[$row->id]['title'] =$row->title;
                $roles[$row->id]['description'] =$row->description;
            } //End foreach

            foreach ($roles as $role) {
                if (isset($parrents[$role['id']]['parrents_id'])) {
                    foreach ($parrents[$role['id']]['parrents_id'] as $parr) {
                        $roles[$role['id']]['parrents'][] =$roles[$parr]['title'];
                    }
                }
            }
            $this->view->generate('admin','premission/index.php', $roles, $user);
        }
    }//End acl()
 
//вызов формы с действиями и передача необходимых параметров    
    function formObj(){
        $code = 9;
        if ( !CheckAction($code, $this->read, $this->premissions)) {
            //$locate = $_SERVER['HTTP_REFERER'];
            echo 0;
        }
        else {
            $premission = array();
            $premission['current_role'] = 0;
            $obj_d = $this->acl_object->GetAllRows();
            foreach ($obj_d as $row) {
                $premission[$row->id]['object_code'] = $row->id;
                $premission[$row->id]['object_title'] = $row->title;
                $premission[$row->id]['description'] = $row->description;
            } //End foreach
            if(isset($_POST['id_role'])){$premission['current_role'] = $_POST['id_role'];}
            $this->view->generateForm('admin', 'premission', '_form_object', $premission);
        }
    }
    
    //вызов формы с действиями и передача необходимых параметров    
    function formAction(){
        $code = 10;
        if ( !CheckAction($code, $this->read, $this->premissions)) {
            echo 0;
        }
        else {
                if (isset($_POST['role_id']) && isset($_POST['object_id'])) {
                $premission = array();
                $prem = array();
                $action = array();

                $current_role = $_POST['role_id'];
                $current_object = $_POST['object_id'];
                $premission['current_role']['id'] = $current_role;
                $premission['current_object'] = $current_object;

                $act_d = $this->acl_action->GetAllRows();
                $parrents_roles = $this->acl_child_roles->GetAllRows('childe_code='.$current_role);
                $prem_d = $this->acl_premission->GetAllRows();
                $role_d = $this->acl_roles->GetAllRows();

                foreach ($act_d as $row) {
                    $action[$row->id] = (array) $row;
                } //End foreach
                foreach ($role_d as $row) {
                    $roles[$row->id] = (array) $row;
                } //End foreach

                $premission['all_actions'] = $action;
                $premission['current_role']['title'] = $roles[$current_role]['title'];

                foreach ($parrents_roles as $p_r) {
                    $parrents[$p_r->parrent_code] = $p_r->parrent_code;
                }

                foreach ($prem_d as $row) {
                    $prem[$row->action_code]['action_title'] = $action[$row->action_code]['title'];
                    $prem[$row->action_code]['action_code'] = $row->action_code;
                    if (isset($parrents[$row->role_code]) && $row->object_code == $current_object) {
                        $premission['parrent_actions'][$row->role_code]['title'] = $roles[$row->role_code]['title'];
                        $premission['parrent_actions'][$row->role_code]['actions'][] = $action[$row->action_code]['title'];
                    }
                    if ($row->role_code == $current_role && $row->object_code == $current_object) {
                        $premission['this_actions'][$row->action_code] = $row->action_code;
                    }
                } //End foreach
                $this->view->generateForm('admin', 'premission', '_form_action', $premission);
            }
        }
    }
    
//получение всех пользователей для ид указанной роли (метод пост или прямой вызов)    
     function getUser($id = NULL) {
        $code = 8;
        if (isset($_POST['id_role'])) {
            if ( !CheckAction($code, $this->read, $this->premissions)) {
                //$locate = $_SERVER['HTTP_REFERER'];
                echo 0;
            }
            else {
                //if (isset($_POST['id_role'])) {
                $id_role = $_POST['id_role'];
                $user = GetUsersByRole($id_role);
                $users = json_encode($user);
                print_r($users);
                //} //End if
            } 
        }
        if (isset($id)) {
            $id_role = $id;
            $user = GetUsersByRole($id_role);
            return $user;
        }
     }
     
//сохранение изменений по определению пользователей по ролям    
     function saveUser(){
         $code = 8;
        if ( !CheckAction($code, $this->update, $this->premissions)) {
            //$locate = $_SERVER['HTTP_REFERER'];
            echo 0;
        }
        else {
            $id_role = NULL;
            $id_users = NULL;
            if (!empty($_POST['id_role'])) {
                $id_role = $_POST['id_role'];
                 if (!empty($_POST['id_users'])) { 
                     $new = array ();
                     $post = $_POST['id_users'];
                     foreach ($post as $ololo) {
                         $new[] = $ololo['id'];
                     }
                    $del = array_keys($this->getUser($id_role));
                    $donttouch = array_intersect($new, $del); 
                    foreach ($donttouch as $val_dont) {
                        if (in_array($val_dont, $new)) {
                                unset($new[array_search($val_dont, $new)]);
                        }
                        if (in_array($val_dont, $del)) {
                                unset($del[array_search($val_dont, $del)]);
                        }
                    }
                    if (!empty($del)) {
                        foreach ($del as $val_del) {
                            $for_delete = $this->acl_premission_user->GetAllRows('role_id = '.$id_role.' AND user_id = '.$val_del);
                            foreach ($for_delete as $row) {
                                $this->acl_premission_user->DeleteRow($row->id);
                            }
                        }
                    }
                    if(!empty($new)) {
                        foreach ($new as $val_new) {
                            $model = new acl_premission_userModel();
                            $model->user_id = $val_new;
                            $model->role_id = $id_role;
                            $model->Save();
                        }
                    }
                }
                else {
                    $for_delete = $this->acl_premission_user->GetAllRows('role_id = '.$id_role);
                    foreach ($for_delete as $row) {
                        $this->acl_premission_user->DeleteRow($row->id);
                    }
                }
                echo 1;
            }
            else {echo 2;}
        }  
     }
     
//добавление действий согласно выбранной роли и объекту     
     function saveAction(){
        $code = 10;
        if ( !CheckAction($code, $this->update, $this->premissions)) {
            //$locate = $_SERVER['HTTP_REFERER'];
            echo "Вам недоступно данное действие";
        }
        else {
            $role_id = NULL;
            $object_id = NULL;
            $new = array ();
            $del = array ();
            if(!empty($_POST['role_id'])) {
                $role_id = $_POST['role_id'];
                if (!empty($_POST['object_id'])) {
                    $object_id = $_POST['object_id'];
                    if (!empty($_POST['action'])) {
                       $new = $_POST['action'];
                       $old = $this->acl_premission->GetAllRows('object_code='.$object_id.' AND role_code='.$role_id);
                       foreach ($old as $d) {
                           $del[]=$d->action_code; 
                       }
                       $donttouch = array_intersect($new, $del); 
                       foreach ($donttouch as $val_dont) {
                           if (in_array($val_dont, $new)) {
                                   unset($new[array_search($val_dont, $new)]);
                           }
                           if (in_array($val_dont, $del)) {
                                   unset($del[array_search($val_dont, $del)]);
                           }
                       }
                       if (!empty($del)) {
                           foreach ($del as $val_del) {
                               $for_delete = $this->acl_premission->GetAllRows('object_code='.$object_id.' AND role_code='.$role_id.' AND action_code = '.$val_del);
                               foreach ($for_delete as $row) {
                                   $this->acl_premission->DeleteRow($row->id);
                               }
                           }
                       }
                       if(!empty($new)) {
                           foreach ($new as $val_new) {
                               $model = new acl_premissionModel();
                               $model->object_code = $object_id;
                               $model->role_code = $role_id;
                               $model->action_code = $val_new;
                               $model->Save();
                               unset($model);
                           }
                       }
                   } 
                   else {
                       $for_delete = $this->acl_premission->GetAllRows('object_code='.$object_id.' AND role_code='.$role_id);
                       foreach ($for_delete as $row) {
                           $this->acl_premission->DeleteRow($row->id);
                       }
                   }
               }
               echo "Сохранили";
            } 
            else {echo "Ошибка";}
        }
     }
     
}
