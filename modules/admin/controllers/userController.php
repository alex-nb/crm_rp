<?php
/*
 * Контроллер для работы с пользователями системы.
 * Код объекта 1
 * @author aleks
 */

require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/user_deptModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/user_positionModel.php";

class userController  extends Controller {
    protected $user;
    protected $dept;
    protected $position;

    function __construct() {
        parent::__construct();
        $this->user = new userModel();
        $this->dept = new user_deptModel();
        $this->position = new user_positionModel();
        $this->code = 1;
    }
    
//метод, вызывающий главную страницу данного блока
    function index() {
        if ( !CheckAction($this->code, $this->read, $this->premissions)) {
            $locate = $_SERVER['HTTP_REFERER'];
            echo "<script>alert('Доступ запрещен'); window.location.href='".$locate."';</script>";
        }
        else {
            $data = $this->user->GetAllRows();
            $depts_d = $this->dept->GetAllRows();
            $positions_d = $this->position->GetAllRows();

            foreach ($positions_d as $row) {
                $positions[$row->id] = (array) $row;
            } //End foreach
            foreach ($depts_d as $row) {
                $depts[$row->id] = (array) $row;
            } //End foreach
            foreach ($data as $row) {
                $user[$row->id] =(array) $row;
                if (isset($user[$row->id]['id_dept'])) {
                    $user[$row->id]['dept_name'] = $depts[$user[$row->id]['id_dept']]['name'];
                } //End if
                 if (isset($user[$row->id]['id_pos'])) {
                    $user[$row->id]['pos_name'] = $positions[$user[$row->id]['id_pos']]['name'];
                } //End if
            } //End foreach
            $user['table_name'] = $this->user->GetTableName();
            $this->view->generate('admin','user/index.php', $user);
        }
    } //End index()

//метод, генерирующий страницу со списком отделов   
     function dept() {
        $code = 2;
        if ( !CheckAction($code, $this->read, $this->premissions)) {
            $locate = $_SERVER['HTTP_REFERER'];
            echo "<script>alert('Доступ запрещен'); window.location.href='".$locate."';</script>";
        }
        else {
            $data = $this->dept->GetAllRows();		
            $this->view->generate('admin','user/dept.php',$data);
        }
        
    } //End dept()
  
//метод, генерирующий страницу со списком должностей
    function position() {
        $code = 3;
         if ( !CheckAction($code, $this->read, $this->premissions)) {
            $locate = $_SERVER['HTTP_REFERER'];
            echo "<script>alert('Доступ запрещен'); window.location.href='".$locate."';</script>";
        }
        else {
            $data = $this->position->GetAllRows();		
            $this->view->generate('admin','user/position.php',$data);          
        }
    } //End position()

//метод, генерирующий форму для модального окна с необходимой инфомрацией по конкректному пользователю
    function form() {
        $user = NULL;
        //file_put_contents("text.txt", "In action form \n", FILE_APPEND);
        if (isset($_POST['id_user'])) {
            //file_put_contents("text.txt", "Id user isset \n", FILE_APPEND);
            if ( !CheckAction($this->code, $this->update, $this->premissions)) {
                echo "Доступ запрещен";
            }
            else {         
                $depts_d = $this->dept->GetAllRows();
                $positions_d = $this->position->GetAllRows();

                 foreach ($positions_d as $row) {
                    $positions[$row->id] = $row->name;
                } //End foreach
                foreach ($depts_d as $row) {
                    $depts[$row->id] = $row->name;
                } //End foreach  
                //if (isset($_POST['id_user'])) {
                    $data = $this->user->GetRowById($_POST['id_user']);
                    foreach ($data as $row) {
                        $user['user']=(array) $row;
                        if (isset($user['user']['id_dept'])) {
                            $user['user']['dept_name'] = $depts[$user['user']['id_dept']];
                        } //End if
                         if (isset($user['user']['id_pos'])) {
                            $user['user']['pos_name'] = $positions[$user['user']['id_pos']];
                        } //End if
                    } //End foreach
                //} //End if
                $user['depts'] = $depts;
                $user['positions'] = $positions;
                $this->view->generateForm('admin', 'user', '_form_user', $user);
            }        
        }
        else {
            //file_put_contents("text.txt", "Id user don't isset \n", FILE_APPEND);
            if ( !CheckAction($this->code, $this->create, $this->premissions)) {
                echo "Доступ запрещен";
            }
            else {         
                $depts_d = $this->dept->GetAllRows();
                $positions_d = $this->position->GetAllRows();

                 foreach ($positions_d as $row) {
                    $positions[$row->id] = $row->name;
                } //End foreach
                foreach ($depts_d as $row) {
                    $depts[$row->id] = $row->name;
                } //End foreach  
                $user['depts'] = $depts;
                $user['positions'] = $positions;
                $this->view->generateForm('admin', 'user', '_form_user', $user);
            }   
        }
    } //End form()
    
//метод, реализующий создание/изменение пользователя системы   
     function edit() {
        if (isset($_POST['id'])) {
            $user = new userModel();
            $fields = array_keys($user->fieldsTable());

            foreach ($fields as $key) {
               if (isset($_POST[$key]) && $key != 'id') {
                    $user->$key = $_POST[$key];
                } //End if
            } //End foreach
            if (!empty($_POST['new_password'])) {
                $user->password = MakePass($_POST['new_password'] );
            } //End if
            if ($_POST['id'] != 0) {
                $user->updated_at = date('Y-m-d\TH:i:sP');
                $user->Update($_POST['id']);
            } //End if         
            else {
                $user->created_at = date('Y-m-d\TH:i:sP');
                $user->Save();
            } //End else
            unset($user);
         }
         $locate = $_SERVER['HTTP_REFERER'];
        echo "<script>window.location.href='".$locate."';</script>";
    } //End edit()
   
 //метод, реализующий удаление пользователя системы по id
    function deleteUser() {
        if ( !CheckAction($this->code, $this->delete, $this->premissions)) {
            echo "Извините, у вас нет прав на данное действие";
        }
        else {
            if (isset($_POST['id_user'])) {
               $id = $_POST['id_user'];
               $answer = $this->user->DeleteRow($id);
               if ($answer) {
                   echo 1; 
               }
               else {
                   echo "Извините, объект удалить нельзя, так как он используется в других записях";
               }
           }           
        }
    } //End delete()

 //получение доступных действий для пользователя по ид
    function getPremission() {
        $code = 8;
        if ( !CheckAction($code, $this->read, $this->premissions)) {
            echo "Извините, у вас нет прав на данное действие";
        }
        else {
            $premission_for_user = NULL;
            $data = array ();
            if (isset($_POST['id_user'])) {
               $id = $_POST['id_user'];
               $premission_for_user = GetPremissions($id);
               if (isset($premission_for_user['dissallow'])) {
                   $data['dissallow'] = $premission_for_user['dissallow'];
               }
               foreach ($premission_for_user as $row) {
                   if (isset($row['objects'])) {
                        foreach ($row['objects'] as $obj) {
                            $data['objects'][$obj['obj_id']]['title'] = $obj['obj_title'];
                            foreach ($obj['actions'] as $act) {
                                $data['objects'][$obj['obj_id']]['actions'][$act['act_id']]['title'] = $act['act_title'];
                            }
                        }
                   }
               }
           }
           $this->view->generateForm('admin', 'user', '_form_premission_for_user', $data);
        }
    } //End delete()    
}
