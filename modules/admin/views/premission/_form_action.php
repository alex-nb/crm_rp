<?php
    $premission = $data;
    $current_role = $premission['current_role']['id'];
    $current_object = $premission['current_object'];
    $html = '';
   /*echo "<pre>";
    var_dump($premission);
    echo "</pre>";*/
    
    foreach ($premission['all_actions'] as $key=>$actions) {
        if (isset($premission['this_actions'][$actions['id']])) {
            $html .= "<label><input type='checkbox' class='thing' checked name='action[]' value='".$actions['id']."'>".$actions['title']."</label>";
        }
        else { $html .= "<label><input type='checkbox' class='thing' name='action[]' value='".$actions['id']."'>".$actions['title']."</label>"; 
        }
    }
    $html .= '<input name ="object_id" class="unvisible" readonly value='.$current_object.'>';
  
    if (isset($premission['parrent_actions'])) {
        $action = array();
        $html .= "<label><br>По этому объекту для роли '".$premission['current_role']['title']."' доступны так же действия: <br>";
        foreach ($premission['parrent_actions'] as $key=>$parrent) {
            $html .= "&#10003; От роли '".$parrent['title']."': ";
            $name = implode (", ", $parrent['actions']);
            $html .= $name."<br>";
        }
        $html .= '</label>';
    }
    echo $html;
