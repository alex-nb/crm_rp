<?php
        $premission = $data;
        $role = $data['current_role'];
?>

<input name ="role_id" class='unvisible' readonly value=<?=$premission['current_role']?>>

<div class="col-xs-5">
   <table class="table table-bordered table-hover table-condensed"  id="table_act" width="100%">
       <thead>
           <tr>
               <th>Объект доступа</th>
           </tr>
       </thead>
       <tbody id="action">
           <?php
           foreach ($premission as $prem) {
               if (isset($prem['object_title'])) {
                   echo "<tr title='".$prem['description']."' class='rowlink' id='".$prem['object_code']."'>";
                   echo "<td class='title pointer'>".$prem['object_title']."</td>";
                    echo "</tr>";
               }
           }
           ?>
       </tbody>
   </table>
    <br><br>
 </div>
<div class="unvisible" id="actions">
   <div class="checkboxmy" id="checkboxmy">      
   </div>
    <label id="parrent_msg"></label>
</div>


<script>
    $(document).ready(function(){
        var cur_role = <?=$role?>;
        $('#action').on('click', '.rowlink', function(){
            var object_id = $(this).attr('id');
            //выделяем строку классом info (синенький)
            $('tr.info').removeClass('info');
            $(this).addClass('info');
            $.ajax({
                type: 'POST',
                url: 'formAction',
                data: {
                    object_id:object_id,
                    role_id:cur_role
                },
                success: function(response) {
                    if (response == 0) {
                        alert("Вам недоступно данное действие");
                    }
                    else {
                    //очищаем самую правую сторону с чекбоксами (если она вдргу была)          
                          $("#checkboxmy").html('');
                    //находим id объекта и задаем начальный HTML для чекбоксов      
                          var html = '<label><input type="checkbox" id="checkall"> Выбрать все действия</label>';
                          html += response;

                    //записываем полученный код
                          $('#checkboxmy').append(html);
                    //находим див для отображение действий и кнопку, убираем класс unvisible и добавляем ему необходимый класс
                          document.getElementById('actions').className = 'col-md-5';
                          document.getElementById('act_butt').className = 'btn btn-primary';

                   //делаем штуки для красивого отображения чекбоксов и работы "выбрать все действия"
                          var checkboxes = document.querySelectorAll('input.thing'),
                          checkall = document.getElementById('checkall');
                          for(var i=0; i<checkboxes.length; i++) {
                              checkboxes[i].onclick = function() {
                                  var checkedCount = document.querySelectorAll('input.thing:checked').length;
                                  checkall.checked = checkedCount > 0;
                                  checkall.indeterminate = checkedCount > 0 && checkedCount < checkboxes.length;
                              };
                          };
                          checkall.onclick = function() {
                              for(var i=0; i<checkboxes.length; i++) {
                                  checkboxes[i].checked = this.checked;
                              };
                          };   
                    }
                }
            }); 
        });
    }); 
</script>