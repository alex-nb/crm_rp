<?php 
$roles = $data;
$users = json_encode($data2);
/*
echo "<pre>";
var_dump($data3);
echo "</pre>";
*/
?>    
    <li role="presentation"><a href="../user/index">Назад</a></li> 
</ul>
    <br>
    <div class="row">
        <div class="col-md-6">
            <table class="table table-bordered table-hover table-condensed"  id="table_role" width="100%">    
            <tr>
                <th>ИД</th>
                <th>Наименование</th>
                <th>Наследование</th>
                <th>Описание</th>
                <th></th>
            </tr>
          <?php
            
             foreach ($roles as $row) {
                $parrents_title = NULL;
                echo "<tr id=tr".$row['id'].">";
                echo "<td>".$row['id']."</td>";
                echo "<td>".$row['title']."</td>"; 
                echo "<td>";
                if (isset($row['parrents'])) {       
                    $parrents = implode (", ", $row['parrents']);
                    echo $parrents;
                }
                else {echo "-";}
                echo "</td>";
                echo "<td>";
                 if (!empty($row['description'])) echo $row['description'];
                 else echo "-";
                echo "</td>"; 
                echo "<td><span class='glyphicon glyphicon-user pointer' id='".$row['id']."' data-toggle='tooltip' title='Пользователи'></span>  <span class='glyphicon glyphicon-ban-circle pointer' id='".$row['id']."' data-toggle='tooltip' title='Действия по объектам'></span></td>";
                echo "</tr>";
            }
          ?>
          </table>
        </div>
        
        <div class="unvisible" id="users">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Сотрудники (общий список/добавленные к выбранной роли)</h3>
              </div>
              <div class="panel-body">
                <div  id="pickList"></div>
                <br>
                <button class="btn btn-primary" id="getSelected">Сохранить</button>
              </div>
            </div>
        </div>
        
        <div class="unvisible" id="action">
            <div>
                <form onsubmit="return false;" class="form-horizontal" id = "form_action" method="POST">
                    <div class="form-group" id="form-group">
                    </div>
                    <button type="submit" class="unvisible" id="act_butt">Сохранить</button>
                </form>
            </div>
        </div>
        
    </div>   


<script>
$(document).ready(function(){
    var id_role = 0;
    var pick = 0;
    $('.glyphicon-user').click(function() {
        document.getElementById('action').className = 'unvisible';
        id_role = $(this).attr('id');
       
        $('tr.success').removeClass('success');
        document.getElementById('tr'+id_role).className = 'success';
        $.ajax({
                type: 'POST',
                url: 'getUser',
                data: {id_role:id_role},
                success: function(data) {
                    if (data == 0) {
                        alert("Вам недоступно данное действие");
                    }
                    else {
                        var users = JSON.parse(data);                  
                        var val = <?=$users?>;
                        var all = [];
                        for(var val_key in val) {
                            var found = false;
                            for(var user_key in users) {
                              if (val[val_key].id == users[user_key].id) {
                                found = true;
                                continue;
                              }
                            }
                            if (!found) {
                              all.push(val[val_key]);
                            }
                        }
                        $("#pickList").html('');
                        pick = $("#pickList").pickList({
                          data: all,
                          res: users
                        });              
                       document.getElementById('users').className = 'col-md-6';
                    }
                }
            });    
    });

    $("#getSelected").click(function() { 
      $.ajax({
          type: 'POST',
          url: 'saveUser',
          data: {
              id_users:pick.getValues(),
              id_role:id_role
          },
          success: function(data) {
              if (data == 0) {
                  alert("Вам недоступно данное действие");
              }
              else {
                  alert("Cохранено");
              }
              //console.log(data);
              //alert(data);
          }
      }); 
    });

    $('.glyphicon-ban-circle').click(function() {
        id_role = $(this).attr('id');
        document.getElementById('users').className = 'unvisible';
        document.getElementById('act_butt').className = 'unvisible';
        $('tr.success').removeClass('success');
        document.getElementById('tr'+id_role).className = 'success';
        
        $.ajax({
                type: 'POST',
                url: 'formObj',
                data: {id_role:id_role},
                success: function(html) {
                    if (html == 0) {
                        alert("Вам недоступно данное действие");
                    }
                    else {
                        $("#form-group").html('');
                        $('#form-group').append(html);
                        document.getElementById('action').className = 'col-md-6';
                    }
                }
            }); 
    });

  //для отправки формы ajax'ом
  //На будущее: тут была заморочка с невыполнением запроса. Так как запускалась стандартная отправка формы
  //и страница перезагружалась прежде, чем запрос снизу начинал выполняться 
  //Два решения: в форму прописать onsubmit="return false;", либо менять кнопку <button..> на <input type="button" onclick="">
  //И чутка изменить вызов функции, соответственно.
    $('#form_action').submit(function(){ 
        $.ajax({
            url: 'saveAction',
            type: 'POST',
            data: $("#form_action").serialize(),
            success: function(data) {
                alert(data);
            },
        });  
    });  

});
</script>