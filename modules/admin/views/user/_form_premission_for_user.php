<?php
/*echo "<pre>";
var_dump($data);
echo "</pre>";*/
?>
<a href="#premission" data-toggle="collapse" class="btn btn-default btn-lg btn-block btn-sm">Показать доступные действия для пользователя</a>
<div class="collapse" id="premission">
    <div class="well">
        <?php
        foreach ($data['objects'] as $row) {
            echo "<p>Объект: ".$row['title']."</p>";
            echo "<ul>";
            foreach ($row['actions'] as $act) {
                echo "<li>".$act['title'];
            }
            echo "</ul>";
        }
        ?>        
    </div>
</div>
<br>
<a href="#dissallow" data-toggle="collapse" class="btn btn-default btn-lg btn-block btn-sm">Показать запреты для пользователя</a>
<div class="collapse" id="dissallow">
    <div class="well">
        <?php
        if (isset($data['dissallow'])) {
            foreach ($data['dissallow'] as $diss) {
                echo "<p>Объект: ".$diss['title']."</p>";
                echo "<ul>";
                foreach ($diss['actions'] as $act) {
                    echo "<li>".$act['title'];
                }
                echo "</ul>";
            }
        }
        else {echo "Запретов нет";}
        ?>
        <br><span class='glyphicon glyphicon-edit pointer' id='' data-toggle='tooltip' title='Добавить запрет'></span>
    </div>
</div>

<div class="col-xs-5">
   <table class="table table-bordered table-hover table-condensed"  id="table_act" width="100%">
       <thead>
           <tr>
               <th>Объект доступа</th>
           </tr>
       </thead>
       <tbody id="row_obj">
           <?php
           foreach ($premission as $prem) {
               if (isset($prem['object_title'])) {
                   echo "<tr class='rowlink' id='".$prem['object_code']."'>";
                   echo "<td class='title'>".$prem['object_title']."</td>";
                    echo "</tr>";
               }
           }
           ?>
       </tbody>
   </table>
    <br><br>
 </div>

<div class="unvisible" id="action">
    <div>
        <form onsubmit="return false;" class="form-horizontal" id = "form_action" method="POST">
            <div class="form-group" id="form-group">
            </div>
            <button type="submit" class="unvisible" id="act_butt">Сохранить</button>
        </form>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('.btn-default').click(function() {
            setTimeout( function () {
                $('#myModal').modal('handleUpdate');
            } , 500 );
        });

        $('.glyphicon-edit').click(function() {
            $.ajax({
                    type: 'POST',
                    url: 'form_object',
                    data: {},
                    success: function(html) {
                        if (html == 0) {
                            alert("Вам недоступно данное действие");
                        }
                        else {
                            console.log(html);
                            $("#objects").html('');
                            $('#objects').append(html);
                            setTimeout( function () {
                                console.log("HII");
                                $('#myModal').modal('handleUpdate');
                            } , 1000 );
                        }
                    }
                }); 
        }); 
 
        $('#row_obj').on('click', '.rowlink', function(){
            var object_id = $(this).attr('id');
            //выделяем строку классом info (синенький)
            $('tr.info').removeClass('info');
            $(this).addClass('info');
            $.ajax({
                type: 'POST',
                url: 'form_action',
                data: {
                    object_id:object_id
                },
                success: function(response) {
                    if (response == 0) {
                        alert("Вам недоступно данное действие");
                    }
                    else {
                    //очищаем самую правую сторону с чекбоксами (если она вдргу была)          
                          $("#checkboxmy").html('');
                    //находим id объекта и задаем начальный HTML для чекбоксов      
                          var html = '<label><input type="checkbox" id="checkall"> Выбрать все действия</label>';
                          html += response;
                          $('#checkboxmy').append(html);
                    //находим див для отображение действий и кнопку, убираем класс unvisible и добавляем ему необходимый класс
                          document.getElementById('actions').className = 'col-md-5';
                          document.getElementById('act_butt').className = 'btn btn-primary';

                   //делаем штуки для красивого отображения чекбоксов и работы "выбрать все действия"
                          var checkboxes = document.querySelectorAll('input.thing'),
                          checkall = document.getElementById('checkall');
                          for(var i=0; i<checkboxes.length; i++) {
                              checkboxes[i].onclick = function() {
                                  var checkedCount = document.querySelectorAll('input.thing:checked').length;
                                  checkall.checked = checkedCount > 0;
                                  checkall.indeterminate = checkedCount > 0 && checkedCount < checkboxes.length;
                              };
                          };
                          checkall.onclick = function() {
                              for(var i=0; i<checkboxes.length; i++) {
                                  checkboxes[i].checked = this.checked;
                              };
                          };   
                    }
                }
            }); 
        });
    }); 
</script>