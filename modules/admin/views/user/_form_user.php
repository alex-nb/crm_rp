<form class="form-horizontal" id = "form" method="POST" action="edit">
    <?php // echo "<pre>"; var_dump($data);  echo "</pre>";
    $sex_user = [0=>'Выберете пол', 1=>'Мужской', 2=>'Женский'];?>
    <div class="form-group">
        <label for="id" class="col-xs-2 control-label">ИД:</label>
        <div class="col-xs-10">
            <input type="number" class="form-control" id="id" name ="id" readonly value=<?php if (isset($data['user'])) {echo $data['user']['id'];} else { echo 0;} ?>>
        </div>
    </div>
    
    <div class="form-group">
        <label for="last_name" class="col-xs-2 control-label">Фамилия*:</label>
        <div class="col-xs-10">
            <input type="text" required class="form-control" id="last_name" name = "last_name" <?php if (isset($data['user'])) {echo 'value="'.$data['user']['last_name'].'"';} else { echo "placeholder='Введите фамилию'";} ?>>
        </div>
    </div>
    
    <div class="form-group">
        <label for=first_name" class="col-xs-2 control-label">Имя*:</label>
        <div class="col-xs-10">
            <input type="text" required class="form-control" id="first_name" name = "first_name" <?php if (isset($data['user'])) {echo 'value="'.$data['user']['first_name'].'"';} else { echo "placeholder='Введите имя'";} ?>>
        </div>
    </div>
    
    <div class="form-group">
        <label for=second_name" class="col-xs-2 control-label">Отчество:</label>
        <div class="col-xs-10">
            <input type="text" class="form-control" id="second_name" name = "second_name" <?php if (isset($data['user'])) {echo 'value="'.$data['user']['second_name'].'"';} else { echo "placeholder='Введите отчество'";} ?>>
        </div>
    </div>
    
    <div class="form-group">
        <label for="sex" class="col-xs-2 control-label">Пол:</label>
        <div class="col-xs-6">
            <select class="form-control" id="sex" name = "sex">
            <?php 
                    foreach ($sex_user as $key=>$name) {
                        if ($key == $data['user']['sex']) {
                            echo "<option selected value=".$key.">".$name."</option>";
                        }
                        else {
                            echo "<option value=".$key.">".$name."</option>";
                        }
                    }
                ?>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="username" class="col-xs-2 control-label">Логин*:</label>
        <div class="col-xs-10">
            <input type="text"  required class="form-control" id="username"  name = "username" <?php if (isset($data['user'])) {echo 'value="'.$data['user']['username'].'"';} else { echo "placeholder='Введите логин'";} ?>>
        </div>
    </div>
    
    <div class="form-group">
        <label for="new_password" class="col-xs-2 control-label">Пароль:</label>
        <div class="col-xs-10">
            <input type="text" class="form-control" id="new_password"  name = "new_password" placeholder="Введите новый пароль">
        </div>
    </div>    
    
    <div class="form-group">
        <label for="email" class="col-xs-2 control-label">Адрес email*:</label>
        <div class="col-xs-10">
            <input type="email" required class="form-control" id="email" name = "email" <?php if (isset($data['user'])) {echo 'value="'.$data['user']['email'].'"';} else { echo "placeholder=''Введите email'";} ?>>
        </div>
    </div>

    <div class="form-group">
        <label for="id_pos" class="col-xs-2 control-label">Должность:</label>
        <div class="col-xs-6">
            <select class="form-control" id="id_pos" name = "id_pos">
                <option value="1">Неизвестно</option>
                <?php 
                    unset($data['positions'][1]);
                    asort($data['positions']);
                    foreach ($data['positions'] as $id=>$pos) {
                        if ($id == $data['user']['id_pos']) {
                            echo "<option selected value=".$id.">".$pos."</option>";
                        }
                        else {
                            echo "<option value=".$id.">".$pos."</option>";
                        }
                    }
                ?>
            </select>
        </div>
    </div>
    
    <div class="form-group">
        <label for="id_dept" class="col-xs-2 control-label">Отдел:</label>
        <div class="col-xs-6">
            <select class="form-control" id="id_dept" name = "id_dept">
                <option value="1">Неизвестно</option>
                <?php 
                    unset($data['depts'][1]);
                    asort($data['depts']);
                    foreach ($data['depts'] as $id=>$dept) {
                        if ($id == $data['user']['id_dept']) {
                            echo "<option selected value=".$id.">".$dept."</option>";
                        }
                        else {
                            echo "<option value=".$id.">".$dept."</option>";
                        }
                    }
                ?>
            </select>
        </div>
    </div>
    <!-- Футер модального окна -->
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
      <button type="submit" class="btn btn-primary">Сохранить изменения</button>
    </div>
</form>