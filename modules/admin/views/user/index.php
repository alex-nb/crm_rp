    <li role="presentation"><a href="../../../site/site/index">Назад</a></li> 
    <li role="presentation"><a class='open_modal pointer'>Создать пользователя</a></li> 
    <li role="presentation"><a href="../user/dept">Отделы</a></li>
    <li role="presentation"><a href="../user/position">Должности</a></li> 
    <li role="presentation"><a href="../premission/index">Права пользователей</a></li> 
</ul>
        <?= "<center>Пользователи</center>";?>
<div class="table-responsive">
        <table class="table table-striped table-bordered table-hover" id="table_user" width="100%">
`       <thead>
            <tr class="info">
                <th>ИД</th>
                <th>Логин</th>
                <th>Email</th>
                <th>Фамилия</th>
                <th>Имя</th>
                <th>Отчество</th>
                <th>Отдел</th>
                <th>Должность</th>
                <th>Пол</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
     <?php
        foreach ($data as $row) {
            if(isset($row['id'])) {
                echo "<tr>";
                echo "<td>".$row['id']."</td>"; 
                echo "<td>".$row['username']."</td>"; 
                echo "<td>".$row['email']."</td>"; 
                echo "<td>".$row['last_name'] ."</td>"; 
                echo "<td>".$row['first_name']."</td>"; 
                echo (isset($row['second_name']) ? "<td>".$row['second_name']."</td>" : "<td>-</td>"); 
                echo (isset($row['dept_name']) ? "<td>".$row['dept_name']."</td>" : "<td>-</td>"); 
                echo (isset($row['pos_name']) ? "<td>".$row['pos_name']."</td>" : "<td>-</td>"); 
                if ($row['sex'] == 2) { echo "<td>Женский</td>"; } elseif ($row['sex'] == 1) { echo "<td>Мужской</td>";}  else { echo "<td>Неизвестно</td>"; }
                echo "<td><span class='glyphicon glyphicon-pencil pointer' id='".$row['id']."' data-toggle='tooltip' title='Редактировать'></span>     "
                        ."<span class='glyphicon glyphicon-trash pointer' id='".$row['id']."' data-toggle='tooltip' title='Удалить'></span>     "
                        ."<span class='glyphicon glyphicon-lock pointer' id='".$row['id']."' data-toggle='tooltip' title='Права доступа'></span></td>";
                echo "</tr>";
            }                
        }
    ?>
        </tbody>
    </table>
</div>

<!-- HTML-код модального окна для создания/удаления-->
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
        <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">Пользователь</h4>
            </div>
        <!-- Основное содержимое модального окна -->
            <div class="modal-body" id="modal-body">
            </div>
        </div>
    </div>
</div>

<script>
$('#myModal').on('hidden.bs.modal', function () {
    $("#modal-body").html('');
});

$('.glyphicon-pencil').click(function() {
    var id_user = $(this).attr('id');
    $.ajax({
        url : "form",
        type: "POST",
        data: {id_user:id_user},
        success: function(html){
           $('#modal-body').append(html);
           $('#myModal').modal('show');
        }
    });   
});

$('.open_modal').click(function() {
    $.ajax({
        url : "form",
        type: "POST",
        data: {},
        success: function(html){
           $('#modal-body').append(html);
           $('#myModal').modal('show');
        }
    });   
});

$('.glyphicon-trash').click(function() {
    var id_user = $(this).attr('id');
    if (confirm("Подверждаете удаление пользователя?")) {
        $.ajax({
            url : "deleteUser",
            type: "POST",
            data: {id_user:id_user},
            success: function(data){  
               if (data == 1) {
                   data = "Удаление завершено";
                   alert(data);
                   location.reload();
               }
               else { alert(data); }
            }
        });   
    }
}); 

$('.glyphicon-lock').click(function() {
    var id_user = $(this).attr('id');
    $.ajax({
        url : "getPremission",
        type: "POST",
        data: {id_user:id_user},
        success: function(html){
           $('#modal-body').append(html);
           $('#myModal').modal('show');

        }
    });   
});

$(document).ready(function() {
    $('#table_user').DataTable({
        "columnDefs": [
            { "visible": false, "targets": 6 }
        ],
        fixedHeader: true,
        "order": [[ 6, 'asc' ]],
        "displayLength": 25,
        "language": {
            url: '//cdn.datatables.net/plug-ins/1.10.16/i18n/Russian.json'
        },
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
            api.column(6, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="10">'+group+'</td></tr>'
                    );
                    last = group;
                }
            } );
        }
    });
} );
</script>