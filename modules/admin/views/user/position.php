    <li role="presentation"><a href="../user/index">Назад</a></li> 
    <li role="presentation"><a href="../user/dept">Отделы</a></li>
</ul>
<?php
    echo "<center>Должности</center>";
?>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover"  id="table_pos" width="100%">
        <thead>
                <tr class="info">
                    <th>ИД</th>
                    <th>Наименование</th>
                </tr>
        </thead>
        <tbody>
             <?php
                while($row = $data->fetch()) {
                     echo "<tr>";
                     echo "<td>".$row->id."</td>"; 
                     echo "<td>".$row->name."</td>"; 
                     echo "</tr>";
                }
            ?>
        </tbody>
</table>
</div>

<script>
$(document).ready(function() {
    $('#table_pos').DataTable({
        fixedHeader: true,
        "order": [[ 0, 'asc' ]],
        "displayLength": 25,
        "language": {
            "processing": "Подождите...",
            "search": "Поиск:",
            "lengthMenu": "Показать _MENU_ записей",
            "zeroRecords": "Записи отсутствуют.",
            "loadingRecords": "Загрузка записей...",
            "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
            "infoEmpty": "Нет данных для отображения",
            "infoFiltered": "(отфильтровано из _MAX_ записей)",
            "emptyTable": "В таблице отсутствуют данные",
            "paginate": {
              "first": "Первая",
              "previous": "Предыдущая",
              "next": "Следующая",
              "last": "Последняя"
            },
            "aria": {
              "sortAscending": ": активировать для сортировки столбца по возрастанию",
              "sortDescending": ": активировать для сортировки столбца по убыванию"
            }
        },
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
    });
} );    
</script>    