<?php


class mainController  extends Controller {

//метод, вызывающий главную страницу данного блока
    function index() {
            $request = $_REQUEST;
            $queryUrl = 'https://'.$request['DOMAIN'].'/rest/user.current.json';
            $queryData = http_build_query(array(
                "auth" => $request['AUTH_ID']
            ));

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_POST => 1,
                CURLOPT_HEADER => 0,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $queryUrl,
                CURLOPT_POSTFIELDS => $queryData,
            ));

            $result = json_decode(curl_exec($curl), true);
            curl_close($curl);
            
            $this->view->generate('bitrix','productBlock/index.php', $request, $result);
        }
    } //End index()
