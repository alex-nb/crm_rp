<?php
/*
 * Контроллер для работы с контрагентами.
 * Код объекта 
 * @author aleks
 */
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/firms_directionModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/firms_equipmentModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/firms_equipment_specModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/firms_makingModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/firms_making_specModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/firms_out_contact_phoneModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/firms_out_contact_emailModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/firms_out_empsModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/firms_out_positionModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/firms_stockModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/firms_type_firmModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/firms_typeModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/directionModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/cityModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/firms_out_deptsModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/production_directionModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/production_nameModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/production_group_classModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/production_classesModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/production_charactisticModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/production_info_charModel.php";

class firmsController extends Controller{
    protected $firms_direction;
    protected $firms_equipment;
    protected $firms_equipment_spec;
    protected $firms_making;
    protected $firms_making_spec;
    protected $firms_out_contact_phone;
    protected $firms_out_contact_email;
    protected $firms_out_emps;
    protected $firms_out_position;
    protected $firms_out_depts;
    protected $firms_stock;
    protected $firms_type_firm;
    protected $firms_type;
    protected $firms;
    protected $direction;
    protected $city;
    protected $production_direction;
    protected $production_name;
    protected $production_group_class;
    protected $production_classes;
    protected $production_charactistic;
    protected $production_info_char;
            
    function __construct() {
        parent::__construct();
        $this->firms = new firmsModel;
        $this->firms_direction = new firms_directionModel;
        $this->firms_equipment = new firms_equipmentModel;
        $this->firms_equipment_spec = new firms_equipment_specModel;
        $this->firms_making = new firms_makingModel;
        $this->firms_making_spec = new firms_making_specModel;
        $this->firms_out_contact_phone = new firms_out_contact_phoneModel;
        $this->firms_out_contact_email = new firms_out_contact_emailModel;
        $this->firms_out_emps = new firms_out_empsModel;
        $this->firms_out_position = new firms_out_positionModel;
        $this->firms_stock = new firms_stockModel;
        $this->firms_type_firm = new firms_type_firmModel;
        $this->firms_type = new firms_typeModel;
        $this->direction = new directionModel;
        $this->city = new cityModel;
        $this->firms_out_depts = new firms_out_deptsModel;
        $this->production_direction = new production_directionModel;
        $this->production_name = new production_nameModel;
        $this->production_group_class = new production_group_classModel;
        $this->production_classes = new production_classesModel;
        $this->production_charactistic = new production_charactisticModel;
        $this->production_info_char = new production_info_charModel;
    }
    
    
    function index(){
        $data = $this->genInfoIndex();
        $this->view->generate('card','firms/index.php', $data);
    } // End index
 
//печать перечня товаров из продуктого блока
    function printProduction() {
        $data = NULL;
        if (isset($_POST['id_firm'])) {
            $data2 = $_POST['id_firm'];
            $data['info_firm'] = $this->genInfoIndex($data2);
            $data['contacts'] = $this->genInfoContact($data2);
            $data['addres'] = $this->genInfoStock($data2);
        }
        if (!empty($_POST['id_product'])) {
            $char = '';
            if (!empty($_POST['id_product']['table_take_other'])) {
                $char = '17, 18, 19, 20, 21, 22';
                $data['table']['take_other'] = [0=>'№', 1=>'Наименование', 22=>'У кого?', 17=>'Объем раз. поставки', 18=>'Период закупок', 19=>'Потребление/месяц', 20=>'Цена', 21=>'Комментарии'];
                $id_product_array = array_unique($_POST['id_product']['table_take_other']);
                $id_product = implode(",", $id_product_array);
                $all_char_d = $this->production_info_char->GetAllRows("id_prod IN(".$id_product.") AND id_firm=".$data2." AND id_char IN(".$char.")");
                foreach ($all_char_d as $row) {
                    if ($row->id_char != 22) {
                        $data['take_other']['product_char'][$row->id_prod][$row->id_char] = $row->value;
                    }
                    else {
                        $name_firm = $this->firms->GetRowById($row->value);
                        foreach ($name_firm as $vrag) {
                             $data['take_other']['product_char'][$row->id_prod][$row->id_char] = $vrag->name;
                             break;
                        }
                    }
                }
                $product_name = $this->production_name->GetAllRows("id IN(".$id_product.")");
                foreach ($product_name as $row) {
                    $data['take_other']['product_char'][$row->id][0] = $row->id;
                    $data['take_other']['product_char'][$row->id][1] = $row->name;
                }
            }
            if (!empty($_POST['id_product']['table_offer'])) {
                $char = '21';
                $data['table']['offer'] = [0=>'№', 1=>'Наименование', 21=>'Комментарии'];
                $id_product_array = array_unique($_POST['id_product']['table_offer']);
                $id_product = implode(",", $id_product_array);
                $all_char_d = $this->production_info_char->GetAllRows("id_prod IN(".$id_product.") AND id_firm=".$data2." AND id_char IN(".$char.")");
                foreach ($all_char_d as $row) {
                    $data['offer']['product_char'][$row->id_prod][$row->id_char] = $row->value;
                }
                $product_name = $this->production_name->GetAllRows("id IN(".$id_product.")");
                foreach ($product_name as $row) {
                    $data['offer']['product_char'][$row->id][0] = $row->id;
                    $data['offer']['product_char'][$row->id][1] = $row->name;
                }
            }
            if (!empty($_POST['id_product']['table_take_us'])) {
                $char = '17, 18, 19, 20, 21';
                $data['table']['take_us'] = [0=>'№', 1=>'Наименование', 17=>'Объем раз. поставки', 18=>'Период закупок', 19=>'Потребление/месяц', 20=>'Цена', 21=>'Комментарии'];
                $id_product_array = array_unique($_POST['id_product']['table_take_us']);
                $id_product = implode(",", $id_product_array);
                $all_char_d = $this->production_info_char->GetAllRows("id_prod IN(".$id_product.") AND id_firm=".$data2." AND id_char IN(".$char.")");
                foreach ($all_char_d as $row) {
                    $data['take_us']['product_char'][$row->id_prod][$row->id_char] = $row->value;
                }
                $product_name = $this->production_name->GetAllRows("id IN(".$id_product.")");
                foreach ($product_name as $row) {
                    $data['take_us']['product_char'][$row->id][0] = $row->id;
                    $data['take_us']['product_char'][$row->id][1] = $row->name;
                }
            }
            if (!empty($_POST['id_product']['table_dont_take'])) {
                $char = '16, 21';
                $data['table']['dont_take'] = [0=>'№', 1=>'Наименование', 16=>'Тип', 21=>'Комментарии'];
                $id_product_array = array_unique($_POST['id_product']['table_dont_take']);
                $id_product = implode(",", $id_product_array);
                $all_char_d = $this->production_info_char->GetAllRows("id_prod IN(".$id_product.") AND id_firm=".$data2." AND id_char IN(".$char.")");
                foreach ($all_char_d as $row) {
                    $data['dont_take']['product_char'][$row->id_prod][$row->id_char] = $row->value;
                }
                $product_name = $this->production_name->GetAllRows("id IN(".$id_product.")");
                foreach ($product_name as $row) {
                    $data['dont_take']['product_char'][$row->id][0] = $row->id;
                    $data['dont_take']['product_char'][$row->id][1] = $row->name;
                }
            }
            unset($all_char_d);
            unset($product_name);
        }
        $this->view->generateForm('card', 'firms', '_form_for_print', $data, $data2);
    }

//производство фирмы    
    function making() {
        $making = NULL;
        $making_d = $this->firms_making->GetAllRows();
       foreach ($making_d as $row) {
           $making[$row->id] = (array) $row;
       } //End foreach
        $this->view->generate('card','firms/making.php', $making);
     }

//уровень оснащения фирмы    
    function equipment() {
        $equipment = NULL;
        $equipment_d = $this->firms_equipment->GetAllRows();
       foreach ($equipment_d as $row) {
           $equipment[$row->id] = (array) $row;
       } //End foreach
        $this->view->generate('card','firms/equipment.php', $equipment);
     }     
    
//редактирование основной информации по фирмам    
    function edit($id = 0){
        if (isset($_POST['id_firm'])) {
             $id = $_POST['id_firm'];
         }
       $data2 = $id;
       $data = $this->genInfoEdit($id);
       $this->view->generate('card', 'firms/_form_edit_geninfo.php', $data, $data2);
     }
     
//вызов формы для редактирвоания контактов     
    function editContact($id_firm = 0){
        if (isset($_POST['id_firm'])) {
            $id_firm = $_POST['id_firm'];  
            $data3 = $_POST['name'];
            if (isset($_POST['inn'])) {$values = $_POST; $this->saveGenInfo($values);}
       }
        $data = $this->genInfoContact($id_firm);
        $this->view->generate('card', 'firms/_form_edit_contact.php', $data, $id_firm, $data3);
     }
     
//вызов формы для редактирования доп инфы
    function editDopInfo($id_firm = 0){
        $file = 'firms/_form_edit_dopinfo.php';
        if (isset($_POST['id_firm'])) {
            $id_firm = $_POST['id_firm'];  
            $data3 = $_POST['name'];
       }
        $data = $this->genInfoDop($id_firm);
        $this->view->generate('card', $file, $data, $id_firm, $data3);
    }
   
//вызов формы для редактирования адресов складов     
    function editStock($id_firm = 0) {
        if (isset($_POST['id_firm'])) {
            $id_firm = $_POST['id_firm']; 
            $data3 = $_POST['name'];
        }
        $data = $this->genInfoStock($id_firm);
        $types = $this->firms_type_firm->GetAllRows("id_firm=".$id_firm);
        foreach ($types as $row) {
            $data['types'][] = $row->id_type;
        }
        $this->view->generate('card', 'firms/_form_edit_stock.php', $data, $id_firm, $data3);
    }
    
//вызов формы для редактирования продуктового блока    
    function editProduction($id_firm = 0){
        if (isset($_POST['id_firm'])) {
            $id_firm = $_POST['id_firm']; 
            $data3 = $_POST['name'];
            if (isset($_POST['direction']) || isset($_POST['making']) || isset($_POST['equipment'])) {$values = $_POST; $this->saveDopInfo($values);}
        }
        $data = $this->genInfoProd($id_firm);
        $this->view->generate('card', 'firms/_form_edit_product.php', $data, $id_firm, $data3);
    }
          
//добавление/редактирование производств для фирм
    function editMaking() {
        $locate = $_SERVER['HTTP_REFERER'];
        if (isset($_POST['editType']) && !empty($_POST['name'])) {
            $making = new firms_makingModel;
            if((int)$_POST['editType'] === 1) {
                $making->name = $_POST['name'];
                $making->id_parrent = $_POST['id'];
                $making->Save();
            }
            else {
                if((int)$_POST['editType'] === 0) {
                    $making->name = $_POST['name'];
                    $making->Update($_POST['id']);
                }
            }
            unset($making);
        }
        echo "<script>window.location.href='".$locate."';</script>";
     }

//добавление/редактирование уровней оснащений для фирм
    function editEquipment() {
        $locate = $_SERVER['HTTP_REFERER'];
        if (isset($_POST['editType']) && !empty($_POST['name'])) {
            $equipment = new firms_equipmentModel;
            if((int)$_POST['editType'] === 1) {
                $equipment->name = $_POST['name'];
                $equipment->id_parrent = $_POST['id'];
                $equipment->Save();
            }
            else {
                if((int)$_POST['editType'] === 0) {
                    $equipment->name = $_POST['name'];
                    $equipment->Update($_POST['id']);
                }
            }
            unset($equipment);
        }
        echo "<script>window.location.href='".$locate."';</script>";
     }

//добавление нового контрагента - конкурента     
     function newVrag() {
         $vrag = new firmsModel;
         if (isset($_POST['name']) && isset($_POST['inn'])) {
             $fields = array_keys($vrag->fieldsTable());
            foreach ($fields as $key) {
                 if (isset($_POST[$key]) && $key != 'id') {
                       $vrag->$key = $_POST[$key];
                   } //End if
            }
            $id = $vrag->Save();
            $firm_type = new firms_type_firmModel;
            $firm_type->id_firm = $id;
            $firm_type->id_type = 5;
            $firm_type->Save();
            unset($vrag);
            unset($firm_type);
            echo $id;
         }
         else {
             echo "Oops";
         }
     }
     
//перенос продукта в продуктовом блоке     
     function transferenceProduct() {
         if (!empty($_POST['id_firm'])) {$id_firm = $_POST['id_firm'];}
         else {$id_firm = NULL;}
         
         if(!empty($_POST['id_prod']) && !empty($id_firm)) {
             $id_product = $_POST['id_prod'];
             $chars_array = [16=>"type",17=>"v",18=>"zakup",19=>"potr",20=>"price",21=>"komment",22=>"vrag"];
             foreach ($chars_array as $id_char=>$char) {
                 if (!empty($_POST[$char])) {
                     $id_row = NULL;
                     $row = $this->production_info_char->GetAllRows("id_prod=".$id_product." AND id_char=".$id_char."  AND id_firm=".$id_firm);
                     foreach ($row as $r) {
                         $id_row = $r->id;
                     }
                     if ($id_row !== NULL) {
                         if ($char !== "type") {
                                $product_char = new production_info_charModel;
                                $product_char->value = $_POST[$char];
                                $product_char->Update($id_row);
                         }
                        else {
                            $_POST[$char] == 1 ? $type = "На выявление потребностей": $type = "На продвижение";
                            $product_char = new production_info_charModel;
                            $product_char->value = $type;
                            $product_char->Update($id_row);               
                        }
                     } 
                 }
             }
         }
         else {
             $chars_array = [15=>"status",16=>"type",17=>"v",18=>"zakup",19=>"potr",20=>"price",21=>"komment",22=>"vrag"];
             if (!empty($_POST['prod']) && !empty($_POST['where']) && !empty($id_firm)) {
                 foreach ($_POST['prod'] as $prod) {
                     foreach ($chars_array as $id_char=>$char) {
                        $id_row = NULL;
                         if ($id_char == 15) {
                            $row = $this->production_info_char->GetAllRows("id_prod=".$prod." AND id_char=".$id_char."  AND id_firm=".$id_firm);
                            foreach ($row as $r) {
                                $id_row = $r->id;
                            }
                            $where = "Предложение";
                            if ($_POST['where'] == "donttake") {$where = "Не берут";}
                            if ($_POST['where'] == "takeus") {$where = "Берут у нас";}
                            if ($_POST['where'] == "takeother") {$where = "Берут не у нас";}
                            $product_char = new production_info_charModel;
                            $product_char->value = $where;
                             if ($id_row !== NULL) {$product_char->Update($id_row);}
                            else {
                                $product_char->id_char = $id_char;
                                $product_char->id_prod = $prod;
                                $product_char->id_firm = $id_firm;
                                $product_char->Save();
                            }
                        }
                        else {
                            if (!empty($_POST[$char])) {
                                $row = $this->production_info_char->GetAllRows("id_prod=".$prod." AND id_char=".$id_char."  AND id_firm=".$id_firm);
                                foreach ($row as $r) {
                                    $id_row = $r->id;
                                }
                                if ($char !== "type") {
                                    $product_char = new production_info_charModel;
                                    $product_char->value = $_POST[$char];
                                    if ($id_row !== NULL) {$product_char->Update($id_row);}                              
                                }
                               else {
                                    $_POST[$char] == 1 ? $type = "На выявление потребностей": $type = "На продвижение";
                                    $product_char = new production_info_charModel;
                                    $product_char->value = $type;
                               }
                                if ($id_row !== NULL) {$product_char->Update($id_row);}
                                else {
                                    $product_char->id_char = $id_char;
                                    $product_char->id_prod = $prod;
                                    $product_char->id_firm = $id_firm;
                                    $product_char->Save();
                                }
                            }
                        }                     
                     }               
                 }
             }
         }
        unset($product_char);
        $data = $this->genInfoProd($id_firm);
        $this->view->generateForm('card', 'firms', '_form_for_new_edit', $data, $id_firm);
     }
    
//вызов формы для создания контакта    
    function formCreareContact(){
         $id_emp = 0;
         $id_firm = 0;
         $out_depts_d = $this->firms_out_depts->GetAllRows();
        foreach ($out_depts_d as $row) {
             $data['all_depts'][$row->id] = $row->name;
         }
        $out_positions_d = $this->firms_out_position->GetAllRows();
        foreach ($out_positions_d as $row) {
             $data['all_positions'][$row->id] = $row->name;
         }   
         if (isset($_POST['id_contact'])) {
             $id_emp = $_POST['id_contact'];
             $emp = $this->firms_out_emps->GetRowById($id_emp);
             foreach ($emp as $row) {
                 $data[$row->id] = (array) $row;
             }
         }
         if (isset($_POST['id_firm'])) {
             $id_firm = $_POST['id_firm'];
         }
         if (isset($_POST['name'])) {
             $data['name'] = $_POST['name'];
         }
         $this->view->generateForm('card', 'firms', '_form_create_contact', $data, $id_emp, $id_firm);
     }
     
   //вызов формы для создания конкурента  
     function formNewFirm() {
         $city_d = $this->city->GetAllRows();
         foreach ($city_d as $row) {
             $city[$row->id] = $row->name;
         }
         unset($city_d);
         $this->view->generateForm('card', 'firms', '_form_new_firm', $city);
     }
     
//вызов формы для создания адреса склада     
    function formCreateStock(){
        $data = NULL;
        
        $data['types'] = array ();
    //доп. массив для городов
        $city_d = $this->city->GetAllRows();
        foreach ($city_d as $row) {
            $data['all_city'][$row->id] = $row->name;
        }
        if (isset($_POST['id_stock'])) {
            $id_stock = $_POST['id_stock'];
            $stocks = $this->firms_stock->GetRowById($id_stock);
            foreach ($stocks as $row) {
                $data[$row->id] = (array) $row;
            } 
        }
        if (isset($_POST['id_firm'])) {
             $id_firm = $_POST['id_firm'];
         }
         if (isset($_POST['name'])) {
             $data['name'] = $_POST['name'];
         }
         $this->view->generateForm('card', 'firms', '_form_create_stock', $data, $id_stock, $id_firm);        
    } 
           
//вызов формы для переноса или редактирования продукта
    function formCheckOut() {
        $where = NULL;
        $form = NULL;
        $info = NULL;
        $id_prod = NULL;
        $id_firm = NULL;
        if (isset($_POST['id_prod']) && isset($_POST['id_firm'])) {
            $id_prod = $_POST['id_prod'];
            $id_firm = $_POST['id_firm'];
            $info = $this->genInfoProd($id_firm, $id_prod);
            //var_dump($info);
        }
        if (!empty($_POST['id_firm'])) {
            $id_firm = $_POST['id_firm'];
        }
        if(isset($_POST['where'])) {
            $where = $_POST['where'];
            $info['where'] = $where;
        }
        if(isset($_POST['from'])) {
            $from = $_POST['from'];
            $info['from'] = $from;
        }
        switch ($where) {
            case "donttake":
                $form = '_form_dont_take';
                break;
            case "takeus":
                $form = '_form_take_us';
                break;
            case "takeother":
                $form = '_form_take_other';
                $firms_id = $this->firms_type_firm->GetAllRows("id_type=5");
                foreach ($firms_id as $id_f) {
                    $firm = $this->firms->GetRowById($id_f->id_firm);
                    foreach ($firm as $row) {
                        $info['all_firms'][$row->id] = (array) $row;
                    }
                    unset ($firm);
                }
                unset($firms_id);
                break;
            case "offer":
                $form = '_form_offer';
                break;
        }
        $this->view->generateForm('card', 'firms', $form, $info, $id_prod, $id_firm);
    }

//запись в БД контактов для фирмы    
    function saveContact(){
         $id = 0;
         $emp = new firms_out_empsModel;
         $fields = array_keys($emp->fieldsTable());
         foreach ($fields as $key) {
              if (isset($_POST[$key]) && $key != 'id') {
                    $emp->$key = $_POST[$key];
                } //End if
         }
         if (!empty($_POST['id'])) {
             $id = $_POST['id'];
             $emp->Update($id);
         }
         else {
             $id = $emp->Save();
         }
         $this->editContact($_POST['id_firm']);
         unset($emp);
     }
  
//запись в БД адресов хозяйств/складов     
     function saveStock(){
         $id = 0;
         $stock = new firms_stockModel;
         $fields = array_keys($stock->fieldsTable());
         foreach ($fields as $key) {
              if (isset($_POST[$key]) && $key != 'id') {
                    $stock->$key = $_POST[$key];
                } //End if
         }
          if (!empty($_POST['id'])) {
             $id = $_POST['id'];
             $stock->Update($id);
         }
         else {
             $id = $stock->Save();
         }
         $this->editStock($_POST['id_firm']);
         unset($stock);
     }
     
//запись в БД дополнительной инфы о фриме
    function saveDopInfo($values){
        if (isset($values['id_firm'])) {
            $f_id = $values['id_firm'];
            $del_dir = $del_make = $del_eq = array();                
            $del_dir = $this->getDirection($f_id);
            $del_make = $this->getMaking($f_id);
            $del_eq = $this->getEquipment($f_id);     

            $new_dir = array ();
            if (!empty($values['direction'])) {
                $new_dir = $values['direction'];
            } //End if
            $new_make = array ();
            if (!empty($values['making'])) {
                $new_make = $values['making'];
            } //End if
            $new_equip = array ();
            if (!empty($values['equipment'])) {
                $new_equip = $values['equipment'];
            } //End if

            $donttouch_dir = array_intersect($del_dir, $new_dir); 
            foreach ($donttouch_dir as $value) {
                if (in_array($value, $new_dir)) { unset($new_dir[array_search($value, $new_dir)]);}
                if (in_array($value, $del_dir)) { unset($del_dir[array_search($value, $del_dir)]); }
            }
            if (!empty($del_dir)) {
                foreach ($del_dir as $key=>$value) { $this->firms_direction->DeleteRow($key); }
            }
            if (!empty($new_dir)){
                 foreach ($new_dir as $value) {
                    $f_dir = new firms_directionModel;
                    $f_dir->id_direction = $value;
                    $f_dir->id_firm = $f_id;
                    $f_dir->Save();
                    unset($f_dir);
                }
            }
            $donttouch_make = array_intersect($del_make, $new_make); 
            foreach ($donttouch_make as $value) {
                if (in_array($value, $new_make)) { unset($new_make[array_search($value, $new_make)]);}
                if (in_array($value, $del_make)) { unset($del_make[array_search($value, $del_make)]); }
            }
            if (!empty($del_make)) {
                foreach ($del_make as $key=>$value) { 
                   $this->firms_making_spec->DeleteRow($key); }
            }
            if (!empty($new_make)){
                 foreach ($new_make as $value) {
                    $f_make = new firms_making_specModel;
                    $f_make->id_make = $value;
                    $f_make->id_firm = $f_id;
                    $f_make->Save();
                    unset($f_make);
                }
            }
            $donttouch_equip= array_intersect($del_eq, $new_equip); 
            foreach ($donttouch_equip as $value) {
                if (in_array($value, $new_equip)) { unset($new_equip[array_search($value, $new_equip)]);}
                if (in_array($value, $del_eq)) { unset($del_eq[array_search($value, $del_eq)]); }
            }
            if (!empty($del_eq)) {
                foreach ($del_eq as $key=>$value) { $this->firms_equipment_spec->DeleteRow($key); }
            }
            if (!empty($new_equip)){
                 foreach ($new_equip as $value) {
                    $f_equip = new firms_equipment_specModel;
                    $f_equip->id_equip = $value;
                    $f_equip->id_firm = $f_id;
                    $f_equip->Save();
                    unset($f_equip);
                }
            }
         }
    }
     
//запись в БД по основной информации по фирме             
     function saveGenInfo($values){
         if (isset($values['id_firm'])) {
            $del_type = array();
            $firm = new firmsModel;
            $fields = array_keys($firm->fieldsTable());

            foreach ($fields as $key) {
               if (isset($values[$key]) && $key != 'id_firm') {
                    $firm->$key = $values[$key];
                } //End if
            } //End foreach

            if ($values['id_firm'] != 0) {
                $f_id = $values['id_firm'];
                $firm->Update($values['id_firm']);
                $del_dir = $this->getDirection($f_id);
                $del_make = $this->getMaking($f_id);
                $del_eq = $this->getEquipment($f_id);
                $del_type = $this->getTypes($f_id);
            } //End if         
            else {
                $f_id = $firm->Save();
            } //End else

            $new_type = array();
            if(!empty($values['types'])) {
                $new_type = $values['types'];
            }
            
            $donttouch_types= array_intersect($del_type, $new_type); 
            foreach ($donttouch_types as $value) {
                if (in_array($value, $new_type)) { unset($new_type[array_search($value, $new_type)]);}
                if (in_array($value, $del_type)) { unset($del_type[array_search($value, $del_type)]); }
            }
            if (!empty($del_type)) {
                foreach ($del_type as $key=>$value) { $this->firms_type_firm->DeleteRow($key); }
            }
            if (!empty($new_type)){
                 foreach ($new_type as $value) {
                    $f_type = new firms_type_firmModel;
                    $f_type->id_type = $value;
                    $f_type->id_firm = $f_id;
                    $f_type->Save();
                    unset($f_type);
                }
            }
         }
         unset($firm);
     }
     
//генрация информации для заглавной страницы
     function genInfoIndex ($id=NULL) {
         if (!empty($id)) {
             $select = 'id_firm='.$id;
         }
         else {
             $select = NULL;
        }
            $gen_info = array ();
            
            $making = $equipment =  $direction = $city = NULL;
            $firms_type = $firm_direction = $firm_equip = $firm_making = NULL;
    //доп. массив для городов
            $city_d = $this->city->GetAllRows();
            foreach ($city_d as $row) {
                $city[$row->id] = $row->name;
            }
    //доп. массив для направлений
            $direction_d = $this->direction->GetAllRows();
            foreach ($direction_d as $row) {
                $direction[$row->id] = (array) $row;
            }
    //доп. массив для типов фирм (всех)        
            $firms_type_d = $this->firms_type->GetAllRows();
             foreach ($firms_type_d as $row) {
                 $firms_type[$row->id] = $row->name;
             }
    //доп. массив для что производят
            $making_d = $this->firms_making->GetAllRows();
            foreach ($making_d as $row) {
                $making[$row->id] = (array) $row;
            }
    //доп. массив для уровня оснащения
            $equipment_d = $this->firms_equipment->GetAllRows();
            foreach ($equipment_d as $row) {
                $equipment[$row->id] = (array) $row;
            }
    //массив по направлениям по фирмам
           $firm_direction_d = $this->firms_direction->GetAllRows($select);
           foreach ($firm_direction_d as $row) {
               $firm_direction[$row->id_firm][] = $row->id_direction;
           }
    //массив по уровню оснащения по фирмам
           $firm_equip_d = $this->firms_equipment_spec->GetAllRows($select);
           foreach ($firm_equip_d as $row) {
               $firm_equip[$row->id_firm][] = $row->id_equip;
           }
    //массив по производству по фирмам
           $firm_making_d = $this->firms_making_spec->GetAllRows($select);
           foreach ($firm_making_d as $row) {
               $firm_making[$row->id_firm][] = $row->id_make;
           }         
    //заполнение основной инфы по фирмам         
            $firms_d = $this->firms->GetRowById($id); //вернет все строки, если id NULL
            foreach ($firms_d as $row) {
               $gen_info[$row->id] = (array) $row;
               if (!empty($row->id_legal_city)) { $gen_info[$row->id]['name_legal_city'] = $city[$row->id_legal_city];}
               if (!empty($row->id_fact_city)) { $gen_info[$row->id]['name_fact_city'] = $city[$row->id_fact_city];}
                if(!empty($firm_direction) && isset($firm_direction[$row->id])) {
                    foreach ($firm_direction[$row->id] as $key=>$row_ds) {
                        $gen_info[$row->id]['direction'][$key][$row_ds] = $direction[$row_ds]['name'];
                        $first = $row_ds;
                        while (!empty($direction[$first]['id_parrent'])) {
                            $first = $direction[$first]['id_parrent'];
                            $gen_info[$row->id]['direction'][$key][$first] = $direction[$first]['name'];
                        } 
                    }
                }
                if(!empty($firm_equip) && isset($firm_equip[$row->id])) {
                    foreach ($firm_equip[$row->id] as $key=>$row_ds) {
                        $gen_info[$row->id]['equipment'][$key][$row_ds] = $equipment[$row_ds]['name'];
                        $first = $row_ds;
                        while (!empty($equipment[$first]['id_parrent'])) {
                            $first = $equipment[$first]['id_parrent'];
                            $gen_info[$row->id]['equipment'][$key][$first] = $equipment[$first]['name'];
                        } 
                    }
                }
                if(!empty($firm_making) && isset($firm_making[$row->id])) {
                    foreach ($firm_making[$row->id] as $key=>$row_ds) {
                        $gen_info[$row->id]['making'][$key][$row_ds] = $making[$row_ds]['name'];
                        $first = $row_ds;
                        while (!empty($making[$first]['id_parrent'])) {
                            $first = $making[$first]['id_parrent'];
                            $gen_info[$row->id]['making'][$key][$first] = $making[$first]['name'];
                        } 
                    }
                }
            }
     //заполнение какие типы фирм присущи одной конкректной       
            $firms_type_firm_d = $this->firms_type_firm->GetAllRows($select);
            foreach ($firms_type_firm_d as $row) {
                $gen_info[$row->id_firm]['types'][$row->id_type] = $firms_type[$row->id_type];
            }
             
         //}      
        return $gen_info;
     }
     
//генерация информации для страницы редактирования основной информации     
     function genInfoEdit ($id = NULL) {
        $gen_info = array ();
        $select = NULL;
        $city = NULL;
        $firms_type = NULL;

//доп. массив для городов
        $city_d = $this->city->GetAllRows();
        foreach ($city_d as $row) {
            $city[$row->id] = $row->name;
        }
//доп. массив для типов фирм (всех)        
        $firms_type_d = $this->firms_type->GetAllRows();
         foreach ($firms_type_d as $row) {
             $firms_type[$row->id] = $row->name;
         }

        $gen_info['all_city'] = $city;
        $gen_info['all_types'] = $firms_type;

        if ($id !== NULL) {
            $select='id_firm='.$id;
    //заполнение основной инфы по фирмам         
            $firms_d = $this->firms->GetRowById($id); //вернет все строки, если id NULL
            foreach ($firms_d as $row) {
               $gen_info[$row->id] = (array) $row;
               if (!empty($row->id_legal_city)) { $gen_info[$row->id]['name_legal_city'] = $city[$row->id_legal_city];}
               if (!empty($row->id_fact_city)) { $gen_info[$row->id]['name_fact_city'] = $city[$row->id_fact_city];}
            }
    //заполнение какие типы фирм присущи одной конкректной       
           $firms_type_firm_d = $this->firms_type_firm->GetAllRows($select);
           foreach ($firms_type_firm_d as $row) {
               $gen_info[$row->id_firm]['types'][$row->id_type] = $firms_type[$row->id_type];
           }
        }
        return $gen_info;
     }
     
//генерация информации для страницы редактирования дополнительно информации
    function genInfoDop ($id = NULL) {
        $gen_info = array ();
        $select = NULL;
        $making = $equipment =  $direction = NULL;
        $firm_direction = $firm_equip = $firm_making = NULL;

//доп. массив для направлений
        $direction_d = $this->direction->GetAllRows();
        foreach ($direction_d as $row) {
            $direction[$row->id] = (array) $row;
        }
//доп. массив для что производят
        $making_d = $this->firms_making->GetAllRows();
        foreach ($making_d as $row) {
            $making[$row->id] = (array) $row;
        }
//доп. массив для уровня оснащения
        $equipment_d = $this->firms_equipment->GetAllRows();
        foreach ($equipment_d as $row) {
            $equipment[$row->id] = (array) $row;
        }

        $gen_info['all_direction'] = $direction;
        $gen_info['all_making'] = $making;
        $gen_info['all_equipment'] = $equipment;

        if ($id !== NULL) {
            $select='id_firm='.$id;
    //массив по направлениям по фирмам
           $firm_direction_d = $this->firms_direction->GetAllRows($select);
           foreach ($firm_direction_d as $row) {
               $firm_direction[$row->id_firm][] = $row->id_direction;
           }
    //массив по уровню оснащения по фирмам
           $firm_equip_d = $this->firms_equipment_spec->GetAllRows($select);
           foreach ($firm_equip_d as $row) {
               $firm_equip[$row->id_firm][] = $row->id_equip;
           }
    //массив по производству по фирмам
           $firm_making_d = $this->firms_making_spec->GetAllRows($select);
           foreach ($firm_making_d as $row) {
               $firm_making[$row->id_firm][] = $row->id_make;
           }         
    //заполнение основной инфы по фирмам         
            $firms_d = $this->firms->GetRowById($id); //вернет все строки, если id NULL
            foreach ($firms_d as $row) {
                if(!empty($firm_direction) && isset($firm_direction[$row->id])) {
                    foreach ($firm_direction[$row->id] as $key=>$row_ds) {
                        $gen_info[$row->id]['direction'][$key][$row_ds] = $direction[$row_ds]['name'];
                        $first = $row_ds;
                        while (!empty($direction[$first]['id_parrent'])) {
                            $first = $direction[$first]['id_parrent'];
                            $gen_info[$row->id]['direction'][$key][$first] = $direction[$first]['name'];
                        } 
                    }
                }
                if(!empty($firm_equip) && isset($firm_equip[$row->id])) {
                    foreach ($firm_equip[$row->id] as $key=>$row_ds) {
                        $gen_info[$row->id]['equipment'][$key][$row_ds] = $equipment[$row_ds]['name'];
                        $first = $row_ds;
                        while (!empty($equipment[$first]['id_parrent'])) {
                            $first = $equipment[$first]['id_parrent'];
                            $gen_info[$row->id]['equipment'][$key][$first] = $equipment[$first]['name'];
                        } 
                    }
                }
                if(!empty($firm_making) && isset($firm_making[$row->id])) {
                    foreach ($firm_making[$row->id] as $key=>$row_ds) {
                        $gen_info[$row->id]['making'][$key][$row_ds] = $making[$row_ds]['name'];
                        $first = $row_ds;
                        while (!empty($making[$first]['id_parrent'])) {
                            $first = $making[$first]['id_parrent'];
                            $gen_info[$row->id]['making'][$key][$first] = $making[$first]['name'];
                        } 
                    }
                }
            }
        }
        return $gen_info;
    }

//генерация информации контактов по фирме     
     function genInfoContact ($id = NULL) {
        $gen_info = array ();
        $select = NULL;
        $out_depts = $out_positions = $out_email =  $out_phone = NULL;
//доп. массив для отделов в сторонних организациях         
        $out_depts_d = $this->firms_out_depts->GetAllRows();
        foreach ($out_depts_d as $row) {
             $out_depts[$row->id] = $row->name;
         }
//доп. массив для должностей в сторонних организациях               
        $out_positions_d = $this->firms_out_position->GetAllRows();
        foreach ($out_positions_d as $row) {
             $out_positions[$row->id] = $row->name;
         } 
        $gen_info['all_depts'] = $out_depts;
        $gen_info['all_positions'] = $out_positions;
        
        if ($id !== NULL) {
            $select='id_firm='.$id;
    //заполняем контактные данные для каждой фирмы        
            $out_emps_d = $this->firms_out_emps->GetAllRows($select);
            foreach ($out_emps_d as $row) {
                $gen_info[$row->id_firm]['emps'][$row->id] = (array) $row;
                $gen_info[$row->id_firm]['emps'][$row->id]['dept'] = $out_depts[$row->id_out_dept];
                $gen_info[$row->id_firm]['emps'][$row->id]['position'] = $out_positions[$row->id_out_pos];
            }
        }
        return $gen_info;
     }
     
//генерация информации по хозяйствам (складам) для фирмы     
     function genInfoStock ($id = NULL) {
         $gen_info = array ();
        $select = NULL;
        $city = NULL;

//доп. массив для городов
        $city_d = $this->city->GetAllRows();
        foreach ($city_d as $row) {
            $city[$row->id] = $row->name;
        }
        $gen_info['all_city'] = $city;
        if ($id !== NULL) {
            $select='id_firm='.$id;
    //заполнение адресов хозяйств/складов        
            $firms_stock_d = $this->firms_stock->GetAllRows($select);
            foreach ($firms_stock_d as $row) {
                $gen_info[$row->id_firm]['stocks'][$row->id] = (array) $row;
                $gen_info[$row->id_firm]['stocks'][$row->id]['name_city'] = $city[$row->id_city];
            }
        }
        return $gen_info;
     }
    
/*генерация инфомрации по продукции для конкректной фирмы (покупатель)    
 * ID необходимых характеристик:
 * 15 - статус (берут у нас/берут не у нас/не берут/предложение)
 * 16 - частота проверки (на выявление потребностей (раз в год)/на продвижение (раз в полгода))
 * 17 - объем размера поставки (участвует в берут у нас/берут не у нас)
 * 18 - период закупки  (участвует в берут у нас/берут не у нас)
 * 19 - потребление/месяц (участвует в берут у нас/берут не у нас)
 * 20 - цена (участвует в берут у нас/берут не у нас)
 * 21 - комментарий (участвует в берут у нас/берут не у нас)
 * 22 - название поставщика (участвует в берут не у нас)
 */ 
     function genInfoProd($id_firm, $id_prod = NULL) {
         $chars_array = [15=>"status",16=>"type",17=>"v",18=>"zakup",19=>"potr",20=>"price",21=>"komment",22=>"vrag"];
         $chars = implode(",", array_keys($chars_array));
         $products = array();
         $prod_id = array();
         $prod_info_char = array();
         $prod = array();
         
         if(!empty($id_prod)) {
             $products_char_d = $this->production_info_char->GetAllRows('id_char IN ('.$chars.') AND id_firm='.$id_firm.' AND id_prod='.$id_prod);
             foreach ($products_char_d as $row) {
                 if ($row->id_char == '22') {
                     $firm = $this->firms->GetRowById((int) $row->value);
                     foreach ($firm as $f) {
                         $products[$id_prod][$chars_array[$row->id_char]] = $f->name;
                     }
                     unset($firm);
                 }
                 else {
                     $products[$id_prod][$chars_array[$row->id_char]] = $row->value;
                 }
                 
             }
             unset($products_char_d);
         }
         else {
            $firm_direction = implode(",",$this->getDirection($id_firm));
            $product_name= NULL;
            $product_name_d = $this->production_name->GetAllRows();
            foreach ($product_name_d as $row) {
                $product_name[$row->id] = $row->name;
            }
            $group_class_d = $this->production_group_class->GetAllRows();
           foreach ($group_class_d as $row) {
                $products['all_class'][$row->id] = (array) $row;
                if($row->id_parrent != NULL) {
                    $products['id_parrent'][] = $row->id_parrent;
                }
           } //End foreach

           $prod_class_d = $this->production_classes->GetAllRows();
           foreach ($prod_class_d as $row) {
               $prod_class[$row->id_prod][] = $row->id_class;
           } //End foreach
           $products_d = $this->production_info_char->GetAllRows('id_char IN ('.$chars.') AND id_firm='.$id_firm);
           foreach ($products_d as $row){
               $prod_info_char[$row->id_prod][$row->id_char] = $row->value;
               $prod_id[] = $row->id_prod;
               $prod[] = (array) $row;
           }
           foreach ($prod as $row) {
               if($row['id_char'] == 15) {
                   if(isset($prod_class[$row['id_prod']])) {
                       foreach ($prod_class[$row['id_prod']] as $pc) {
                           $products[$row['value']]['in_class'][$pc][$row['id_prod']]['id'] = $row['id_prod'];
                           $products[$row['value']]['in_class'][$pc][$row['id_prod']]['name'] = $product_name[$row['id_prod']];
                           foreach ($chars_array as $id_ch=>$name) {
                               if(isset($prod_info_char[$row['id_prod']][$id_ch])) {
                                   if ($id_ch == '22') {
                                        $firm = $this->firms->GetRowById((int) $prod_info_char[$row['id_prod']][$id_ch]);
                                        foreach ($firm as $f) {
                                            $products[$row['value']]['in_class'][$pc][$row['id_prod']]['char'][$name]  = $f->name;
                                        }
                                        unset($firm);
                                    }
                                    else {
                                         $products[$row['value']]['in_class'][$pc][$row['id_prod']]['char'][$name] = $prod_info_char[$row['id_prod']][$id_ch];
                                    }
                                  
                               }
                           }
                       }
                   }
                   else {
                       $products[$row['value']]['none_class'][$row['id_prod']]['id'] = $row['id_prod'];
                       $products[$row['value']]['none_class'][$row['id_prod']]['name'] = $product_name[$row['id_prod']];
                       foreach ($chars_array as $id_ch=>$name) {
                            if(isset($prod_info_char[$row['id_prod']][$id_ch])) {
                                if ($id_ch == '22') {
                                        $firm = $this->firms->GetRowById((int) $prod_info_char[$row['id_prod']][$id_ch]);
                                        foreach ($firm as $f) {
                                            $products[$row['value']]['none_class'][$pc][$row['id_prod']]['char'][$name]  = $f->name;
                                        }
                                        unset($firm);
                                    }
                                    else {
                                        $products[$row['value']]['none_class'][$pc][$row['id_prod']]['char'][$name] = $prod_info_char[$row['id_prod']][$id_ch];
                                    }
                            }
                        }
                   }
               }
           }
            $product_direction_d = $this->production_direction->GetAllRows('id_direction IN ('.$firm_direction.')');
            if (!empty($product_direction_d)) {
                foreach ($product_direction_d as $row) {
                    if (!in_array($row->id_prod, $prod_id)) {
                       if(isset($prod_class[$row->id_prod])) {
                           foreach ($prod_class[$row->id_prod] as $pc) {
                               $products['Предложение']['in_class'][$pc][$row->id_prod]['id'] = $row->id_prod;
                               $products['Предложение']['in_class'][$pc][$row->id_prod]['name'] = $product_name[$row->id_prod];
                           }
                       }
                       else {
                           $products['Предложение']['none_class'][$row->id_prod]['id'] = $row->id_prod;
                           $products['Предложение']['none_class'][$row->id_prod]['name'] = $product_name[$row->id_prod];
                       }
                    }
                }
            }

            unset($prod_class_d);
            unset($products_d);
            unset($group_class_d);
            unset($product_direction_d);
            unset($product_name_d);
         }
         return $products;
     }
     
//метод для выборки уже существующих направлений фирмы по его id
     function getDirection($id) {
        $direction = array();
    //Выборка данных
        $direction_d = $this->firms_direction->GetAllRows('id_firm='.$id);
        foreach ($direction_d as $row) {
            $direction[$row->id] = $row->id_direction;
         } //End foreach
        return $direction;
     }

//метод для выборки уже существующих производств фирмы по его id
     function getMaking($id) {
        $making = array();
    //Выборка данных
        $making_d = $this->firms_making_spec->GetAllRows('id_firm='.$id);
        foreach ($making_d as $row) {
            $making[$row->id] = $row->id_make;
         } //End foreach
        return $making;
     }         
     
//метод для выборки уже существующих оборудований фирм по его id
     function getEquipment($id) {
        $equipment = array();
    //Выборка данных
        $equipment_d = $this->firms_equipment_spec->GetAllRows('id_firm='.$id);
        foreach ($equipment_d as $row) {
            $equipment[$row->id] = $row->id_equip;
         } //End foreach
        return $equipment;
     }      
     
//метод для выборки уже существующих типов фирм по его id
     function getTypes($id) {
        $types = array();
    //Выборка данных
        $types_d = $this->firms_type_firm->GetAllRows('id_firm='.$id);
        foreach ($types_d as $row) {
            $types[$row->id] = $row->id_type;
         } //End foreach
        return $types;
     }  
     
//метод для удаления контакта     
    function deleteContact() {
        if (isset($_POST['id_contact'])) {
           $id = $_POST['id_contact'];
           $answer = $this->firms_out_emps->DeleteRow($id);
           if ($answer) {
               echo 1; 
           }
           else {
               echo "Извините, объект удалить нельзя, так как он используется в других записях";
           }
       }           
    } //End delete()
     
//метод для удаления адреса хозяйства
    function deleteStock() {
            if (isset($_POST['id_stock'])) {
               $id = $_POST['id_stock'];
               $answer = $this->firms_stock->DeleteRow($id);
               if ($answer) {
                   echo 1; 
               }
               else {
                   echo "Извините, объект удалить нельзя, так как он используется в других записях";
               }
           }           
    } //End delete()  

//метод, реализующий удаление производства по id
    function deleteMaking() {
        if (isset($_POST['id_making'])) {
           $id = $_POST['id_making'];
            if ($this->firms_making->DeleteRow($id)) {
                echo 1; 
            }
            else {
                echo "Извините, объект удалить нельзя, так как он используется в других записях";
            }
       }           
    } //End delete()   
    
//метод, реализующий удаление уровня оснащения по id
    function deleteEquipment() {
        if (isset($_POST['id_equipment'])) {
           $id = $_POST['id_equipment'];
            if ($this->firms_equipment->DeleteRow($id)) {
                echo 1; 
            }
            else {
                echo "Извините, объект удалить нельзя, так как он используется в других записях";
            }
       }           
    } //End delete()       
    
 //метод, реализующий удаление контрагента по id
    function deleteFirm() {
        if (isset($_POST['id_firm'])) {
           $id = $_POST['id_firm'];
           $this->firms_direction->DeleteRow(NULL, 'id_firm='.$id);
           $this->firms_equipment_spec->DeleteRow(NULL, 'id_firm='.$id);
           $this->firms_making_spec->DeleteRow(NULL, 'id_firm='.$id);
           $this->firms_type_firm->DeleteRow(NULL, 'id_firm='.$id);
           $answer = $this->firms->DeleteRow($id);
           if ($answer) {
               echo 1; 
           }
           else {
               echo "Извините, объект удалить нельзя, так как он используется в других записях";
           }
       }           
    } //End delete()    
    
} // class firmsController
