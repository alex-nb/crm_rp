<?php
/*
 * Контроллер для работы с картой товара.
 * Код объекта 4
 * @author aleks
 */

require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/production_nameModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/directionModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/production_directionModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/production_group_classModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/production_classesModel.php";

class productionController  extends Controller {
    
    protected $production_name;
    protected $direction;
    protected $prod_direction;
    protected $group_class;
    protected $production_classes;
            
    function __construct() {
        parent::__construct();
        $this->production_name = new production_nameModel;
        $this->direction = new directionModel;
        $this->prod_direction = new production_directionModel;
        $this->group_class = new production_group_classModel;
        $this->production_classes = new production_classesModel;
        $this->code = 4;
    }
//метод, вызывающий главную страницу данного блока
    function index() {
        if(!CheckAction($this->code, $this->read, $this->premissions)) {
            $locate = $_SERVER['HTTP_REFERER'];
            echo "<script>alert('Доступ запрещен'); window.location.href='".$locate."';</script>";
        }
        else {
            $data = $this->genInfo();
            $this->view->generate('card','production/index.php', $data);
        }
    } //End index()

     function groups() {
         $code = 6;
         if(!CheckAction($code, $this->read, $this->premissions)) {
            $locate = $_SERVER['HTTP_REFERER'];
            echo "<script>alert('Доступ запрещен'); window.location.href='".$locate."';</script>";
        }
        else {
            $group_class = NULL;
            $group_class_d = $this->group_class->GetAllRows();
           foreach ($group_class_d as $row) {
               $group_class[$row->id] = (array) $row;
           } //End foreach
           $this->view->generate('card','production/groups.php', $group_class);
        }
     }

     function directions() {
         $code = 5;
         if(!CheckAction($code, $this->read, $this->premissions)) {
            $locate = $_SERVER['HTTP_REFERER'];
            echo "<script>alert('Доступ запрещен'); window.location.href='".$locate."';</script>";
        }
        else {
            $direction = NULL;
            $direction_d = $this->direction->GetAllRows();
           foreach ($direction_d as $row) {
               $direction[$row->id] = (array) $row;
           } //End foreach
            $this->view->generate('card','direction/directions.php', $direction);
        }
     }

//метод, генерирующий форму для модального окна с необходимой инфомрацией по конкректному товару
    function form() {
        $data = NULL;
        $data2 = NULL;
        if (isset($_POST['id_prod'])) {
            if (!CheckAction($this->code, $this->update, $this->premissions)) {
                echo "Доступ запрещен";
            }
            else {
                $data = $this->genInfo($_POST['id_prod']);
                $data2 = $_POST['id_prod'];
                $this->view->generateForm('card', 'production', '_form_production', $data, $data2);
            }  
        }
        else {
            if (!CheckAction($this->code, $this->create, $this->premissions)) {
                echo "Доступ запрещен";
            }
            else {
                $data = $this->genInfo($id = 0);
                $this->view->generateForm('card', 'production', '_form_production', $data);
            }
        }
    } //End form()

    
//метод, реализующий создание/изменение товара
     function edit() {
        $del_dir = array();
        $del_class = array();
        $product = new production_nameModel;
        $fields = array_keys($product->fieldsTable());
        foreach ($fields as $key) {
           if (isset($_POST[$key]) && $key != 'id') {
                $product->$key = $_POST[$key];
            } //End if
        } //End foreach
        
        if (!empty($_POST['id'])) {
            $product->Update($_POST['id']);
            $direct = $this->getDirection($_POST['id']);
            if (!empty($direct)) {$del_dir = $direct;}
            $classes = $this->getClasses($_POST['id']);
            if (!empty($classes)) {$del_class = $classes;}
            $p_id = $_POST['id'];
        } //End if         
        else {
            $p_id=$product->Save();
        } //End else
        
        $new_dir = array ();
        if (!empty($_POST['direction'])) {
            $new_dir = $_POST['direction'];
        } //End if

        $new_class = array ();
        if (!empty($_POST['classes'])) {
            $new_class = $_POST['classes'];
        } //End if
        

        $donttouch_dir = array_intersect($del_dir, $new_dir); 
        foreach ($donttouch_dir as $value) {
            if (in_array($value, $new_dir)) { unset($new_dir[array_search($value, $new_dir)]);}
            if (in_array($value, $del_dir)) { unset($del_dir[array_search($value, $del_dir)]); }
        }
        if (!empty($del_dir)) {
            foreach ($del_dir as $key=>$value) { $this->prod_direction->DeleteRow($key); }
        }
        if (!empty($new_dir)){
             foreach ($new_dir as $value) {
                 $p_dir = new production_directionModel;
                $p_dir->id_direction = $value;
                $p_dir->id_prod = $p_id;
                $p_dir->Save();
                unset($p_dir);
            }
        }

        $donttouch_class = array_intersect($del_class, $new_class); 
        foreach ($donttouch_class as $value) {
            if (in_array($value, $new_class)) { unset($new_class[array_search($value, $new_class)]);}
            if (in_array($value, $del_class)) { unset($del_class[array_search($value, $del_class)]); }
        }
        if (!empty($del_class)) {
            foreach ($del_class as $key=>$value) { $this->production_classes->DeleteRow($key); }
        }
        if (!empty($new_class)){
             foreach ($new_class as $value) {
                $p_class = new production_classesModel;
                $p_class->id_class = $value;
                $p_class->id_prod = $p_id;
                $p_class->Save();
                unset($p_class);
            }
        }
        unset($product);
        $locate = $_SERVER['HTTP_REFERER'];
        echo "<script>window.location.href='".$locate."';</script>";
    } //End edit()   

  //добавление/редактирование направлений для товаров и фирм
    function editDirect() {
        $code = 5;
        $locate = $_SERVER['HTTP_REFERER'];
        if (isset($_POST['editType']) && !empty($_POST['name'])) {
            $direct = new directionModel;
            if((int)$_POST['editType'] === 1 && CheckAction($code, $this->create, $this->premissions)) {
                $direct->name = $_POST['name'];
                $direct->id_parrent = $_POST['id'];
                $direct->Save();
            }
            else {
                if((int)$_POST['editType'] === 0 && CheckAction($code, $this->update, $this->premissions)) {
                    $direct->name = $_POST['name'];
                    $direct->Update($_POST['id']);
                }
                else { echo "<script>alert('Вам недоступно данное действие'); window.location.href='".$locate."';</script>";}
            }
            unset($direct);
        }
        echo "<script>window.location.href='".$locate."';</script>";
         //header('Location: /card/production/directions');
     }
 
  //добавление/редактирование классов и групп для товаров     
    function editClass() {
        $code = 6;
        $locate = $_SERVER['HTTP_REFERER'];
        if (isset($_POST['editType']) && !empty($_POST['name'])) {
            $class = new production_group_classModel;
            if((int)$_POST['editType'] === 1 && CheckAction($code, $this->create, $this->premissions)) {
                $class->name = $_POST['name'];
                $class->id_parrent = $_POST['id'];
                $class->Save();
            }
            else {
                if((int)$_POST['editType'] === 0 && CheckAction($code, $this->update, $this->premissions)) {
                    $class->name = $_POST['name'];
                    $class->Update($_POST['id']);
                }
                else { echo "<script>alert('Вам недоступно данное действие'); window.location.href='".$locate."';</script>";}
            }
            unset($class);
        }
        echo "<script>window.location.href='".$locate."';</script>";
     }
    
//метод для создания массива по информации с товаром
     function genInfo($id = NULL) {
        $gen_info = NULL;
        $direction = NULL;
        $group_class = NULL;
        $select = NULL;
//Выборка данных из всех сопуствующих таблиц           
        $direction_d = $this->direction->GetAllRows();
        foreach ($direction_d as $row) {
            $direction[$row->id] = (array) $row;
        } //End foreach
        
        $group_class_d = $this->group_class->GetAllRows();
        foreach ($group_class_d as $row) {
            $group_class[$row->id] = (array) $row;
        } //End foreach
        
        if ($id !== NULL) {$select='id_prod='.$id;}
       $prod_direction_d = $this->prod_direction->GetAllRows($select);
       foreach ($prod_direction_d as $row) {
           $prod_direction[$row->id_prod][] = $row->id_direction;
       } //End foreach
       $production_name_d = $this->production_name->GetRowById($id); //вернет все строки, если id NULL
       foreach ($production_name_d as $row) {
           $production_name[$row->id] = (array) $row;
       } //End foreach
       $prod_class_d = $this->production_classes->GetAllRows($select);
       foreach ($prod_class_d as $row) {
           $prod_class[$row->id_prod][] = $row->id_class;
       } //End foreach
       $gen_info['all_directions'] = $direction;
       $gen_info['all_group_class'] = $group_class;

//Обработка данных и создание массива        
        if (!empty($production_name)) {
            foreach ($production_name as $row_pn) {
                $gen_info[$row_pn['id']]['id'] = $row_pn['id'];
                $gen_info[$row_pn['id']]['name'] = $row_pn['name'];
                if (isset($row_pn['application'])) {
                    $gen_info[$row_pn['id']]['application'] = $row_pn['application'];
                }
                if (isset($row_pn['appearance'])) {
                    $gen_info[$row_pn['id']]['appearance'] = $row_pn['appearance'];
                }
                $gen_info[$row_pn['id']]['veterinary'] = $row_pn['veterinary'];
                if(!empty($prod_class) && isset($prod_class[$row_pn['id']])) {
                    foreach ($prod_class[$row_pn['id']] as $key=>$row_pc) {
                        $gen_info[$row_pn['id']]['classes'][$key][$row_pc] = $group_class[$row_pc]['name'];
                        $first = $row_pc;
                        while (!empty($group_class[$first]['id_parrent'])) {
                            $first = $group_class[$first]['id_parrent'];
                            $gen_info[$row_pn['id']]['classes'][$key][$first] = $group_class[$first]['name'];
                        } 
                    }
                }
                if(!empty($prod_direction) && isset($prod_direction[$row_pn['id']])) {
                    foreach ($prod_direction[$row_pn['id']] as $key=>$row_ds) {
                        $gen_info[$row_pn['id']]['direction'][$key][$row_ds] = $direction[$row_ds]['name'];
                        $first = $row_ds;
                        while (!empty($direction[$first]['id_parrent'])) {
                            $first = $direction[$first]['id_parrent'];
                            $gen_info[$row_pn['id']]['direction'][$key][$first] = $direction[$first]['name'];
                        } 
                    }
                }
            }
        }
        return $gen_info;
     }
     
//метод для выборки уже существующих направлений товара по его id
     function getDirection($id) {
        $direction = NULL;
    //Выборка данных
        $prod_direction_d = $this->prod_direction->GetAllRows('id_prod = '.$id);
        foreach ($prod_direction_d as $row) {
            $direction[$row->id] = $row->id_direction;
         } //End foreach
        return $direction;
     }

//метод для выборки уже существующих классов товара по его id
     function getClasses($id) {
        $classes = NULL;
    //Выборка данных
        $prod_class_d = $this->production_classes->GetAllRows('id_prod = '.$id);
        foreach ($prod_class_d as $row) {
            $classes[$row->id] = $row->id_class;
         } //End foreach
        return $classes;
     }     
     
//метод для выборки уже существующих направлений товара по id направления
 /*    function getDirectionDirect($id) {
        $direction = NULL;
    //Выборка данных
        $prod_direction_d = $this->prod_direction->GetAllRows('id_direction = '.$id);
        foreach ($prod_direction_d as $row) {
           $direction[] = $row->id;
        } //End foreach
        return $direction;
     }     
*/
 //метод, реализующий удаление товара по id
    function deleteprod() {
        if ( !CheckAction($this->code, $this->delete, $this->premissions)) {
            echo "Извините, у вас нет прав на данное действие";
        }
        else {
            if (isset($_POST['id_prod'])) {
               $id = $_POST['id_prod'];
                if ($this->production_name->DeleteRow($id)) {
                    $direct = $this->prod_direction->GetAllRows('id_prod='.$id);
                    foreach ($direct as $row) {
                        $this->prod_direction->DeleteRowForever($row->id);
                    }
                    echo 1; 
                }
                else {
                    echo "Извините, объект удалить нельзя, так как он используется в других записях";
                }
           }           
        }
    } //End delete()     
    
 //метод, реализующий удаление направления по id
    function deletedirect() {
        $code = 5;
        if ( !CheckAction($code, $this->delete, $this->premissions)) {
            echo "Извините, у вас нет прав на данное действие";
        }
        else {
            if (isset($_POST['id_direct'])) {
               $id = $_POST['id_direct'];
                if ($this->direction->DeleteRow($id)) {
                    echo 1; 
                }
                else {
                    echo "Извините, объект удалить нельзя, так как он используется в других записях";
                }
           }           
        }
    } //End delete()    
    
 //метод, реализующий удаление группы/класса по id
    function deletegroup() {
        $code = 6;
        if ( !CheckAction($code, $this->delete, $this->premissions)) {
            echo "Извините, у вас нет прав на данное действие";
        }
        else {
            if (isset($_POST['id_group'])) {
               $id = $_POST['id_group'];
                if ($this->group_class->DeleteRow($id)) {
                    echo 1; 
                }
                else {
                    echo "Извините, объект удалить нельзя, так как он используется в других записях";
                }
           }           
        }
    } //End delete()        
    
}
