<?php
/**
 * Description of firmsController
 * Фирмы
 * @author user
 */
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/raw_registryModel.php";
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/userModel.php";

class raw_registryController extends Controller{
    protected $registry;
    protected $users;

    function __construct() {
        parent::__construct();
        $this->registry = new raw_registryModel;
        $this->users = new userModel;
    }

    function index(){
        $role = $this->userAuth['role'];
        $id = NULL;
        $id_role_log = 6;
        $id_role_snab = 3;
        if (!isset($this->premissions[$id_role_snab]) && !isset($this->premissions[$id_role_log])) {
            $id = $this->userAuth['id'];
        }
        $data = $this->genInfo($id);
        $this->view->generate('card','raw_registry/index.php', $data, $role);
    } // End index
    
    function genInfo($id = NULL){
        $gen_info = NULL;
        $registry_d = NULL;
        $users_d = $this->users->GetAllRows();
        foreach ($users_d as $row){
          $users[$row->id] = (array)$row;
        } //End foreach
        if (!empty($id)) {
            $registry_d = $this->registry->GetAllRows('who='.$id);
        }
        else {
            $registry_d = $this->registry->GetAllRows();
        }
        foreach($registry_d as $row) {
            $registry[$row->id] = (array) $row;
        }
        if (!empty($registry)) {
            foreach ($registry as $row) {
                if ($row['id_parrent'] == 0 || $row['id_parrent'] == $row['id']) {
                    $gen_info[$row['id']] = $row;
                    foreach ($gen_info[$row['id']] as &$s){
                        $s = htmlspecialchars($s);
                    }
                    $gen_info[$row['id']]['who'] = htmlspecialchars($users[$row['who']]['last_name']);
                    $gen_info[$row['id']]['me'] = $this->userAuth['id'];
                }
            } //End foreach
            foreach ($registry as $row) {
                if ($row['id_parrent'] != 0 && $row['id_parrent'] != $row['id']) {
                    //file_put_contents("text.txt", "CHILDE: ".$row['id']."\n", FILE_APPEND);
                    if(isset($gen_info[$row['id_parrent']]['id'])) {
                       //file_put_contents("text.txt", "CHILDE IN: ".$row['id']."\n", FILE_APPEND);
                        $gen_info[$row['id_parrent']]['childe'][$row['id']] = $row;
                        foreach ($gen_info[$row['id_parrent']]['childe'][$row['id']] as &$s){
                            $s = htmlspecialchars($s);
                        }
                        $gen_info[$row['id_parrent']]['childe'][$row['id']]['who'] = htmlspecialchars($users[$row['who']]['last_name']);
                    }
                    else {
                        $first = $row['id_parrent'];
                        $i = 1;
                        while (empty($gen_info[$first]) && $i<10) {
                            $first = $registry[$first]['id_parrent'];
                            $i++;
                        }
                        $gen_info[$first]['childe'][$row['id']] = $row;
                        foreach ($gen_info[$first]['childe'][$row['id']] as &$s){
                            $s = htmlspecialchars($s);
                        }
                        $gen_info[$first]['childe'][$row['id']]['who'] = htmlspecialchars($users[$row['who']]['last_name']);
                    }
                }
            } //End foreach
        }

        return $gen_info;
    } // End genInfo
 

    function edit(){
        if (isset($_POST['id'])) {
            
            $toDB = new raw_registryModel;
            $fields = array_keys($toDB->fieldsTable());

            foreach ($fields as $key) {
               if (isset($_POST[$key]) && $key != 'id') {
                    $toDB->$key = $_POST[$key];
                } //End if
            } //End foreach

            if ($_POST['id_parrent'] == 0) {
                $toDB->id_parrent = $_POST['id'];
            }
            if ($_POST['id'] != 0) {
                $toDB->Update($_POST['id']);
            } //End if
            else {
                $toDB->who = $this->userAuth['id'];
                $toDB->Save();
            } //End else
        }
        unset($toDB);
        header('Location: /card/raw_registry/index');
    }
    
    function form_reg() {
        $role = $this->userAuth['role'];
        $reestr = array ();
        if (isset($_POST['id_reg'])) {
            $reg = $_POST['id_reg'];
            $data = $this->registry->GetRowById($reg);
            foreach ($data as $row){
                $reestr = (array) $row;
                foreach ($reestr as &$s){
                    $s = htmlspecialchars($s);
                }
            } 
            if (isset($_POST['type']) && $_POST['type'] === 'copy') {
                $reestr['id'] = 0;
                unset($reestr['who']);
            }
        }
        
        $this->view->generateForm('card', 'raw_registry', '_form_reg', $reestr,$role);
    } // End form()
    
} // class firmsController
