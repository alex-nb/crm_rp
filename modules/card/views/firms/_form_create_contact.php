<?php 
$id_emp = $data2;
$id_firm = $data3;
?>
<form class="form-horizontal" id = "formcontact" method="POST" action="saveContact">
    <input type="text" id="id" class="unvisible" name = "id" value='<?=$id_emp?>'>
    <input type="text" id="id_firm" class="unvisible" name = "id_firm" value='<?=$id_firm?>'>
    <input type="text" id="name" class="unvisible" name = "name" value='<?=$data['name']?>'>
    <div class="form-group">
        <label for="last_name" class="col-xs-2 control-label">Фамилия:</label>
        <div class="col-xs-10">
            <input type="text" required id="last_name" class="form-control" name = "last_name"  <?php if (!empty($data[$id_emp]['last_name'])) {echo 'value="'.$data[$id_emp]['last_name'].'"';} else { echo "placeholder='Введите фамилию'";}  ?>>
        </div>
    </div>
    
    <div class="form-group">
        <label for="first_name" class="col-xs-2 control-label">Имя:</label>
        <div class="col-xs-10">
            <input type="text" required id="first_name" class="form-control" name = "first_name"  <?php if (!empty($data[$id_emp]['first_name'])) {echo 'value="'.$data[$id_emp]['first_name'].'"';} else { echo "placeholder='Введите имя'";}  ?>>
        </div>
    </div>
    <div class="form-group">
        <label for="second_name" class="col-xs-2 control-label">Отчество:</label>
        <div class="col-xs-10">
            <input type="text" required id="first_name" class="form-control" name = "first_name"  <?php if (!empty($data[$id_emp]['second_name'])) {echo 'value="'.$data[$id_emp]['second_name'].'"';} else { echo "placeholder='Введите отчество'";}  ?>>
        </div>
    </div>
    <div class='form-group'>
            <label for='phone' class='col-xs-2 control-label'>Телефон:</label>
            <div class='col-xs-10'> 
                <input type='text' class='form-control' id='phone' name = 'phone'  <?php if (!empty($data[$id_emp]['phone'])) {echo 'value="'.$data[$id_emp]['phone'].'"';} else { echo "placeholder='Введите номер телефона'";}  ?>>
            </div>
    </div>
    <div class='form-group'>
            <label for='email' class='col-xs-2 control-label'>Email:</label>
            <div class='col-xs-10'> 
                <input type='text' class='form-control' id='email' name = 'email'  <?php if (!empty($data[$id_emp]['email'])) {echo 'value="'.$data[$id_emp]['email'].'"';} else { echo "placeholder='Введите email'";}  ?>>
            </div>
    </div>
     <div class="form-group">
        <label for="id_out_dept" class="col-xs-2 control-label">Отдел:</label>
        <div class="col-xs-10">
            <select class="form-control"id="id_out_dept" name = "id_out_dept">
                <option value="0">Неизвестно</option>
                <?php 
                    asort($data['all_depts']);
                    foreach ($data['all_depts'] as $id_D=>$dept) {
                        if ($id_D == $data[$id_emp]['id_out_dept']) {
                            echo "<option selected value=".$id_D.">".$dept."</option>";
                        }
                        else {
                            echo "<option value=".$id_D.">".$dept."</option>";
                        }
                    }
                ?>
            </select>
        </div>
    </div>
     <div class="form-group">
        <label for="id_out_pos" class="col-xs-2 control-label">Должность:</label>
        <div class="col-xs-10">
            <select class="form-control" id="id_out_pos" name = "id_out_pos">
                <option value="0">Неизвестно</option>
                <?php 
                    asort($data['all_positions']);
                    foreach ($data['all_positions'] as $id_P=>$pos) {
                        if ($id_P == $data[$id_emp]['id_out_pos']) {
                            echo "<option selected value=".$id_P.">".$pos."</option>";
                        }
                        else {
                            echo "<option value=".$id_P.">".$pos."</option>";
                        }
                    }
                ?>
            </select>
        </div>
    </div>

<!-- Футер модального окна -->
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
      <button type="submit" class="btn btn-primary">Сохранить</button>
    </div>
</form>