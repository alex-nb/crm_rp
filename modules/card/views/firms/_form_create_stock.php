<?php 
$id_stock = $data2;
$id_firm = $data3;
?>
<form class="form-horizontal" id = "formcontact" method="POST" action="saveStock">
    <input type="text" id="id" class="unvisible" name = "id" value='<?=$id_stock?>'>
    <input type="text" id="id_firm" class="unvisible" name = "id_firm" value='<?=$id_firm?>'>
    <input type="text" id="name" class="unvisible" name = "name" value='<?=$data['name']?>'>
    <div class="form-group">
        <label for="id_city" class="col-xs-2 control-label">Город:</label>
        <div class="col-xs-10">
            <select class="form-control" id="id_city" name = "id_city">
                <option value="0">Неизвестно</option>
                <?php 
                    asort($data['all_city']);
                    foreach ($data['all_city'] as $id_S=>$city) {
                        if ($id_S == $data[$id_stock]['id_city']) {
                            echo "<option selected value=".$id_S.">".$city."</option>";
                        }
                        else {
                            echo "<option value=".$id_S.">".$city."</option>";
                        }
                    }
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="address" class="col-xs-2 control-label">Адрес:</label>
        <div class="col-xs-10">
            <textarea class="form-control" id="address" name = "address" placeholder="Введите полный адрес БЕЗ города"><?php if (!empty($data[$id_stock]['address'])) {echo $data[$id_stock]['address'];}?></textarea>
        </div>
    </div>

<!-- Футер модального окна -->
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
      <button type="submit" class="btn btn-primary">Сохранить</button>
    </div>
</form>