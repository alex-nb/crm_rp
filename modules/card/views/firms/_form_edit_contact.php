</ul>
<?php 
$id_firm = $data2;
$depts = $data['all_depts'];
$positions = $data['all_positions'];
?>

<form class="form-horizontal"  id = "form" method="POST" action="editStock">
    <input type="text" id="id_firm" class="unvisible" name = "id_firm" value='<?=$id_firm?>'>
    <input type="text" id="name" class="unvisible" name = "name" value='<?=$data3?>'>
    <div class="d4"><h3>Контакты (<?=$data3?>)</h3></div>
     <?php
        if (isset($data[$id_firm]['emps'])) {
            $dept = array ();
            foreach ($data[$id_firm]['emps'] as $emp) {
                if (empty($dept[$emp['dept']])) {$dept[$emp['dept']] = "<div class='d4'><h3>".$emp['dept']."</h3></div><div class='row'>";}
                $fio = $emp['last_name'];
                $fio .= " ".$emp['first_name'];
                $fio .= " ".$emp['second_name'];
                $string = "<div class='col-xs-3'>
                                    <span class='glyphicon glyphicon-pencil pointer contactedit' id='".$emp['id']."' data-toggle='tooltip' title='Редактировать'></span>
                                    <span class='glyphicon glyphicon-trash pointer contactedelete' id='".$emp['id']."' data-toggle='tooltip' title='Удалить'></span>
                                    <p><b>".$emp['position'].": </b> ".$fio." </p>";
                if (isset($emp['phone'])) {
                    $string .= "<p><b>Телефон: </b>  ".$emp['phone']." </p>";
                }
                if (isset($emp['email'])) {
                    $string .= "<p><b>Email: </b>  ".$emp['email']." </p>";
                }
                $string .= "</div>";
                $dept[$emp['dept']] .= $string;
            }
            foreach ($dept as $row) {
                echo $row."</div>";
            }
        }
    ?>
    <hr>
    <button type="button" class="btn btn-info" id="new_contact">Добавить новый контакт</button>
    <br><br><br>
    <button type="button" class="btn btn-primary" id="cancel">На главную</button>
    <button type="submit" class="btn btn-primary">Далее</button>
</form>

<!-- HTML-код модального окна для создания/удаления-->
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
        <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">Контакт</h4>
            </div>
        <!-- Основное содержимое модального окна -->
            <div class="modal-body" id="modal-body">
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function() {
    var id_firm = <?=$id_firm?>;
    var name = '<?=$data3?>';
    $('#myModal').on('hidden.bs.modal', function () {
        $("#modal-body").html('');
    });
    
    $("#new_contact").click(function() {
        $("#modal-body").html('');
        $.ajax({
            url : "formCreareContact",
            type: "POST",
            data: {id_firm:id_firm, name:name},
            success: function(html){
               $('#modal-body').append(html);
               $('#myModal').modal('show');
            }
        });  
    });
    
    $(".contactedit").click(function() { 
        var id_contact = $(this).attr('id');
        $("#modal-body").html('');
        $.ajax({
            url : "formCreareContact",
            type: "POST",
            data: {id_contact:id_contact, id_firm:id_firm, name:name},
            success: function(html){
               $('#modal-body').append(html);
               $('#myModal').modal('show');
            }
        });  
    });
    
    $(".contactedelete").click(function() { 
        var id_contact = $(this).attr('id');
        var div_parrent = $(this).parent("div");
        if (confirm("Подверждаете удаление контакта?")) {
            $.ajax({
                url : "deleteContact",
                type: "POST",
                data: {id_contact:id_contact},
                success: function(data){
                    if (data == 1) {
                        data = "Удаление завершено";
                        alert(data);
                        $(div_parrent).remove();
                    }
                    else { alert(data); }
                }
            });  
        }
    });
    
    $("#cancel").click(function() {
        window.location.href="index";
    });
    
});
</script>