</ul>
<?php 
$id = $data2;
?>

<form class="form-horizontal"  id = "form" method="POST" action="editProduction">
    <input type="text" id="id_from_gen" class="unvisible" name = "id_firm" value='<?=$id?>'>
    <input type="text" id="name" class="unvisible" name = "name" value='<?=$data3?>'>
    <div class="d4"><h3>Доп. информация (<?=$data3?>)</h3></div>
    <div class='row'>
        <div class='col-xs-4'>
            <ul class="ul-treefree ul-dropfree">
                <li><label>Направления работы</label></li>
                <?php
                if (isset($data[$id]['direction'])) {
                    echo  makeBeatiful_list($data['all_direction'], 'direction', $data[$id]['direction']);
                }
                else {
                    if (isset($data['all_direction'])) {echo  makeBeatiful_list($data['all_direction'], 'direction');}
                    else {echo "Направлений нет";}
                }
                 ?>
            </ul>
        </div>
        <div class='col-xs-4'>
        <ul class="ul-treefree ul-dropfree">
            <li><label>Что производят?</label></li>
            <?php
            if (isset($data[$id]['making'])) {
                echo  makeBeatiful_list($data['all_making'], 'making', $data[$id]['making']);
            }
            else {
                if (isset($data['all_making'])) {echo  makeBeatiful_list($data['all_making'], 'making');}
                else {echo "Сфер производства нет";}
            }
             ?>
        </ul>
        </div>
        <div class='col-xs-4'>
        <ul class="ul-treefree ul-dropfree">
            <li><label>Уровень оснащения</label></li>
            <?php
            if (isset($data[$id]['equipment'])) {
                echo  makeBeatiful_list($data['all_equipment'], 'equipment', $data[$id]['equipment']);
            }
            else {
                if (isset($data['all_equipment'])) {echo  makeBeatiful_list($data['all_equipment'], 'equipment');}
                else {echo "Оборудования нет";}
            }
             ?>
        </ul>
        </div>
    </div>
    <button type="button" class="btn btn-primary" id="cancel">На главную (без сохранения)</button>
    <button type="submit" class="btn btn-primary">Сохранить и далее</button>
</form>

<script>
$(document).ready(function() {
    var id = <?=$id?>;
    
    $("#cancel").click(function() {
        window.location.href="index";
    });

    $(".ul-dropfree div.drop").click(function() {
            if ($(this).nextAll("ul").css('display')=='none') {
                    $(this).nextAll("ul").slideDown(400);
                    $(this).css({'background-position':"-11px 0"});
            } else {
                    $(this).nextAll("ul").slideUp(400);
                    $(this).css({'background-position':"0 0"});
            }
            setTimeout( function () {
              $('#myModal').modal('handleUpdate');
              } , 500 );
    });
    
    $(".ul-dropfree").find("ul").slideUp(400).parents("li").children("div.drop").css({'background-position':"0 0"});
    
    $("input:checkbox").on('change', function() {
        var all_check = $(this).parent().siblings('ul').find("input:checkbox");
       //отмечаем или снимаем отметку с детей
       if ($(this).prop("checked") == true){
           $(this).parent("label").addClass("shine");
           $(all_check).parent("label").addClass("shine");
           $(all_check).prop({"checked":true, "indeterminate":false});
       } else {
           $(all_check).prop({"checked":false, "indeterminate":false});
           $(this).parent("label").removeClass("shine");
           $(all_check).parent("label").removeClass("shine");
       }
       check_childe($(this));
    });
    
    function check_childe (obj) {
         $(obj).parents("li").each(function(){ //находим родителей у данного чекбокса
             var all = $(this).find("input:checkbox").not(":first"); //выбираем всех чекбоксов-детей-внуков и т.д.
             var check = $(all).filter("input:checkbox:checked"); //выбираем чекнутые чекбоксы среди них
             if (check.length !== 0 || all.length !== 0) {
                 if (check.length === all.length) { //если кол-во чекнутых равно общему кол-ву чекбоксов, то чекаем и этот чекбокс
                     $(this).find("input:checkbox:first").prop({"indeterminate":false, "checked":true});
                     $(this).find("label:first").addClass("shine");
                 }
                 else if (check.length === 0) { //если кол-во чекнутых равно 0, то отменяем чек у этого чекбокса
                     $(this).find("input:checkbox:first").prop({"indeterminate":false, "checked":false});
                     $(this).find("label:first").removeClass("shine");
                 }
                 else if (check.length < all.length) { //если чекнутые есть, но их меньше, чем всех чекбоксов, то делаем этот indeterminate
                     $(this).find("input:checkbox:first").prop({"indeterminate":true, "checked":false});
                     $(this).find("label:first").addClass("shine");
                 }
             }
         });
    }
});
</script>

<?php
function makeBeatiful_list($array, $name, $selected = NULL, $id_parrent = NULL) {
    $out = NULL;
    foreach ($array as $row) {
        $flag = true; 
        if ($row['id_parrent'] == $id_parrent) {     
            $return_func = makeBeatiful_list($array, $name, $selected, $row['id']);   
            if (empty($return_func)) {
                if(!empty($selected)) {
                    foreach ($selected as $sel) {
                        if(isset($sel[$row['id']])) {
                            $out .= "<li><label class='shine'><input type='checkbox' checked name='".$name."[]' value='".$row['id']."'/>".$row['name']."</label></li>";
                            $flag = false;
                            break;
                        }
                    }
                }
                if($flag) {$out .= "<li><label><input type='checkbox' name='".$name."[]' value='".$row['id']."'/>".$row['name']."</label></li>";}
            }
            else {
                if (!empty($selected)) {
                    foreach ($selected as $sel) {      
                        if (isset($sel[$row['id']])) {                     
                            $out .="<li><div class='drop'></div><label class='shine'><input type='checkbox' checked/>".$row['name']."</label><ul>";
                            $flag = false;  
                            break;
                        }
                    }
                }
                if($flag) {$out .="<li><div class='drop'></div><label><input type='checkbox'/>".$row['name']."</label><ul>";}
               $out .= $return_func."</ul></li>";  
            }          
        } 
    }
    return $out;
}
