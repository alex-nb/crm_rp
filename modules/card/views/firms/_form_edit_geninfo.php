</ul>
<?php 
$id = $data2;
$types = $data['all_types'];
$city = $data['all_city'];
?>

<form class="form-horizontal"  id = "form" method="POST" action="editContact">
    <input type="text" id="id_from_gen" class="unvisible" name = "id_firm" value='<?=$id?>'>
    <div class="d4"><h3>Общая информация</h3></div>
    <div class="form-group">
        <label for="name" class="col-xs-1 control-label">Наименование*:</label>
        <div class="col-xs-4">
            <input type="text" required id="name" class="form-control" name = "name" <?php if (!empty($id)) {echo 'value="'.$data[$id]['name'].'"';} else { echo "placeholder='Введите наименование фирмы'";}  ?>>
        </div>
        <label for="type" class="col-xs-2 control-label">Тип контрагента*:</label>
        <div>
            <?php 
                foreach ($types as $key=>$name) {
                    $readonly = '';
                    if($key != 2 && $key !=5) {
                        $readonly = 'disabled';
                    }
                    if (isset($data[$id]['types'][$key])) {
                        echo "<label><input type='checkbox' checked name='types[]' value='".$key."' ".$readonly."/>".$name."</label>     ";
                    }
                    else {
                        echo "<label><input type='checkbox' name='types[]' value='".$key."' ".$readonly."/>".$name."</label>     ";
                    }
                }
            ?>
        </div>
    </div>
    <div class="form-group">
        <label for="inn" class="col-xs-1 control-label">ИНН*:</label>
        <div class="col-xs-6">
            <input type="text" required id="inn" class="form-control" name = "inn"  <?php if (!empty($id)) {echo 'value="'.$data[$id]['inn'].'"';} else { echo "placeholder='Введите ИНН фирмы'";}  ?>>
        </div>
    </div>
    <div class="form-group">
        <label for="ogrn" class="col-xs-1 control-label">ОГРН:</label>
        <div class="col-xs-6">
            <input type="text" id="ogrn" class="form-control" name = "ogrn"  <?php if (!empty($id)) {echo 'value="'.$data[$id]['ogrn'].'"';} else { echo "placeholder='Введите ОГРН фирмы'";}  ?>>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-1 control-label">Юридический адрес</label>
        <label for="id_legal_city" class="col-xs-1 control-label">Город:</label>
        <div class="col-xs-2">
            <select class="form-control" id="id_legal_city" name = "id_legal_city">
                <?php 
                    foreach ($city as $key=>$name) {
                        if ($key == $data[$id]['id_legal_city']) {
                            echo "<option selected value=".$key.">".$name."</option>";
                        }
                        else {
                            echo "<option value=".$key.">".$name."</option>";
                        }
                    }
                ?>
            </select>
        </div>
        <label for="legal_address" class="col-xs-1 control-label">Адрес:</label>
        <div class="col-xs-5">
            <input type="text"  id="legal_address" class="form-control" name = "legal_address"  <?php if (!empty($id)) {echo 'value="'.$data[$id]['legal_address'].'"';} else { echo "placeholder='Введите полный юридический адрес БЕЗ города'";}  ?>>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-1 control-label">Фактический адрес</label>
        <label for="id_fact_city" class="col-xs-1 control-label">Город:</label>
        <div class="col-xs-2">
            <select class="form-control" id="id_fact_city" name = "id_fact_city">
                <?php 
                foreach ($city as $key=>$name) {
                        if ($key == $data[$id]['id_fact_city']) {
                            echo "<option selected value=".$key.">".$name."</option>";
                        }
                        else {
                            echo "<option value=".$key.">".$name."</option>";
                        }
                    }
                    ?>
            </select>
        </div>
        <label for="fact_address" class="col-xs-1 control-label">Адрес:</label>
        <div class="col-xs-5">
            <input type="text"  id="fact_address" class="form-control" name = "fact_address"  <?php if (!empty($id)) {echo 'value="'.$data[$id]['fact_address'].'"';} else { echo "placeholder='Введите полный фактический адрес БЕЗ города'";}  ?>>
        </div>
    </div>

    <button type="button" class="btn btn-primary" id="cancel">На главную</button>
    <button type="submit" class="btn btn-primary">Сохранить и далее</button>
</form>

<script>
$(document).ready(function() {
    var id = <?=$id?>;
    
    $("#cancel").click(function() {
        window.location.href="index"
    });

});
</script>