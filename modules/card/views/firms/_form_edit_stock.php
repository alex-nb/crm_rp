</ul>
<?php
$id_firm = $data2;
if (isset($data['types']) && in_array(2, $data['types'])) {
    $next_step = 'editDopInfo';
}
else {
    $next_step = 'index';
}
?>
<form class="form-horizontal"  id = "form" method="POST" action=<?=$next_step?>>
    <input type="text" id="id_firm" class="unvisible" name = "id_firm" value='<?=$id_firm?>'>
    <input type="text" id="name" class="unvisible" name = "name" value='<?=$data3?>'>
    <div class='d4'><h3>Текущие адреса хозяйств/складов (<?=$data3?>)</h3></div>

    <table class="table table-striped table-bordered table-hover"  id="table_stocs" width="100%">
        <thead>
            <tr class="info">
                <th>ИД</th>
                <th>Город</th>
                <th>Адрес</th>
                <th width="5%"></th>
            </tr>
        </thead>
        <tbody>
            <?php
                if (isset($data[$id_firm]['stocks'])) {
                    foreach ($data[$id_firm]['stocks'] as $stock) {
                        echo "<tr>";
                        echo "<td>".$stock['id']."</td>";
                        echo "<td>".$stock['name_city']."</td>";
                        echo "<td>".$stock['address']."</td>";
                        echo "<td>
                                    <span class='glyphicon glyphicon-pencil pointer stockedit' id='".$stock['id']."' data-toggle='tooltip' title='Редактировать'></span>
                                    <span class='glyphicon glyphicon-trash pointer stockdelete' id='".$stock['id']."' data-toggle='tooltip' title='Удалить'></span>
                                </td>";
                        echo "</tr>";
                    }
                }
            ?>
        </tbody>
    </table>                        
    <button type="button" class="btn btn-info" id="new_stock">Добавить новый адрес</button>
    <br><br><br>
    <button type="button" class="btn btn-primary" id="cancel">На главную</button>
    <button type="submit" class="btn btn-primary">Далее</button>
</form>

<!-- HTML-код модального окна для создания/удаления-->
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
        <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">Создание</h4>
            </div>
        <!-- Основное содержимое модального окна -->
            <div class="modal-body" id="modal-body">
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function() {
    var id_firm = <?=$id_firm?>;
    var name = '<?=$data3?>';
    $('#myModal').on('hidden.bs.modal', function () {
        $("#modal-body").html('');
    });

    $(".stockedit").click(function() { 
        var id_stock = $(this).attr('id');
        $("#modal-body").html('');
        $.ajax({
            url : "formCreateStock",
            type: "POST",
            data: {id_stock:id_stock, id_firm:id_firm, name:name},
            success: function(html){
               $('#modal-body').append(html);
               $('#myModal').modal('show');
            }
        });  
    });
    
    $(".stockdelete").click(function() { 
        var id_stock = $(this).attr('id');
        var tr_parrent = $(this).parents("tr");
        if (confirm("Подверждаете удаление адреса?")) {
            $.ajax({
                url : "deleteStock",
                type: "POST",
                data: {id_stock:id_stock},
                success: function(data){
                    if (data == 1) {
                        data = "Удаление завершено";
                        alert(data);
                        $(tr_parrent).remove();
                    }
                    else { alert(data); }
                }
            });  
        }
    });

    $("#new_stock").click(function() {
        $("#modal-body").html('');
        $.ajax({
            url : "formCreateStock",
            type: "POST",
            data: {id_firm:id_firm, name:name},
            success: function(html){
               $('#modal-body').append(html);
               $('#myModal').modal('show');
            }
        });  
    });
    
    $("#cancel").click(function() {
        window.location.href="index";
    });

});
</script>