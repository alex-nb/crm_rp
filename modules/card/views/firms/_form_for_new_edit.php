<?php
$id_firm = $data2;
/*echo "<pre>";
var_dump($data);
echo "</pre>";*/
?>
<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#offer">Предложение</a></li>
    <li><a data-toggle="tab" href="#take_us">Берут у нас</a></li>
    <li><a data-toggle="tab" href="#take_other">Берут не у нас</a></li>
    <li><a data-toggle="tab" href="#dont_take">Не берут</a></li>
    <li><a data-toggle="tab" href="#spec">Спецзадачи</a></li>
</ul>

<div class="tab-content">
    
    <div id="offer" class="tab-pane fade in active">
        <br>
        <div class="btn-group">
            <button type="button" data-toggle="dropdown" class="btn btn-primary dropdown-toggle">Перенести в другой раздел <span class="caret"></span></button>
              <ul class="dropdown-menu">
                <li><a id="donttake" class="out" style="cursor: pointer">Не берут</a></li>
                <li><a id="takeus" class="out" style="cursor: pointer">Берут у нас</a></li>
                <li><a id="takeother" class="out" style="cursor: pointer">Берут не у нас</a></li>
             </ul>
        </div>
        <button type="button" class="btn btn-primary printButton" style="margin-left: 15px" >Вывести на печать <span class="glyphicon glyphicon-print"></span></button>
        <br><br>
        <form id="product_offer">
            <input type="text" class="form-control unvisible" id="id_firm" name ="id_firm" readonly value=<?= $id_firm ?>>
            <table class="table table-striped table-bordered table-hover"  id="table_offer" width="100%">
                <thead>
                    <tr class="reg_purchase">
                        <th width="2%"></th>
                        <th width="2%"></th>
                        <th width="2%"></th>
                        <th width="30%">Наименование</th>
                        <th width="64%">Комментарии</th>
                    </tr>
                </thead>
                <tbody id="table_offer">
                    <?php
                    if(!empty($data['Предложение'])) {
                        echo makeBeatiful_list($data['all_class'], $data['Предложение'], 'table_offer');
                    }
                    else {
                        echo '<tr class="prod">
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                        </tr>';
                    }
                    ?>
                </tbody>
            </table>
        </form>
    </div>
    <div id="take_us" class="tab-pane fade">
        <br>
        <div class="btn-group">
            <button type="button" data-toggle="dropdown" class="btn btn-primary dropdown-toggle">Перенести в другой раздел <span class="caret"></span></button>
              <ul class="dropdown-menu">
                  <li><a id="offer" class="out" style="cursor: pointer">Предложение</a></li>
                <li><a id="donttake" class="out" style="cursor: pointer">Не берут</a></li>
                <li><a id="takeother" class="out" style="cursor: pointer">Берут не у нас</a></li>
             </ul>
        </div>
        <button type="button" class="btn btn-primary printButton" style="margin-left: 15px" >Вывести на печать <span class="glyphicon glyphicon-print"></span></button>
        <br><br>
        <form id="product_take_us">
            <input type="text" class="form-control unvisible" id="id_firm" name ="id_firm" readonly value=<?= $id_firm ?>>
            <table class="table table-striped table-bordered table-hover"  id="table_take_us" width="100%">
                <thead>
                    <tr class="reg_purchase">
                        <th width="2%"></th>
                        <th width="2%"></th>
                        <th width="2%"></th>
                        <th width="20%">Наименование</th>
                        <th width="10%">Объем раз. поставки</th>
                        <th width="10%">Период закупок</th>
                        <th width="10%">Потребление/месяц</th>
                        <th width="5%">Цена</th>
                        <th width="49%">Комментарии</th>
                    </tr>
                </thead>
                <tbody id="table_take_us">
                    <?php
                    if(!empty($data['Берут у нас'])) {
                        echo makeBeatiful_list($data['all_class'], $data['Берут у нас'], 'table_take_us');
                    }
                    else {
                        echo '<tr class="prod">
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                        </tr>';
                    }
                    ?>
                </tbody>
            </table>
        </form>
    </div>
    <div id="take_other" class="tab-pane fade">
        <br>
        <div class="btn-group">
            <button type="button" data-toggle="dropdown" class="btn btn-primary dropdown-toggle">Перенести в другой раздел <span class="caret"></span></button>
              <ul class="dropdown-menu">
                  <li><a id="offer" class="out" style="cursor: pointer">Предложение</a></li>
                <li><a id="donttake" class="out" style="cursor: pointer">Не берут</a></li>
                <li><a id="takeus" class="out" style="cursor: pointer">Берут у нас</a></li>
             </ul>
        </div>
        <button type="button" class="btn btn-primary printButton" style="margin-left: 15px" >Вывести на печать <span class="glyphicon glyphicon-print"></span></button>
        <br><br>
        <form id="product_take_other">
            <input type="text" class="form-control unvisible" id="id_firm" name ="id_firm" readonly value=<?= $id_firm ?>>
            <table class="table table-striped table-bordered table-hover"  id="table_take_other" width="100%">
                <thead>
                    <tr class="reg_purchase">
                        <th width="2%"></th>
                        <th width="2%"></th>
                        <th width="2%"></th>
                        <th width="15%">Наименование</th>
                        <th width="7%">У кого?</th>
                        <th width="7%">Объем раз. поставки</th>
                        <th width="7%">Период закупок</th>
                        <th width="7%">Потребление/месяц</th>
                        <th width="5%">Цена</th>
                        <th width="46%">Комментарии</th>
                    </tr>
                </thead>
                <tbody id="table_take_other">
                    <?php
                    if(!empty($data['Берут не у нас'])) {
                        echo makeBeatiful_list($data['all_class'], $data['Берут не у нас'], 'table_take_other');
                    }
                    else {
                        echo '<tr class="prod">
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                        </tr>';
                    }
                    ?>
                </tbody>
            </table>
        </form>
    </div>
    <div id="dont_take" class="tab-pane fade">
        <br>
        <div class="btn-group">
            <button type="button" data-toggle="dropdown" class="btn btn-primary dropdown-toggle">Перенести в другой раздел <span class="caret"></span></button>
              <ul class="dropdown-menu">
                <li><a id="offer" class="out" style="cursor: pointer">Предложение</a></li>
                <li><a id="takeus" class="out" style="cursor: pointer">Берут у нас</a></li>
                <li><a id="takeother" class="out" style="cursor: pointer">Берут не у нас</a></li>
             </ul>
        </div>
        <button type="button" class="btn btn-primary printButton" style="margin-left: 15px" >Вывести на печать <span class="glyphicon glyphicon-print"></span></button>
        <br><br>
        <form id="product_dont_take">
            <input type="text" class="form-control unvisible" id="id_firm" name ="id_firm" readonly value=<?= $id_firm ?>>
            <table class="table table-striped table-bordered table-hover"  id="table_dont_take" width="100%">
                <thead>
                    <tr class="reg_purchase">
                        <th width="2%"></th>
                        <th width="2%"></th>
                        <th width="2%"></th>
                        <th width="25%">Наименование</th>
                        <th width="9%">Тип</th>
                        <th width="60%">Комментарии</th>
                    </tr>
                </thead>
                <tbody id="table_dont_take">
                    <?php
                    if(!empty($data['Не берут'])) {
                        echo makeBeatiful_list($data['all_class'], $data['Не берут'], 'table_dont_take');
                    }
                    else {
                        echo '<tr class="prod">
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                        </tr>';
                    }
                    ?>
                </tbody>
            </table>
        </form>
    </div>
    <div id="spec" class="tab-pane fade">
        <p>Содержимое 5 панели...</p>
    </div>
</div>

<?php
//создание упорядочных строк таблицы (по группам/классам) и запись в соответствующие классы товаров
function makeBeatiful_list($array, $product=NULL, $class, $id_parrent = NULL) {
    $out = NULL;
    $dop_td = '<td></td>';
    if($class=="table_take_other") {$dop_td.="<td></td><td></td><td></td><td></td><td></td>";}
    if($class=="table_take_us") {$dop_td.="<td></td><td></td><td></td><td></td>";}
    if($class=="table_dont_take") {$dop_td.="<td></td>";}
    foreach ($array as $id=>$row) {  
        if ($row['id_parrent'] == $id_parrent) {     
            $return_func = makeBeatiful_list($array, $product, $class, $row['id']);   
            if (empty($return_func)) {
                $unvisible = '';
                $classes = '';

                if ($id_parrent != NULL) {
                    $unvisible .= "unvisible";
                    $classes = array ();
                    $first = $id_parrent;
                    while (!empty($array[$first]['id_parrent'])) {
                        $first = $array[$first]['id_parrent'];
                        $classes[] = $first;
                    } 
                    $classes = implode(" ", $classes);
                }
                
                $for_tr = 'class="row_'.$id_parrent.' '.$classes.' '.$unvisible.'"';
                $for_td = '';
                if (!empty($product["in_class"][$row['id']])) { 
                    $for_tr = 'class="row_'.$id_parrent.' '.$classes.' '.$unvisible.'" id="'.$row['id'].'"';
                    $for_td = '<span class="glyphicon glyphicon-plus pointer" id="'.$class.'"></span>';
                }
                $out .= '<tr '.$for_tr.'>
                                <td>'.$for_td.'</td>
                                <td><input type="checkbox" class="'.$class.'"></td>
                                <td></td>
                                <td>'.$row['name'].'</td>'.$dop_td.'</tr>';
                if (!empty($product["in_class"][$row['id']])) {
                    $classes .= " ".$id_parrent;
                    foreach ($product["in_class"][$row['id']] as $prod) {
                        $char = '';
                        if($class=="table_take_other") {
                            !empty($prod['char']['vrag']) ? $char .= "<td>".$prod['char']['vrag']."</td>" : $char .= "<td>-</td>";
                        }
                        if($class=="table_take_us" || $class=="table_take_other") {
                            !empty($prod['char']['v']) ? $char .= "<td>".$prod['char']['v']."</td>" : $char .= "<td>-</td>";
                            !empty($prod['char']['zakup']) ? $char .= "<td>".$prod['char']['zakup']."</td>" : $char .= "<td>-</td>";
                            !empty($prod['char']['potr']) ? $char .= "<td>".$prod['char']['potr']."</td>" : $char .= "<td>-</td>";
                            !empty($prod['char']['price']) ? $char .= "<td>".$prod['char']['price']."</td>" : $char .= "<td>-</td>";
                        }
                        if($class=="table_dont_take") {
                            !empty($prod['char']['type']) ? $char .= "<td>".$prod['char']['type']."</td>" : $char .= "<td>-</td>";
                        }
                        //if($class=="table_offer" || $class=="table_take_other" || $class=="table_take_us") {
                            !empty($prod['char']['komment']) ? $char .= "<td>".$prod['char']['komment']."</td>" : $char .= "<td>-</td>";
                        //}
                        $out .= '<tr class="prod unvisible row_'.$row['id'].' '.$classes.'">
                                <td></td>
                                <td><input type="checkbox" class="'.$class.'" name="prod[]" value='.$prod['id'].'></td>
                                <td><span class="glyphicon glyphicon-pencil '.$class.' pointer" id="'.$prod['id'].'" data-toggle="tooltip" title="Редактирвоать"></span></td>
                                <td>'.$prod['name'].'</td>'.$char."</tr>";
                    }
                }
            }
            else {
                $unvisible = '';
                $classes = '';

                if ($id_parrent != NULL) {
                    $unvisible .= "unvisible";
                    $classes = array ();
                    $first = $id_parrent;
                    while (!empty($array[$first]['id_parrent'])) {
                        $first = $array[$first]['id_parrent'];
                        $classes[] = $first;
                    } 
                    $classes = implode(" ", $classes);
                }
                $out .= '<tr class="row_'.$id_parrent.' '.$classes.' '.$unvisible.'" id="'.$row['id'].'">
                                <td><span class="glyphicon glyphicon-plus pointer" id="'.$class.'"></span></td>
                                <td><input type="checkbox" class="'.$class.'"></td>
                                <td></td>
                                <td>'.$row['name'].'</td>'.$dop_td.'</tr>';
                
                $out .= $return_func;  
            }          
        } 
    }
    return $out;
}