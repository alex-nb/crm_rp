<?php
$id_firm = $data2;
//echo $id_firm;
//var_dump($data);
?>

<table width='100%'>
        <?php
            if(!empty($data['info_firm'])) {
                foreach ($data['info_firm'] as $row) {
                    echo "<tr><th align='center'  width='50%'>".$row['name']."</th>";
                    if (isset($row['types'])) {
                        echo "<th width='35%'>";
                        foreach ($row['types'] as $type) {
                            echo $type."<br>";
                        }
                        echo "</th>";
                    }
                    else {echo "<td width='50%'>-</td>";}
                    echo "</tr>";
                    echo "<tr>";
                    echo "<td width='50%'><b>ИНН:</b> ".$row['inn']."</td>";
                    echo "<td width='50%'><b>ОГРН:</b> ".$row['ogrn']."</td>";
                    echo "</tr>";
                    echo "<tr>";
                        if (isset($row['name_legal_city'])) { echo "<td width='50%'><b>Юридический адрес:</b> ".$row['name_legal_city'].", ".$row['legal_address']."</td>"; }
                        else {if (!empty($row['legal_address'])) {echo "<td width='50%'><b>Юридический адрес:</b> ".$row['legal_address']."</td>";} else {echo "<td>-</td>";}}
                         if (isset($row['name_fact_city'])) { echo "<td width='50%'><b>Фактический адрес:</b> ".$row['name_fact_city'].", ".$row['fact_address']."</td>"; }
                        else {if (!empty($row['fact_address'])) {echo "<td width='50%'><b>Фактический адрес:</b> ".$row['fact_address']."</td>";} else {echo "<td>-</td>";}}
                    echo "</tr>";
                    if (isset($row['direction'])) {
                        echo "<tr><th colspan='2'>Направления работы</th></tr>";
                        echo "<tr><td colspan='2'>";
                        foreach ($row['direction'] as $dir) {
                            $direction = implode ("->", array_reverse($dir));
                            echo $direction."<br>";
                        }
                        echo "</td></tr>";
                    }
                    else {echo "<tr><td colspan='2'>-</td></tr>";}
                    if (isset($row['equipment'])) {
                        echo "<tr><th colspan='2'>Уровень оснащения</th></tr>";
                        echo "<tr><td colspan='2'>";
                        foreach ($row['equipment'] as $equip) {
                            $equipment = implode ("->", array_reverse($equip));
                            echo $equipment."<br>";
                        }
                        echo "</td></tr>";
                    }
                    else {echo "<tr><td colspan='2'>-</td></tr>";}
                    if (isset($row['making'])) {
                        echo "<tr><th colspan='2'>Производство</th></tr>";
                       echo "<tr><td colspan='2'>";
                        foreach ($row['making'] as $mak) {
                            $making = implode ("->", array_reverse($mak));
                            echo $making."<br>";
                        }
                        echo "</td></tr>";
                    }
                    else {echo "<tr><td colspan='2'>-</td></tr>";}
                }
            }
        ?>
</table>
<br>
<?php if(!empty($data['contacts'][$id_firm]['emps'])) { ?>
<table width='100%'>
    <thead>
        <tr>
            <th colspan="5" align="center">Контакты</th>
        </tr>
        <tr>
            <th>Отдел</th>
            <th>Должность</th>
            <th>ФИО</th>
            <th>Телефон</th>
            <th>Email</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($data['contacts'][$id_firm]['emps'] as $row) {
            echo "<tr>";
            echo "<td>".$row['dept']."</td>";
            echo "<td>".$row['position']."</td>";
             $fio = $row['last_name'];
            $fio .= " ".$row['first_name'];
            $fio .= " ".$row['second_name'];
            echo "<td>".$fio."</td>";
            if (isset($row['phone'])) { echo "<td>".$row['phone']."</td>"; } else {echo "<td>-</td>";}
            if (isset($row['email'])) { echo "<td>".$row['email']."</td>"; } else {echo "<td>-</td>";}
            echo "</tr>";
        }
        ?>
    </tbody>
</table>
<br>
<?php } if(!empty($data['addres'][$id_firm]['stocks'])) { ?>
<table width='100%'>
    <thead>
        <tr>
            <th colspan="3" align="center">Адреса хозяйств/складов</th>
        </tr>
        <tr>
            <th>№</th>
            <th>Город</th>
            <th>Адрес</th>
        </tr>
    </thead>
    <tbody>  
    <?php
        foreach ($data['addres'][$id_firm]['stocks'] as $row) {
            echo "<tr>";
            echo "<td>".$row['id']."</td>";
            echo "<td>".$row['name_city']."</td>";
            echo "<td>".$row['address']."</td>";
            echo "</tr>";
        }
    ?>
    </tbody>
</table>
<br>
<?php }
if (!empty($data['table'])) { 
    foreach ($data['table'] as $table=>$column) {
        $title = '';
        if ($table == 'take_other') {
            $title = '<br><center><b>Берут не у нас</b></center>';
        }
        if ($table == 'offer') {
            $title = '<br><center><b>Предложение</b></center>';
        }
        if ($table == 'take_us') {
            $title = '<br><center><b>Берут у нас</b></center>';
        }
        if ($table == 'dont_take') {
            $title = '<br><center><b>Не берут</b></center>';
        }
        echo $title;
?>  
    <table width='100%'>
        <thead>
            <tr>
                <?php
                    echo "<tr>";
                    foreach ($column as $row) {
                       echo "<th>".$row."</th>";
                    }
                    echo "</tr>";
                ?>
            </tr>
        </thead>
        <tbody>
            <?php
                foreach ($data[$table]['product_char'] as $char) {
                    echo "<tr>";
                    foreach ($column as $id_char=>$table) {
                        if (isset($char[$id_char])) { echo "<td>".$char[$id_char]."</td>";}
                        else {echo "<td>-</td>";}
                    }
                    echo "</tr>";
                }
            ?>
        </tbody>
    </table>
<?php
    }
    

    
}