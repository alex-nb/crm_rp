<?php
$city = $data;
?>

<form onsubmit="return false;" class="form-horizontal"  id = "new_firm" method="POST">
    <div class="form-group">
        <label for="name" class="col-xs-3 control-label">Наименование*:</label>
        <div class="col-xs-9">
            <input type="text" required id="name" class="form-control" name = "name" placeholder='Введите наименование фирмы'>
        </div>
    </div>
    <div class="form-group">
        <label for="inn" class="col-xs-2 control-label">ИНН*:</label>
        <div class="col-xs-10">
            <input type="text" required id="inn" class="form-control" name = "inn"  placeholder='Введите ИНН фирмы'>
        </div>
    </div>
    <div class="form-group">
        <label for="ogrn" class="col-xs-2 control-label">ОГРН:</label>
        <div class="col-xs-10">
            <input type="text" id="ogrn" class="form-control" name = "ogrn"  placeholder='Введите ОГРН фирмы'>
        </div>
    </div>
    <label>Юридический адрес</label>
    <div class="form-group">
        <label for="id_legal_city" class="col-xs-2 control-label">Город:</label>
        <div class="col-xs-10">
            <select class="form-control" id="id_legal_city" name = "id_legal_city">
                <?php 
                    foreach ($city as $key=>$name) {
                            echo "<option value=".$key.">".$name."</option>";
                    }
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="legal_address" class="col-xs-2 control-label">Адрес:</label>
        <div class="col-xs-10">
            <input type="text"  id="legal_address" class="form-control" name = "legal_address"  placeholder='Введите полный юридический адрес БЕЗ города'>
        </div>
    </div>
    <label>Фактический адрес</label>
    <div class="form-group">
        <label for="id_fact_city" class="col-xs-2 control-label">Город:</label>
        <div class="col-xs-10">
            <select class="form-control" id="id_fact_city" name = "id_fact_city">
                <?php 
                foreach ($city as $key=>$name) {
                            echo "<option value=".$key.">".$name."</option>";
                    }
                    ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="fact_address" class="col-xs-2 control-label">Адрес:</label>
        <div class="col-xs-10">
            <input type="text"  id="fact_address" class="form-control" name = "fact_address" placeholder='Введите полный фактический адрес БЕЗ города'>
        </div>
    </div>
</form>

<script>
    $(document).ready(function(){
        $('#firmSubmit').click(function(){
            var massiv = $("#new_firm").serializeArray();
            $.ajax({
                url: 'newVrag',
                type: 'POST',
                data: $("#new_firm").serialize(),
                success: function(data) {
                    if (!isNaN(parseInt(data))) {
                        alert('Сохранено');
                        //console.log($("#vrag"));
                        $("#vrag").append( $('<option value="'+parseInt(data)+'">'+massiv[0].value+'</option>'));
                        $("#closeNewFirm").trigger('click');
                    }
                    else {
                        alert('Ошибка. Возможно такая фирма уже существует');
                    }
                    //alert(data);
                    //console.log(massiv[0].value);
                    
                },
            });  
        });  
});
</script>