<?php
if (!empty($data2)) {
    $id_prod = $data2;
}
else {$id_prod = 0;}
if (!empty($data['from'])) {
    $from = $data['from'];
}
else {$from = 0;}
$id_firm = $data3;
$where = $data['where'];
?>
<form class="form-horizontal" id = "info_offer" method="POST" action="">

    <input type="text" class="form-control unvisible" id="id_prod" name ="id_prod" readonly value=<?= $id_prod ?>>
    <input type="text" class="form-control unvisible" id="id_firm" name ="id_firm" readonly value=<?= $id_firm ?>>
    <input type="text" class="form-control unvisible" id="from" name ="from" readonly value=<?= $from ?>>
    <input type="text" class="form-control unvisible" id="where" name ="where" readonly value=<?= $where ?>>
    <div class="form-group">
        <label for=komment" class="col-xs-3 control-label">Комментарии:</label>
        <div class="col-xs-9">
            <textarea class="form-control" rows="3" id="komment" name = "komment"><?php if (isset($data[$id_prod]['komment'])) {echo $data[$id_prod]['komment'];}?></textarea>
        </div>
    </div>
    
</form>

<script>
    $(document).ready(function(){
        $('#editSubmit').click(function(){
            var form_table = <?=$from?>;
            var id_prod = <?=$id_prod?>;
            if (id_prod === 0) {
                $.ajax({
                    url: 'transferenceProduct',
                    type: 'POST',
                    data: $("#info_offer").serialize()+'&'+$(form_table).serialize(),
                    success: function(data) {
                        $("#closeEditProduct").trigger('click');
                        $("#body").html(data);
                        start();
                        //location.reload();
                    },
                });
            }
            else {
                $.ajax({
                url: 'transferenceProduct',
                type: 'POST',
                data: $("#info_offer").serialize(),
                success: function(data) {
                    $("#closeEditProduct").trigger('click');
                    $("#body").html(data);
                    start();
                    //location.reload();
                },
            }); 
            }
        });  
    });
</script>
