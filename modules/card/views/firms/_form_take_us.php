<?php
if (!empty($data['from'])) {
    $from = $data['from'];
}
else {$from = 0;}
$where = $data['where'];
if (!empty($data2)) {
    $id_prod = $data2;
}
else {$id_prod = 0;}
$id_firm = $data3;
$zakupka = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
?>

<form class="form-horizontal" id = "info_take_us" method="POST" action="">
    <input type="text" class="form-control unvisible" id="id_prod" name ="id_prod" readonly value=<?= $id_prod ?>>
    <input type="text" class="form-control unvisible" id="id_firm" name ="id_firm" readonly value=<?= $id_firm ?>>
    <input type="text" class="form-control unvisible" id="from" name ="from" readonly value=<?= $from ?>>
    <input type="text" class="form-control unvisible" id="where" name ="where" readonly value=<?= $where ?>>
    <div class="form-group">
        <label for="price" class="col-xs-3 control-label">Цена:</label>
        <div class="col-xs-9">
            <input type="number" min="0" step="any" class="form-control" id="price" name = "price" <?php if (isset($data[$id_prod]['price'])) {echo 'value="'.$data[$id_prod]['price'].'"';} else { echo "placeholder='Введите цену'";}  ?>>
        </div>
    </div>
    
    <div class="form-group">
        <label for="v" class="col-xs-3 control-label">Объем разовой поставки:</label>
        <div class="col-xs-9">
            <input onchange="rasschet()" pattern="[0-9]+([\,][0-9]+)?" type="number" step="any" min="0" class="form-control" id="v" name = "v" <?php if (isset($data[$id_prod]['v'])) {echo 'value="'.$data[$id_prod]['v'].'"';} else { echo "placeholder='Введите объем разовой поставки'";}  ?>>
        </div>
    </div>

    <div class="form-group">
        <label for="zakup" class="col-xs-3 control-label">Период закупки:</label>
        <div class="col-xs-9">
             <select onchange="rasschet()" class="form-control" id="zakup" name = "zakup">
            <?php 
                    foreach ($zakupka as $key=>$name) {
                        if ($key == $data[$id_prod]['zakup']) {
                            echo "<option selected value=".$key.">".$name."</option>";
                        }
                        else {
                            echo "<option value=".$key.">".$name."</option>";
                        }
                    }
                ?>
            </select>
        </div>
    </div>
    
    <div class="form-group">
        <label for="potr" class="col-xs-3 control-label">Потребление/месяц:</label>
        <div class="col-xs-9">
            <input type="text" class="form-control" id="potr" readonly name = "potr" <?php if (isset($data[$id_prod]['potr'])) {echo 'value="'.$data[$id_prod]['potr'].'"';} else { echo "placeholder='Введите потребление в месяц'";}  ?>>
        </div>
    </div>
    
    
    <div class="form-group">
        <label for=komment" class="col-xs-3 control-label">Комментарии:</label>
        <div class="col-xs-9">
            <textarea class="form-control" rows="3" id="komment" name = "komment"><?php if (isset($data[$id_prod]['komment'])) {echo $data[$id_prod]['komment'];}?></textarea>
        </div>
    </div>

</form>

<script>
function rasschet (){
    var volume = Number($("#v").val());
    var zakupka = Number($("#zakup").val())+1;
    $("#potr").val(volume * zakupka);
};
    $(document).ready(function(){
        $('#editSubmit').click(function(){
            var form_table = <?=$from?>;
            var id_prod = <?=$id_prod?>;
            if (id_prod === 0) {
                $.ajax({
                    url: 'transferenceProduct',
                    type: 'POST',
                    data: $("#info_take_us").serialize()+'&'+$(form_table).serialize(),
                    success: function(data) {
                        $("#closeEditProduct").trigger('click');
                        $("#body").html(data);
                        start();
                        //location.reload();
                    },
                });  
            }
            else {
                $.ajax({
                    url: 'transferenceProduct',
                    type: 'POST',
                    data: $("#info_take_us").serialize(),
                    success: function(data) {
                        $("#closeEditProduct").trigger('click');
                        $("#body").html(data);
                        start();
                        //location.reload();
                    },
                });  
            }
        });  
    });
</script>
   