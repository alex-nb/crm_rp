        <li role="presentation"><a href="../../../site/site/index">Назад</a></li>
        <li role="presentation"><a href="making">Производство</a></li> 
        <li role="presentation"><a href="equipment">Уровень оснащения</a></li> 
        <li role="presentation"><a href="../production/directions">Направления</a></li> 
        <li role="presentation"><a href="edit" target="_blank">Создать контрагента</a></li> 
</ul>
<?php
/*echo "<pre>";
var_dump($data);
echo "</pre>";*/
?>
<center>Информация по контрагентам</center><br>

<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover"  id="table_firms" width="100%">
    <thead>
        <tr class="info">
            <th>ИД</th>
            <th>Наименование</th>
            <th>ИНН</th>
            <th>ОГРН</th>
            <th>Юридический адрес</th>
            <th>Фактический адрес</th>
            <th>Тип контрагента</th>
            <th>Направления работы</th>
            <th>Уровень оснащения</th>
            <th>Производство</th>
            <th width="5%"></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($data as $row) {
            $span = "";
            if (isset($row['id'])) {
                $str = '';
                echo "<tr>";
                echo "<td>".$row['id']."</td>";
                echo "<td id='name'>".$row['name']."</td>";
                echo "<td>".$row['inn']."</td>";
                echo "<td>".$row['ogrn']."</td>";
                if (isset($row['name_legal_city'])) { echo "<td>".$row['name_legal_city'].", ".$row['legal_address']."</td>"; }
                else {if (!empty($row['legal_address'])) {echo "<td>".$row['legal_address']."</td>";} else {echo "<td>-</td>";}}
                 if (isset($row['name_fact_city'])) { echo "<td>".$row['name_fact_city'].", ".$row['fact_address']."</td>"; }
                else {if (!empty($row['fact_address'])) {echo "<td>".$row['fact_address']."</td>";} else {echo "<td>-</td>";}}
                if (isset($row['types'])) {
                    if (in_array("Покупатель", $row['types'])) {
                        $span = "<span class='glyphicon glyphicon-eye-open pointer' onclick='openUrl(\"editProduction\", {id_firm:\"".$row['id']."\", name:\"".$row['name']."\"})' data-toggle='tooltip' title='Продуктовый блок'></span>";
                    }
                    echo "<td>";
                    foreach ($row['types'] as $type) {
                        echo $type."<br>";
                    }
                    echo "</td>";
                }
                else {echo "<td>-</td>";}
                if (isset($row['direction'])) {
                    echo "<td>";
                    foreach ($row['direction'] as $dir) {
                        $direction = implode ("->", array_reverse($dir));
                        echo $direction."<br>";
                    }
                    echo "</td>";
                }
                else {echo "<td>-</td>";}
                if (isset($row['equipment'])) {
                    echo "<td>";
                    foreach ($row['equipment'] as $equip) {
                        $equipment = implode ("->", array_reverse($equip));
                        echo $equipment."<br>";
                    }
                    echo "</td>";
                }
                else {echo "<td>-</td>";}
                if (isset($row['making'])) {
                    echo "<td>";
                    foreach ($row['making'] as $mak) {
                        $making = implode ("->", array_reverse($mak));
                        echo $making."<br>";
                    }
                    echo "</td>";
                }
                else {echo "<td>-</td>";}
                echo "<td>".$span."
                        <span class='glyphicon glyphicon-pencil pointer' data-toggle='tooltip' title='Редактировать' onclick='openUrl(\"edit\", {id_firm:\"".$row['id']."\"})'></span>
                        <span class='glyphicon glyphicon-trash pointer' id='".$row['id']."' data-toggle='tooltip' title='Удалить'></span>
                    </td>";
                echo "</tr>";
            }
        }
        ?>
    </tbody>
    </table>
</div>

<script>
$(document).ready(function() {
    $('#table_firms').DataTable({
        fixedHeader: true,
        "order": [[ 0, 'asc' ]],
        "displayLength": 25,
        "language": {
                url: '//cdn.datatables.net/plug-ins/1.10.16/i18n/Russian.json'
            },
            "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        });
    });
    
    $('.glyphicon-trash').click(function() {
        var id_firm = $(this).attr('id');
        if (confirm("Подверждаете удаление контрагента?")) {
            $.ajax({
                url : "deleteFirm",
                type: "POST",
                data: {id_firm:id_firm},
                success: function(data){  
                   if (data == 1) {
                       data = "Удаление завершено";
                       alert(data);
                       location.reload();
                   }
                   else { alert(data); }
                }
            });   
        }
    });
    
    function openUrl(url, post) {
        if ( post ) {
            var form = $("<form target='_blank' method='POST' style='display:none;'></form>").attr({
                    action: url
            }).appendTo(document.body);

            for(var key in post) {
                $('<input type="hidden" />').attr({
                    name: key,
                    value: post[key]
                }).appendTo(form);
            }
            //form.appendTo(document.body); // Необходимо для некоторых браузеров
            form.submit();
            form.remove();
        } else {
            window.open( url );
        }
    };
</script>