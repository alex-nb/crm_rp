<?php
    $id = $data2;
    $vet = [0=>'Не требуется', 1=>'Желательно', 2=>'Обязательно'];
    //var_dump($data[$id]['classes']);
    //var_dump($data[$id]['direction']);
?>

<form class="form-horizontal" id = "form" method="POST" action="edit">
    <div class="form-group">
        <label for="id" class="col-xs-3 control-label">ИД:</label>
        <div class="col-xs-9">
            <input type="text" class="form-control" id="id" name ="id" readonly value=<?= $id ?>>
        </div>
    </div>
    
    <div class="form-group">
        <label for="name" class="col-xs-3 control-label">Номенклатура*:</label>
        <div class="col-xs-9">
            <input type="text" required class="form-control" id="name" name = "name" <?php if (!empty($id)) {echo 'value="'.$data[$id]['name'].'"';} else { echo "placeholder='Введите наименование товара'";}  ?>>
        </div>
    </div>
    
    <div class="form-group">
        <label for=application" class="col-xs-3 control-label">Применение:</label>
        <div class="col-xs-9">
            <textarea class="form-control" rows="3" id="application" name = "application"><?php if (isset($data[$id]['application'])) {echo $data[$id]['application'];}?></textarea>
        </div>
    </div>
    
    <div class="form-group">
        <label for=appearance" class="col-xs-3 control-label">Описание:</label>
        <div class="col-xs-9">
            <textarea class="form-control" rows="3" id="appearance" name = "appearance"><?php if (isset($data[$id]['appearance'])) {echo $data[$id]['appearance'];}?></textarea>
        </div>
    </div>
    
    <div class="form-group">
        <label for="veterinary" class="col-xs-3 control-label">Ветеринарное свидетельство:</label>
        <div class="col-xs-9">
            <select class="form-control" id="veterinary" name = "veterinary">
            <?php 
                    foreach ($vet as $key=>$name) {
                        if ($key == $data[$id]['veterinary']) {
                            echo "<option selected value=".$key.">".$name."</option>";
                        }
                        else {
                            echo "<option value=".$key.">".$name."</option>";
                        }
                    }
                ?>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="id_class" class="col-xs-3 control-label">Группа/класс:</label>
        <div class="col-xs-9" id="group">
            <ul class="ul-treefree ul-dropfree">
            <?php
            if (isset($data[$id]['classes'])) {
                echo makeBeatiful_group($data['all_group_class'], $data[$id]['classes']);
            }
            else {
                if (isset($data['all_group_class'])) {
                    echo  makeBeatiful_group($data['all_group_class']);
                }
                else {echo "Групп нет";} 
            }
             ?>
            </ul>
        </div>
    </div>
   

    <div class="form-group">
            <label for="direction" class="col-xs-3 control-label">Направления:</label>
            <div class="col-xs-9">
                <ul class="ul-treefree ul-dropfree">
            <?php
            if (isset($data[$id]['direction'])) {
                echo  makeBeatiful_direction($data['all_directions'], $data[$id]['direction']);
            }
            else {
                if (isset($data['all_directions'])) {echo  makeBeatiful_direction($data['all_directions']);}
                else {echo "Направлений нет";}
            }
             ?>
                </ul>
            </div>
    </div>
    
    <!-- Футер модального окна -->
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
      <button type="submit" class="btn btn-primary">Сохранить изменения</button>
    </div>
    
</form>

<script>
$(document).ready(function() {
    $(".ul-dropfree div.drop").click(function() {
            if ($(this).nextAll("ul").css('display')=='none') {
                    $(this).nextAll("ul").slideDown(400);
                    $(this).css({'background-position':"-11px 0"});
            } else {
                    $(this).nextAll("ul").slideUp(400);
                    $(this).css({'background-position':"0 0"});
            }
            setTimeout( function () {
              $('#myModal').modal('handleUpdate');
              } , 500 );
    });
    
    $(".ul-dropfree").find("ul").slideUp(400).parents("li").children("div.drop").css({'background-position':"0 0"});
    
    $("input:checkbox").on('change', function() {
        var all_check = $(this).parent().siblings('ul').find("input:checkbox");
       //отмечаем или снимаем отметку с детей
       if ($(this).prop("checked") == true){
           $(this).parent("label").addClass("shine");
           $(all_check).parent("label").addClass("shine");
           $(all_check).prop({"checked":true, "indeterminate":false});
       } else {
           $(all_check).prop({"checked":false, "indeterminate":false});
           $(this).parent("label").removeClass("shine");
           $(all_check).parent("label").removeClass("shine");
       }
       check_childe($(this));

        var state = $(this).prop("checked");
        var this_label = $(this).parent().text();
        $(this).parents("div#group").each(function() {
            if (state) {
                $(this).find("label").each(function() {
                    var curr_lable = $(this).text();
                    $(this).children("input:checkbox").prop({"disabled":true});
                    if (curr_lable == this_label) {
                        $(this).children("input:checkbox").prop({"checked":true, "disabled":false});
                        $(this).addClass("shine");
                    }
                    check_childe($(this).children("input:checkbox"));
                });
            }
            else {
                var all_li = $(this).find("li");
                $(all_li).each(function() {
                    if ($(this).children("ul").length > 0) {
                        $(this).find("input:checkbox").prop({"disabled":true, "checked":false, "indeterminate":false});
                    }
                    else {
                        $(this).find("input:checkbox").prop({"disabled":false, "checked":false, "indeterminate":false});
                    }
                });
                $(this).find("label").removeClass("shine");
            }
        });
        $(this).prop({"disabled":false});
    });
    
        function check_childe (obj) {
         $(obj).parents("li").each(function(){ //находим родителей у данного чекбокса
             var all = $(this).find("input:checkbox").not(":first"); //выбираем всех чекбоксов-детей-внуков и т.д.
             var check = $(all).filter("input:checkbox:checked"); //выбираем чекнутые чекбоксы среди них
             if (check.length !== 0 || all.length !== 0) {
                 if (check.length === all.length) { //если кол-во чекнутых равно общему кол-ву чекбоксов, то чекаем и этот чекбокс
                     $(this).find("input:checkbox:first").prop({"indeterminate":false, "checked":true});
                     $(this).find("label:first").addClass("shine");
                 }
                 else if (check.length === 0) { //если кол-во чекнутых равно 0, то отменяем чек у этого чекбокса
                     $(this).find("input:checkbox:first").prop({"indeterminate":false, "checked":false});
                     $(this).find("label:first").removeClass("shine");
                 }
                 else if (check.length < all.length) { //если чекнутые есть, но их меньше, чем всех чекбоксов, то делаем этот indeterminate
                     $(this).find("input:checkbox:first").prop({"indeterminate":true, "checked":false});
                     $(this).find("label:first").addClass("shine");
                 }
             }
         });
    }
});
</script>

<?php 
function makeBeatiful_group($group_class, $selected = NULL, $id_parrent = NULL) {
    $out = NULL;
    foreach ($group_class as $row) {
        $flag = true; 
        if ($row['id_parrent'] == $id_parrent) {     
            $return_func = makeBeatiful_group($group_class, $selected, $row['id']);   
            if (empty($return_func)) {
                if(!empty($selected)) {
                    foreach ($selected as $sel) {
                        if(isset($sel[$row['id']])) {
                            $out .= "<li>\n<label class='shine'><input type='checkbox' checked name='classes[]' value='".$row['id']."'/>".$row['name']."</label>\n</li>\n";
                            $flag = false;
                            break;
                        }
                    }
                    if($flag) {$out .= "<li>\n<label><input type='checkbox' disabled name='classes[]' value='".$row['id']."'/>".$row['name']."</label>\n</li>\n";}
                }
                else {$out .= "<li>\n<label><input type='checkbox' name='classes[]' value='".$row['id']."'/>".$row['name']."</label>\n</li>\n";}
            }
            else {
                if (!empty($selected)) {
                    foreach ($selected as $sel) {      
                        if (isset($sel[$row['id']])) {                     
                            $out .="<li>\n<div class='drop'></div><label class='shine'><input type='checkbox' checked disabled/>".$row['name']."</label>\n<ul>\n";
                            $flag = false;  
                            break;
                        }
                    }
                }
                if($flag) {$out .="<li>\n<div class='drop'></div><label><input type='checkbox' disabled/>".$row['name']."</label>\n<ul>\n";}
               $out .= $return_func."\n</ul>\n</li>\n";  
            }          
        } 
    }
    return $out;
}

function makeBeatiful_direction($direction, $selected = NULL, $id_parrent = NULL) {
    $out = NULL;
    foreach ($direction as $row) {
        $flag = true; 
        if ($row['id_parrent'] == $id_parrent) {     
            $return_func = makeBeatiful_direction($direction, $selected, $row['id']);   
            if (empty($return_func)) {
                if(!empty($selected)) {
                    foreach ($selected as $sel) {
                        if(isset($sel[$row['id']])) {
                            $out .= "<li><label class='shine'><input type='checkbox' checked name='direction[]' value='".$row['id']."'/>".$row['name']."</label></li>";
                            $flag = false;
                            break;
                        }
                    }
                }
                if($flag) {$out .= "<li><label><input type='checkbox' name='direction[]' value='".$row['id']."'/>".$row['name']."</label></li>";}
            }
            else {
                if (!empty($selected)) {
                    foreach ($selected as $sel) {      
                        if (isset($sel[$row['id']])) {                     
                            $out .="<li><div class='drop'></div><label class='shine'><input type='checkbox' checked/>".$row['name']."</label><ul>";
                            $flag = false;  
                            break;
                        }
                    }
                }
                if($flag) {$out .="<li><div class='drop'></div><label><input type='checkbox'/>".$row['name']."</label><ul>";}
               $out .= $return_func."</ul></li>";  
            }          
        } 
    }
    return $out;
}