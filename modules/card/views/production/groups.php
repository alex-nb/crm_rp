        <li role="presentation"><a href="../../../site/site/index">Назад</a></li> 
        <li role="presentation"><a href="index">Товары</a></li> 
        <li role="presentation"><a href="directions">Направления</a></li> 
</ul> 
<ul class="ul-treefree ul-dropfree">
<li ><h4>Новая группа<span onclick="foo('d0','block');"  data-toggle="tooltip" title="Добавить новую группу">&#43; </span></h4>
    <div id='d0' style='display:none;'>
      <form method='POST' action='editClass'>
        <input type='hidden' name='editType' value='1'/>
        <input type='hidden' name='id' value='0'/>
        <input type='text' name='name'  placeholder='Имя нового элемента'/>
        <input type='submit' value='ОК'/>
      </form>
    </div>
</li>
<?php
    if (isset($data)) {
        echo makeBeatiful_group($data);
    } ?>
</ul>
<script>
$(document).ready(function() {
    //localStorage.clear();
    var treeState = { // объект, который хранит состояния веток
        state: JSON.parse(localStorage.getItem('branchOpenedGroups')) || {},
        setOpened: function(element) {
            this.state[this._getKey(element)] = true;
            this._save();
        },
        setClosed: function(element) {
            delete this.state[this._getKey(element)];
            this._save();
        },
        isClosed: function(element) {
            return !this.state[this._getKey(element)];
        },
        _save: function() {
            localStorage.setItem('branchOpenedGroups', JSON.stringify(this.state));
        },
        _getKey: function(element) {
            var ixs = [];
            $(element).parentsUntil('ul-dropfree', 'li')
                .each(function(ix, item) {
                    ixs.unshift($(item).index());
                });
            return ixs.join(',');
        }
    };
    //$(".ul-dropfree").find("li:has(ul)").prepend('<div class="drop"></div>');
    $(".ul-dropfree div.drop").click(function() {
            if ($(this).nextAll("ul").css('display')=='none') {
                    $(this).nextAll("ul").slideDown(400);
                    $(this).css({'background-position':"-11px 0"});
                    // сохраняем состояние ветви
                    treeState.setOpened(this);
            } else {
                    $(this).nextAll("ul").slideUp(400);
                    $(this).css({'background-position':"0 0"});
                    // сохраняем состояние ветви
                    treeState.setClosed(this);
            }
            localStorage.setItem('list', this.value);
    });
    //$(".ul-dropfree").find("ul").slideUp(400).parents("li").children("div.drop").css({'background-position':"0 0"});
    $(".ul-dropfree").find("ul").each(function(ix, branch) {
        $(branch).parent("li")
            .children("div.drop")
            .css({'background-position': "0 0"});
        if (treeState.isClosed(branch)) {
            $(branch).slideUp(400);
        }
    });
});
function foo(id,block) { 
        if (document.getElementById(id).style.display == "none")
           {document.getElementById(id).style.display = block }
        else 
           {document.getElementById(id).style.display = "none"}
    }
        
    function deleterow(id) {
        //id_user = $(this).attr('id');
        if (confirm("Подверждаете удаление?")) {
            $.ajax({
                url : "deletegroup",
                type: "POST",
                data: {id_group:id},
                success: function(data){  
                   if (data == 1) {
                       data = "Удаление завершено";
                       alert(data);
                       location.reload();
                   }
                   else { alert(data); }
                }
            });   
        }
    }
</script>

<?php
function makeBeatiful_group($group_class, $id_parrent = NULL) {
    $out = NULL;
    foreach ($group_class as $row) {
        if ($row['id_parrent'] == $id_parrent) {     
            $return_func = makeBeatiful_group($group_class, $row['id']);   
            if (empty($return_func)) {
                $out .= "<li>".$row['name']."<span onclick=\"foo('u".$row['id']."','inline-block');\" data-toggle='tooltip' title='Изменить наименование'>&#9881;</span>
      <div id='u".$row['id']."' style='display:none;'>
        <form method='POST' action='editClass'>
          <input type='hidden' name='editType' value='0'/>
          <input type='hidden' name='id' value='".$row['id']."'/>
          <input type='text' name='name'  value='".$row['name']."'/>
          <input type='submit' value='ОК'/>
        </form>
      </div>
      
      <span onclick=\"foo('d".$row['id']."','block');\" data-toggle='tooltip' title='Добавить дочерний класс'>&#43;</span>
      <div id='d".$row['id']."' style='display:none;'>
        <form method='POST' action='editClass'>
          <input type='hidden' name='editType' value='1'/>
          <input type='hidden' name='id' value='".$row['id']."'/>
          <input type='text' name='name'  placeholder='Имя нового элемента'/>
          <input type='submit' value='ОК'/>
        </form>
      </div>
      <span onclick = 'deleterow(".$row['id'].");' data-toggle='tooltip' title='Удалить'>&#9249;</span>
      </li>";
            }
            else {
               $out .="<li><div class='drop'></div>".$row['name']."<span onclick=\"foo('u".$row['id']."','inline-block');\" data-toggle='tooltip' title='Изменить наименование'>&#9881;</span>
      <div id='u".$row['id']."' style='display:none;'>
        <form method='POST' action='editClass'>
          <input type='hidden' name='editType' value='0'/>
          <input type='hidden' name='id' value='".$row['id']."'/>
          <input type='text' name='name'  value='".$row['name']."'/>
          <input type='submit' value='ОК'/>
        </form>
      </div>
      
      <span onclick=\"foo('d".$row['id']."','block');\" data-toggle='tooltip' title='Добавить дочерний класс'>&#43; </span>
      <div id='d".$row['id']."' style='display:none;'>
        <form method='POST' action='editClass'>
          <input type='hidden' name='editType' value='1'/>
          <input type='hidden' name='id' value='".$row['id']."'/>
          <input type='text' name='name'  placeholder='Имя нового элемента'/>
          <input type='submit' value='ОК'/>
        </form>
      </div>
      <span onclick = 'deleterow(".$row['id'].");' data-toggle='tooltip' title='Удалить'>&#9249;</span>
      <ul>";
               $out .= $return_func."</ul></li>";  
            }          
        } 
    }
    return $out;
}
/*
function makeBeatiful_group($group_class, $id_parrent = NULL) {
    $out = NULL;
    foreach ($group_class as $row) {
        if ($row['id_parrent'] == $id_parrent) {     
            $return_func = makeBeatiful_group($group_class, $row['id']);   
            if (empty($return_func)) {
                $out .= "<li>".$row['name']."</li>\n";
            }
            else {
                $out .= "<fieldset><legend>−</legend><li>".$row['name']."<fieldset><ul>\n";
                $out .= $return_func."</ul></li></fieldset></fieldset>\n";  
            }          
        } 
    }
    return $out;
}*/