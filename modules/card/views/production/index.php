        <li role="presentation"><a href="../../../site/site/index">Назад</a></li> 
        <li role="presentation"><a href="groups">Группы/классы</a></li> 
        <li role="presentation"><a href="directions">Направления</a></li> 
        <li role="presentation"><a class='open_modal pointer'>Создать новый товар</a></li>
</ul> 
<?php
/*$arr1 = array();
$arr2 = array ();
$donttouch_dir = array_intersect($arr1, $arr2); 
var_dump($donttouch_dir);*/
?>
<?="<center>Информация по товарам</center>";?>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover"  id="table_prod" width="100%">
        <thead>
            <tr class="info">
                <th>ИД</th>
                <th>Номенклатура</th>
                <th>Применение</th>
                <th>Описание</th>
                <th>Ветеринарное свидетельство</th>
                <th>Классы/Группы</th>
                <th>Направления</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php 
            if (isset($data)) {
                 foreach ($data as $prod) {
                    if (isset($prod['id'])) {
                        echo "<tr>";
                        echo "<td>".$prod['id']."</td>"; 
                        echo "<td>".$prod['name']."</td>"; 
                        if (isset($prod['application'])) {
                            echo "<td>".$prod['application']."</td>"; 
                        }
                        else { echo "<td>-</td>";}
                        if (isset($prod['appearance'])) {
                            echo "<td>".$prod['appearance']."</td>"; 
                        }
                        else { echo "<td>-</td>";}
                        if ($prod['veterinary'] == 2) { echo "<td>Обязательно</td>"; } elseif ($prod['veterinary'] == 1) { echo "<td>Желательно</td>";}  else { echo "<td>Не требуется</td>"; }
                        $classes = NULL;
                        $group = NULL;
                        if (isset($prod['classes'])) {
                            echo "<td>";
                             foreach ($prod['classes'] as $class) {
                                 $classes = implode ("->", array_reverse($class));
                                 echo $classes."<br>";
                             }
                             echo "</td>";
                         }
                         else {echo "<td>-</td>";}

                         if (isset($prod['direction'])) {
                             echo "<td>";
                             foreach ($prod['direction'] as $dir) {
                                 $direction = implode ("->", array_reverse($dir));
                                 echo $direction."<br>";
                             }
                             echo "</td>";
                         }
                         else {echo "<td>-</td>";}
                         echo "<td><span class='glyphicon glyphicon-pencil pointer' id='".$prod['id']."' data-toggle='tooltip' title='Редактирвоать'></span>    "
                                 . "<span class='glyphicon glyphicon-trash pointer' id='".$prod['id']."' data-toggle='tooltip' title='Удалить'></span></td>";
                         echo "</tr>";
                    }         
                }
            }
            ?>
        </tbody>
    </table>
</div>

<!-- HTML-код модального окна для создания/удаления-->
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
        <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">Товар</h4>
            </div>
        <!-- Основное содержимое модального окна -->
            <div class="modal-body" id="modal-body">
            </div>
        </div>
    </div>
</div>


<script>
$(document).ready(function(){
            $('.glyphicon-pencil').click(function() {
                var id_prod = $(this).attr('id');
                $.ajax({
                    url : "form",
                    type: "POST",
                    data: {id_prod:id_prod},
                    success: function(html){
                       $('#modal-body').append(html);
                       $('#myModal').modal('show');
                    }
                });   
        });

            $('#myModal').on('hidden.bs.modal', function () {
                $("#modal-body").html('');
            });   

            $('.open_modal').click(function() {
            $.ajax({
                url : "form",
                type: "POST",
                data: {},
                success: function(html){
                   $('#modal-body').append(html);
                   $('#myModal').modal('show');
                }
            });   
        });
        
        $('.glyphicon-trash').click(function() {
        var id_prod = $(this).attr('id');
        if (confirm("Подверждаете удаление товара?")) {
            $.ajax({
                url : "deleteprod",
                type: "POST",
                data: {id_prod:id_prod},
                success: function(data){  
                   if (data == 1) {
                       data = "Удаление завершено";
                       alert(data);
                       location.reload();
                   }
                   else { alert(data); }
                }
            });   
        }
    });    

    $('#table_prod').DataTable({
        fixedHeader: true,
        "order": [[ 0, 'asc' ]],
        "displayLength": 25,
        "language": {
            url: '//cdn.datatables.net/plug-ins/1.10.16/i18n/Russian.json'
        },
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
    });
} );    
</script>
