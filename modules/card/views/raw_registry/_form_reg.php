<?php
$srochno = ['15m'=>'В течении 15 минут', '30m'=>'В течении 30 минут', 'h'=>'В течении 1 часа', '1d'=>'В течении 1 дня', '3d'=>'В течении 3 дней'];
$deistvie = ['zapros'=>'Запрос цены', 'skidka'=>'Необходимость скидки'];
$vid_skidki = ['loyal'=>'На лояльность', 'konk'=>'Конкурент'];
$vid_dostavki = ['with'=>'С доставкой', 'whithout'=>'Без доставки'];
$uslovia_oplati = ['fact' => 'По факту', 'predoplata' => 'Предоплата', 'otsrochka' => 'Отсрочка', 'inoe' => 'Иное'];
$where_price = ['sklad' => 'На складе РП', 'dostavka' => 'С доcтавкой до потребителя (транзит)', 'postavshik' => 'Цена на складе у поставщика'];
$status_otveta = ['yes' => 'Да', 'no' => 'Нет', 'alternativa' => 'Альтернатива'];
$prodazi = ['1' => 'Успешна', '2' => 'Не успешна'];
?>

<form id="form" method="POST" action="edit" class="form-horizontal">
    <input type="hidden" name="id" value="<?php if (isset($data['id'])) {echo $data['id'];} else { echo 0;} ?>" />
    <input type="hidden" name="who" value="<?=$data['who']?>" />
    
    <div class="form-group">
        <label for="id_parrent" class="col-xs-2 control-label">Привязать к записи №: </label>
        <div class="col-xs-10">
            <input id="id_parrent" name="id_parrent" type="text" class="form-control" <?php if (isset($data['id_parrent'])){echo "value='".$data['id_parrent']."'";} else {echo "placeholder='Введите № записи'";}?> />
        </div>
    </div>
    
    <div class="form-group">
        <label for="date_add" class="col-xs-2 control-label">Дата: </label>
        <div class="col-xs-10">
            <input id="date_add" name="date_add" type="text" class="form-control d" <?php if (isset($data['date_add'])){echo "value='".$data['date_add']."'";} else {echo "placeholder='Выберите дату'";}?> />
        </div>
    </div>
    
    <div class="form-group">
        <label for="urgency" class="col-xs-2 control-label">Срочность: </label>
        <div class="col-xs-10">
            <select class="form-control" id="urgency" name = "urgency">
            <?php 
                    foreach ($srochno as $key=>$name) {
                        if ($key == $data['urgency']) {
                            echo "<option selected value=".$key.">".$name."</option>";
                        }
                        else {
                            echo "<option value=".$key.">".$name."</option>";
                        }
                    }
                ?>
            </select>
        </div>
    </div>
    
    <div class="form-group">
        <label for="product" class="col-xs-2 control-label">Продукт</label>
        <div class="col-xs-10">
            <input id="product" name="product" type="text" class="form-control" <?php if (isset($data['product'])) {echo "value='".$data['product']."'";} else {echo "placeholder='Введите наименование продукта'";} ?>/>
        </div>
    </div>
    
    <div class="form-group">
        <label for="qualitative" class="col-xs-2 control-label">Примечание по продукту</label>
        <div class="col-xs-10">
            <textarea id="qualitative" name="qualitative" class="form-control" <?php if (isset($data['qualitative'])) {echo ">".$data['qualitative']."</textarea>";} else {echo "placeholder='Введите примечания по продукту. (Можете приложить ссылку с битрикса на прикрепляемый файл.'></textarea>";}?>
        </div>
    </div>
    
    <div class="form-group">
        <label for="ssilka" class="col-xs-2 control-label">Ссылка на файл</label>
        <div class="col-xs-10">
            <input id="ssilka" name="ssilka" type="text" class="form-control" <?php if (isset($data['ssilka'])) {echo "value='".$data['ssilka']."'";} else {echo "placeholder='Ссылка на файл'";}?> />
        </div>
    </div>
    
    <div class="form-group">
        <label for="bulk" class="col-xs-2 control-label">Необх. Объем</label>
        <div class="col-xs-10">
            <input id="bulk" name="bulk" type="text" class="form-control" <?php if (isset($data['bulk'])) {echo "value='".$data['bulk']."'";} else {echo "placeholder='Введите необходимый объем'";}?> />
        </div>
    </div>
    
    <div class="form-group">
        <label for="times_per_month" class="col-xs-2 control-label">Месячное потребление</label>
        <div class="col-xs-10">
            <input id="times_per_month" name="times_per_month" type="text" class="form-control" <?php if (isset($data['times_per_month'])) {echo "value='".$data['times_per_month']."'";}  else {echo "placeholder='Введите месячное потребление'";}?> />
        </div>
    </div>
    
    <div class="form-group">
        <label for="region" class="col-xs-2 control-label">Область</label>
        <div class="col-xs-10">
            <input id="region" name="region" type="text" class="form-control" <?php if (isset($data['region'])) {echo "value='".$data['region']."'";}  else {echo "placeholder='Введите область'";}?> />
        </div>
    </div>
    
    <div class="form-group">
        <label for="district" class="col-xs-2 control-label">Район</label>
        <div class="col-xs-10">
            <input id="district" name="district" type="text" class="form-control" <?php if (isset($data['district'])) {echo "value='".$data['district']."'";} else {echo "placeholder='Введите район'";}?> />
        </div>
    </div>
    
    <div class="form-group">
        <label for="address_delivery" class="col-xs-2 control-label">Адрес доставки</label>
        <div class="col-xs-10">
            <input id="address_delivery" name="address_delivery" type="text" class="form-control" <?php if (isset($data['address_delivery'])) {echo "value='".$data['address_delivery']."'";} else {echo "placeholder='Введите адрес доставки'";}?> />
        </div>
    </div>
    
    <div class="form-group">
        <label for="farm_name" class="col-xs-2 control-label">Название хозяйства</label>
        <div class="col-xs-10">
            <input id="farm_name" name="farm_name" type="text" class="form-control" <?php if (isset($data['farm_name'])) {echo "value='".$data['farm_name']."'";} else {echo "placeholder='Введите название хозяйства'";}?> />
        </div>
    </div>

    <div class="form-group">
        <label for="deistvie" class="col-xs-2 control-label">Действие</label>
        <div class="col-xs-10">
            <select onchange="check_it()"  class="form-control" id="deistvie" name = "deistvie">
            <?php 
                    foreach ($deistvie as $key=>$name) {
                        if ($key == $data['deistvie']) {
                            echo "<option selected value=".$key.">".$name."</option>";
                        }
                        else {
                            echo "<option value=".$key.">".$name."</option>";
                        }
                    }
                ?>
            </select>
        </div>
    </div>
    
    <div id="skidochka" class="form-group unvisible">
        <label for="vid_skidki" class="col-xs-2 control-label">Вид скидки</label>
        <div class="col-xs-10">
            <select class="form-control" id="vid_skidki" name = "vid_skidki">
                 <option>Вид скидки</option>
            <?php 
                    foreach ($vid_skidki as $key=>$name) {
                        if ($key == $data['vid_skidki']) {
                            echo "<option selected value=".$key.">".$name."</option>";
                        }
                        else {
                            echo "<option value=".$key.">".$name."</option>";
                        }
                    }
                ?>
            </select>
        </div>
    </div>
    
    <div class="form-group">
        <label for="competitor_name" class="col-xs-2 control-label">Конкурент (название)</label>
        <div class="col-xs-10">
            <input id="competitor_name" name="competitor_name" type="text" class="form-control" <?php if (isset($data['competitor_name'])) {echo "value='".$data['competitor_name']."'";} else {echo "placeholder='Введите название конкурента'";}?> />
        </div>
    </div>
    
    <div class="form-group">
        <label for="price_konkur" class="col-xs-2 control-label">Цена конкурента</label>
        <div class="col-xs-10">
            <input onchange="get_dostavka()" id="price_konkur" name="price_konkur" type="text" class="form-control" <?php if (isset($data['price_konkur'])) {echo "value='".$data['price_konkur']."'";} else {echo "placeholder='Введите цену'";}?>/>
        </div>
    </div>
    
    <div id="dostavochka" class="form-group unvisible">
        <label for="price_dostavka" class="col-xs-2 control-label">Вид доставки</label>
        <div class="col-xs-10">
           <select class="form-control" id="price_dostavka" name = "price_dostavka">
               <option>Вид доставки</option>
            <?php 
                    foreach ($vid_dostavki as $key=>$name) {
                        if ($key == $data['price_dostavka']) {
                            echo "<option selected value=".$key.">".$name."</option>";
                        }
                        else {
                            echo "<option value=".$key.">".$name."</option>";
                        }
                    }
                ?>
            </select>
        </div>
    </div>
   
    <div class="form-group">
        <label for="uslovia_oplati" class="col-xs-2 control-label">Условия оплаты</label>
        <div class="col-xs-10">
            <select onchange="oplata()"  class="form-control" id="uslovia_oplati" name = "uslovia_oplati">
                <option>Выберите условие оплаты</option>
            <?php 
                    foreach ($uslovia_oplati as $key=>$name) {
                        if ($key == $data['uslovia_oplati']) {
                            echo "<option selected value=".$key.">".$name."</option>";
                        }
                        else {
                            echo "<option value=".$key.">".$name."</option>";
                        }
                    }
                ?>
            </select>
        </div>
    </div>
    
    <div id="dop_oplata" class="form-group unvisible">
        <label for="dop_uslovia" class="col-xs-2 control-label">Дополнительная информация</label>
        <div class="col-xs-10">
            <textarea id="dop_uslovia" name="dop_uslovia" class="form-control" <?php if (isset($data['dop_uslovia'])) {echo ">".$data['dop_uslovia']."</textarea>";} else {echo "placeholder='Укажите время отсрочки или иные условия оплаты'></textarea>";}?>
        </div>
    </div>

    <div class="form-group">
        <label for="task" class="col-xs-2 control-label">Дать цену где</label>
        <div class="col-xs-10">
            <select class="form-control" id="task" name = "task">
                <option>Выберите где дать цену</option>
            <?php 
                    foreach ($where_price as $key=>$name) {
                        if ($key == $data['task']) {
                            echo "<option selected value=".$key.">".$name."</option>";
                        }
                        else {
                            echo "<option value=".$key.">".$name."</option>";
                        }
                    }
                ?>
            </select>
        </div>
    </div>
    
    <div id="price_neeeeed" class="form-group unvisible">
        <label for="price_need" class="col-xs-2 control-label">Цена</label>
        <div class="col-xs-10">
            <input id="price_need" name="price_need" type="text" class="form-control" <?php if (isset($data['price_need'])) {echo "value='".$data['price_need']."'";} else {echo "placeholder='Цена'";}?> />
        </div>
    </div>

    <div class="form-group">
        <label for="notes" class="col-xs-2 control-label">Примечания</label>
        <div class="col-xs-10">
            <textarea id="notes" name="notes" class="form-control" <?php if (isset($data['notes'])) {echo ">".$data['notes']."</textarea>";} else {echo "placeholder='Введите примечания по задаче'></textarea>";}?>
        </div>
    </div>
    
    <div class="form-group">
        <label for="chat" class="col-xs-2 control-label">Диалог</label>
        <div class="col-xs-10">
            <textarea id="chat" name="chat" class="form-control" <?php if (isset($data['chat'])) {echo ">".$data['chat']."</textarea>";} else {echo "placeholder='Форма диалога'></textarea>";}?>
        </div>
    </div>
    
    <?php
    if (in_array(3, $data2)) {
    ?>
    <div class="reg_purchase">
        <h3>Отдел снабжения</h3>
        <div class="form-group">
            <label for="purchase" class="col-xs-2 control-label">Статус ответа</label>
            <div class="col-xs-10">
                <select onchange="get_comment()" class="form-control" id="purchase" name = "purchase">
                    <option>пока нет</option>
                <?php 
                        foreach ($status_otveta as $key=>$name) {
                            if ($key == $data['purchase']) {
                                echo "<option selected value=".$key.">".$name."</option>";
                            }
                            else {
                                echo "<option value=".$key.">".$name."</option>";
                            }
                        }
                    ?>
                </select>
            </div>
        </div>
        
        <div id="comment" class="form-group unvisible">
            <label for="komment" class="col-xs-2 control-label">Комментарий от закупщика</label>
            <div class="col-xs-10">
                <textarea id="komment" name="komment" class="form-control" <?php if (isset($data['komment'])) {echo ">".$data['komment']."</textarea>";} else {echo "placeholder='Введите комментарий'></textarea>";}?>
            </div>
        </div>

        <div class="form-group">
            <label for="purchase_date_answer" class="col-xs-2 control-label">Дата ответа</label>
            <div class="col-xs-10">
                <input id="purchase_date_answer" name="purchase_date_answer" type="text" class="form-control d" <?php if (isset($data['purchase_date_answer'])) {echo "value='".$data['purchase_date_answer']."'";} else {echo "placeholder='Выберите дату ответа'";}?> />
            </div>
        </div>
        
        <div class="form-group">
            <label for="purchase_answer" class="col-xs-2 control-label">Ответ</label>
            <div class="col-xs-10">
                <input id="purchase_answer" name="purchase_answer" type="text" class="form-control" <?php if (isset($data['purchase_answer'])) {echo "value='".$data['purchase_answer']."'";} else {echo "placeholder='Введите ответ'";}?>/>
            </div>
        </div>
        
        <div class="form-group">
            <label for="purchase_price_delivery" class="col-xs-2 control-label">Стоимость доставки</label>
            <div class="col-xs-10">
                <input id="purchase_price_delivery" name="purchase_price_delivery" type="text" class="form-control" <?php if (isset($data['purchase_price_delivery'])) {echo "value='".$data['purchase_price_delivery']."'";} else {echo "placeholder='Введите стоимость доставки'";}?> />
            </div>
        </div>
        <div class="form-group">
            <label for="purchase_exportation_from" class="col-xs-2 control-label">Место забора</label>
            <div class="col-xs-10">
                <input id="purchase_exportation_from" name="purchase_exportation_from" type="text" class="form-control" <?php if (isset($data['purchase_exportation_from'])) {echo "value='".$data['purchase_exportation_from']."'";} else {echo "placeholder='Введите место забора'";}?> />
            </div>
        </div>
        <div class="form-group">
            <label for="purchase_margin" class="col-xs-2 control-label">Планируемая валовая прибыть</label>
            <div class="col-xs-10">
                <input id="purchase_margin" name="purchase_margin" type="text" class="form-control" <?php if (isset($data['purchase_margin'])) {echo "value='".$data['purchase_margin']."'";} else {echo "placeholder='Прибыль'";}?> />
            </div>
        </div>
        <div class="form-group">
            <label for="purchase_price_available_days" class="col-xs-2 control-label">Срок действия цены, дней</label>
            <div class="col-xs-10">
                <input id="purchase_price_available_days" name="purchase_price_available_days" type="text" class="form-control" <?php if (isset($data['purchase_price_available_days'])) {echo "value='".$data['purchase_price_available_days']."'";} else {echo "placeholder='Введите срок действия цены ( в днях)'";}?> />
            </div>
        </div>
    </div>
    <?php
    }
    if (in_array(5, $data2)) {
    ?>
    <div class="reg_sales">
        <h3>Отдел продаж</h3>
        <div class="form-group">
            <label for="sales_did_yn" class="col-xs-2 control-label">Сделка</label>
            <div class="col-xs-10">
                <select class="form-control" id="sales_did_yn" name = "sales_did_yn">
                    <option>пока нет</option>
                <?php 
                        foreach ($prodazi as $key=>$name) {
                            if ($key == $data['sales_did_yn']) {
                                echo "<option selected value=".$key.">".$name."</option>";
                            }
                            else {
                                echo "<option value=".$key.">".$name."</option>";
                            }
                        }
                    ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="sales_result" class="col-xs-2 control-label">Комментарий</label>
            <div class="col-xs-10">
                <input id="sales_result" name="sales_result" type="text" class="form-control" <?php if (isset($data['sales_result'])) {echo "value='".$data['sales_result']."'";} else {echo "placeholder='Введите комментарий'";}?> />
            </div>
        </div>
    </div>
    
    <?php
    }
    ?>
    
    
    
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button type="submit" class="btn btn-primary">Сохранить изменения</button>
    </div>
</form>

<script>
    $(document).ready(function(){

        $('#form .d').datetimepicker({
            format: 'dd.mm.yyyy hh:ii',
            language: 'ru'
        });
        /*$('#form .d').datepicker({
            format: "dd.mm.yyyy",
            todayBtn: "linked",
            clearBtn: true,
            language: "ru",
            beforeShowMonth: function (date){ if (date.getMonth() == 8) {return false; } }
        });*/
        var vdeistvie = $("select#deistvie").val();
        if (vdeistvie == "skidka") {
            $("#skidochka").css("display", "block");
            $("#price_neeeeed").css("display", "block");
        }
        else {
            $("#skidochka").css("display", "none");
            $("#price_neeeeed").css("display", "none");
        }
        var vprice_konkur = $("#price_konkur").val();
        if (vprice_konkur.length > 0) {
            $("#dostavochka").css("display", "block");
        }
        else {
            $("#dostavochka").css("display", "none");
        }
        var vuslovia_oplati = $("#uslovia_oplati").val();
        if (vuslovia_oplati == "otsrochka" || vuslovia_oplati == "inoe") {
            $("#dop_oplata").css("display", "block");
        }
        else {
            $("#dop_oplata").css("display", "none");
        }
        var vpurchase = $("#purchase").val();
        if (vpurchase == "no" || vpurchase == "alternativa") {
            $("#comment").css("display", "block");
        }
        else {
            $("#comment").css("display", "none");
        }
    });
    
    function check_it (){
        var volume = $("select#deistvie").val();
        if (volume == "skidka") {
            $("#skidochka").css("display", "block");
            $("#price_neeeeed").css("display", "block");
        }
        else {
            $("#skidochka").css("display", "none");
            $("#price_neeeeed").css("display", "none");
        }
    };
    
    function get_dostavka (){
        var volume = $("#price_konkur").val();
        if (volume.length > 0) {
            $("#dostavochka").css("display", "block");
        }
        else {
            $("#dostavochka").css("display", "none");
        }
    };

    function oplata (){
        var volume = $("#uslovia_oplati").val();
        if (volume == "otsrochka" || volume == "inoe") {
            $("#dop_oplata").css("display", "block");
        }
        else {
            $("#dop_oplata").css("display", "none");
        }
    };
    
    function get_comment (){
        var volume = $("#purchase").val();
        if (volume == "no" || volume == "alternativa") {
            $("#comment").css("display", "block");
        }
        else {
            $("#comment").css("display", "none");
        }
    };
</script>
