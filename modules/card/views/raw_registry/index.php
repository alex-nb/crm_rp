        <li role="presentation"><a href="../../../site/site/index">Назад</a></li>
        <li role="presentation"><a class='open_modal pointer'>Добавить запись</a></li>
</ul>
<?php
$stolbec1 = '<th colspan="5" class="reg_purchase" align="center" >Отдел снабжения</th>';
$stolbec2 = '';
$zak_log = false;
$zak = false;
if (in_array(3, $data2)) {
    $stolbec1 = '<th colspan="8" class="reg_purchase" align="center" >Отдел снабжения</th>';
    $stolbec2 = '<th class="reg_purchase" >Стоимость доставки</th><th class="reg_purchase">Место забора</th><th class="reg_purchase" >Планируемая валовая прибыль</th>';
    $zak = true;
    $zak_log = true;
}
else {
    if (in_array(6, $data2)) {
        $stolbec1 = '<th colspan="7" class="reg_purchase" align="center" >Отдел снабжения</th>';
        $stolbec2 = '<th class="reg_purchase" >Стоимость доставки</th><th class="reg_purchase">Место забора</th>';
        $zak_log = true;
    }
}

?>

<table class="table table-bordered table-condensed" id="table_reestr">
    <thead>
        <tr>
            <th rowspan="2"></th>
            <th rowspan="2">№</th>
            <th rowspan="2">№(Р)</th>
            <th rowspan="2">Инициатор</th>
            <th rowspan="2">Дата</th>
            <th rowspan="2">Срочность</th>
            <th rowspan="2">Продукт</th>
            <th rowspan="2">Примечание по продукту</th>
            <th rowspan="2" >Месячное потребление</th>
            <th rowspan="2">Необх. Объем</th>
            <th rowspan="2" >Область</th>
            <th rowspan="2" >Район</th>
            <th rowspan="2" >Адрес доставки</th>
            <th rowspan="2" >Название хозяйства</th>
            <th colspan="2" >Задача</th>
            <th rowspan="2" >Конкурент (название)</th>
            <th colspan="2" >Условия конкурента</th>
            <th colspan="2" >Оплата</th>
            <th rowspan="2" >Дать цену где</th>
            <th rowspan="2" >Цена</th>
            <th rowspan="2" >Примечание по задаче</th>
            <!--<th colspan="8" class='reg_purchase' align="center" >Отдел снабжения</th>-->
            <?=$stolbec1?>
            <th colspan="2" class='reg_sales' >Отдел продаж</th>
            <th rowspan="2" >Коммуникации</th>
        </tr>
        <tr>
            <th>Действие</th>
            <th>Вид скидки</th>
            <th>Цена</th>
            <th>Вид доставки</th>
            <th>Условия</th>
            <th>Дополнительно</th>
            <th class='reg_purchase' >Статус ответа</th>
            <th class='reg_purchase' >Комментарий от закупщика</th>
            <th class='reg_purchase' >Дата ответа</th>
            <th class='reg_purchase' >Ответ</th>
            <!-- <th class="reg_purchase" >Стоимость доставки</th>
            <th class="reg_purchase">Место забора</th>-->
            <?=$stolbec2?>
            <th class='reg_purchase' >Срок действия цены, дней</th>
            <th class='reg_sales' >Сделка</th>
            <th class='reg_sales' >Комментарий</th>
        </tr>
    </thead>
    <tbody>
<?php
if(!empty($data)){
    foreach (array_reverse($data) as $row){
        $sdelka = "-";
        $str=$str2 ='';
        $class= $class2 = '';
        $childe_class='';
        $name_cc='';
        $row['chat'] = nl2br($row['chat']);
        if( !empty($row['komment']) || 
            !empty($row['purchase_date_answer']) || 
            !empty($row['purchase_answer']) ||
            !empty($row['purchase_price_delivery']) ||
            !empty($row['purchase_exportation_from']) ||
            !empty($row['purchase_price_available_days']))
        {
            $str="class='reg_answer'";
            $class = "reg_answer";
        }
        if( $row['sales_did_yn'] == '1' || !empty($row['sales_result']) || $row['sales_did_yn'] == '2') {
            $str2="class='reg_answer'"; $class2 = "reg_answer"; }
        if (!empty($row['childe'])) {
            $childe_class = "class='reg_mainfields'"; $name_cc = "reg_mainfields"; }
        if ($row['urgency'] == 'h') { $urgency = "В течении 1 часа"; }
        else { 
            if ($row['urgency'] == '1d') { $urgency = "В течении 1 дня";}
            else { 
                if ($row['urgency'] == '3d') { $urgency = "В течении 3 дней"; }
                else { 
                    if ($row['urgency'] == '15m') {$urgency = "В течении 15 минут";}
                    else {
                        if ($row['urgency'] == '30m') {$urgency = "В течении 30 минут";}
                        else {$urgency = $row['urgency']; }
                    }
                }
            }
        }
        if ($row['deistvie'] == 'zapros') { $deistvie = "Запрос цены"; }
        else { 
            if ($row['deistvie'] == 'skidka') { $deistvie = "Необходимость скидки";}
            else { $deistvie = "-";}
        }
        if ($row['vid_skidki'] == 'loyal') { $vid_skidki = "На лояльность"; }
        else { 
            if ($row['vid_skidki'] == 'konk') { $vid_skidki = "Конкурент"; }
            else { $vid_skidki = "-";}
        }
        if ($row['price_dostavka'] == 'with') { $vid_dostavki = "С доставкой";}
        else { 
            if ($row['price_dostavka'] == 'whithout') { $vid_dostavki = "Без доставки";}
            else { $vid_dostavki = "-"; }
        }
        if ($row['uslovia_oplati'] == 'fact') { $uslovia_oplati = "По факту";}
        else { 
            if ($row['uslovia_oplati'] == 'predoplata') { $uslovia_oplati = "Предоплата"; }
            else { 
                if ($row['uslovia_oplati'] == 'otsrochka') { $uslovia_oplati = "Отсрочка"; }
                else { $uslovia_oplati = "Иное";}
            }
        }
        if ($row['task'] == 'sklad') { $where_price = "На складе РП";}
        else { 
            if ($row['task'] == 'dostavka') { $where_price = "С доcтавкой до потребителя со склада РП"; }
            else { 
                if ($row['task'] == 'postavshik') { $where_price = "Цена на складе у поставщика";}
                else { $where_price = $row['task'];}
            }
        }
        if ($row['purchase'] == 'yes') { $stat_answer = "Да";}
        else { 
            if ($row['purchase'] == 'no') { $stat_answer = "Нет"; }
            else { 
                if ($row['purchase'] == 'alternativa') { $stat_answer = "Альтернатива"; }
                else { $stat_answer = $row['purchase'] ;}
            }
        }
        if ($row['sales_did_yn'] == '1') { $sdelka = "Успешно";}
        else { if ($row['sales_did_yn'] == '2') { $sdelka = "Не успешно";}}

        echo "<tr title='{$row['id']} | {$row['who']} | {$row['product']} | {$row['bulk']}' id='{$row['id']}'>
                        <td>
                            <span class='glyphicon glyphicon-pencil pointer' data-toggle='tooltip' id='{$row['id']}' title='Редактировать'></span>
                            <span class='glyphicon glyphicon-list-alt pointer' data-toggle='tooltip' id='{$row['id']}' title='Копировать'></span>
                        </td>
                        <td {$childe_class}>{$row['id']}</td>
                        <td {$childe_class}>{$row['id']}</td>
                        <td {$str2}>{$row['who']}</td>
                        <td {$str2}>{$row['date_add']}</td>
                        <td {$str2}>{$urgency}</td>
                        <td class='{$class} little clip' >{$row['product']}</td>";
                        //<td class='{$class} little clip'>{$row['qualitative']}</td>
                        if (!empty($row['ssilka'])) {
                            echo "<td class='{$class} little clip'><a href='{$row['ssilka']}' target='_blank' style='color: black'><span class='glyphicon glyphicon-file pointer' data-toggle='tooltip' title='Файлик'></span></a>  {$row['qualitative']}</td>";
                        }
                        else {
                            echo "<td class='{$class} little clip'>{$row['qualitative']}</td>";
                        }
                        echo "<td class='little clip {$class}'>{$row['times_per_month']}</td>
                        <td {$str}>{$row['bulk']}</td>
                        <td {$str}>{$row['region']}</td>
                        <td {$str}>{$row['district']}</td>
                        <td class='little clip {$class}'>{$row['address_delivery']}</td>
                        <td {$str}>{$row['farm_name']}</td>
                        <td {$str}>{$deistvie}</td> 
                        <td {$str}>{$vid_skidki}</td>
                        <td {$str}>{$row['competitor_name']}</td>
                        <td {$str}>{$row['price_konkur']}</td>
                        <td {$str}>{$vid_dostavki}</td>
                        <td {$str}'>{$uslovia_oplati}</td>
                        <td {$str}'>{$row['dop_uslovia']}</td>
                        <td class='little clip {$class}'>{$where_price}</td>
                        <td class='{$class}'>{$row['price_need']}</td>
                        <td class='little clip {$class}'>{$row['notes']}</td>
                        <td class='reg_purchase' >{$stat_answer}</td>
                        <td class='reg_purchase' >{$row['komment']}</td>
                        <td class='reg_purchase' >{$row['purchase_date_answer']}</td>
                        <td class='reg_purchase little clip' >{$row['purchase_answer']}</td>";
        if ($zak_log) { echo "<td class='reg_purchase' >{$row['purchase_price_delivery']}</td>
                        <td class='reg_purchase' >{$row['purchase_exportation_from']}</td>"; }
        if ($zak) { echo "<td class='reg_purchase' >{$row['purchase_margin']}</td>"; }
        echo "  <td class='reg_purchase' >{$row['purchase_price_available_days']}</td>
                    <td class='reg_sales' >{$sdelka}</td>
                    <td class='reg_sales little clip' >{$row['sales_result']}</td>
                    <td>
                        <details style='width:300px;'>
                            <summary>Коммуникации</summary>
                            {$row['chat']}
                        </details>
                    </td>
                </tr>";
        if (!empty($row['childe'])) {
            foreach ($row['childe'] as $childe) {
                $sdelka = "-";
                $str=$str2 ='';
                $class= $class2 = '';
                if( !empty($childe['komment']) || !empty($childe['purchase_date_answer']) || !empty($childe['purchase_answer']) || !empty($childe['purchase_price_delivery']) ||
                    !empty($childe['purchase_exportation_from']) || !empty($childe['purchase_price_available_days'])) {
                        $str="class='reg_answer'"; $class = "reg_answer"; }
                if( $row['sales_did_yn'] == '1' || !empty($row['sales_result']) || $row['sales_did_yn'] == '2') {
                    $str2="class='reg_answer'";  $class2 = "reg_answer"; }

                if ($childe['urgency'] == 'h') { $urgency = "В течении 1 часа"; }
                else { 
                    if ($childe['urgency'] == '1d') { $urgency = "В течении 1 дня";}
                    else { 
                        if ($childe['urgency'] == '3d') { $urgency = "В течении 3 дней";}
                        else { 
                            if ($childe['urgency'] == '15m') {$urgency = "В течении 15 минут";}
                             else {
                                 if ($childe['urgency'] == '30m') {$urgency = "В течении 30 минут";}
                                 else {$urgency = $childe['urgency']; }
                             }
                        }
                      }
                   }
                   if ($childe['deistvie'] == 'zapros') {$deistvie = "Запрос цены";}
                   else { 
                       if ($childe['deistvie'] == 'skidka') { $deistvie = "Необходимость скидки";}
                       else { $deistvie = $childe['deistvie'];}
                   }
                   if ($childe['vid_skidki'] == 'loyal') { $vid_skidki = "На лояльность";}
                   else { 
                       if ($childe['vid_skidki'] == 'konk') { $vid_skidki = "Конкурент";}
                       else {  $vid_skidki = "-";}
                   }
                   if ($childe['price_dostavka'] == 'with') {$vid_dostavki = "С доставкой"; }
                   else { 
                       if ($childe['price_dostavka'] == 'whithout') { $vid_dostavki = "Без доставки";}
                       else { $vid_dostavki = "-"; }
                   }
                   if ($childe['uslovia_oplati'] == 'fact') {$uslovia_oplati = "По факту";}
                   else { 
                       if ($childe['uslovia_oplati'] == 'predoplata') { $uslovia_oplati = "Предоплата";}
                       else { 
                           if ($childe['uslovia_oplati'] == 'otsrochka') { $uslovia_oplati = "Отсрочка"; }
                           else { $uslovia_oplati = "Иное";}
                       }
                   }
                   if ($childe['task'] == 'sklad') { $where_price = "На складе РП"; }
                   else { 
                       if ($childe['task'] == 'dostavka') { $where_price = "С доcтавкой до потребителя со склада РП"; }
                       else { 
                           if ($childe['task'] == 'postavshik') { $where_price = "Цена на складе у поставщика"; }
                           else {  $where_price = $childe['task'];}
                       }
                   }
                    if ($childe['purchase'] == 'yes') { $stat_answer = "Да";}
                    else { 
                        if ($childe['purchase'] == 'no') { $stat_answer = "Нет"; }
                        else { 
                            if ($childe['purchase'] == 'alternativa') { $stat_answer = "Альтернатива"; }
                            else { $stat_answer = $childe['purchase'] ;}
                        }
                    }
                    if ($childe['sales_did_yn'] == '1') { $sdelka = "Успешно";}
                    if ($childe['sales_did_yn'] == '2') { $sdelka = "Не успешно";}

                echo "<tr title='{$childe['id']} | {$childe['who']} | {$childe['product']} | {$childe['bulk']}' id='{$childe['id']}'>
                        <td>
                            <span class='glyphicon glyphicon-pencil pointer' data-toggle='tooltip' id='{$childe['id']}' title='Редактировать'></span>
                            <span class='glyphicon glyphicon-list-alt pointer' data-toggle='tooltip' id='{$childe['id']}' title='Копировать'></span>
                        </td>
                        <td {$childe_class}>{$childe['id']}</td>
                            <td {$childe_class}>{$row['id']}</td>
                        <td {$str2}>{$childe['who']}</td>
                        <td {$str2}>{$childe['date_add']}</td>
                        <td {$str2}>{$urgency}</td>
                        <td class='{$class} little clip' >{$childe['product']}</td>";
                        //<td class='{$class} little clip'>{$childe['qualitative']}</td>
                        if (!empty($childe['ssilka'])) {
                            echo "<td class='{$class} little clip'><a href='{$childe['ssilka']}' target='_blank' style='color: black'><span class='glyphicon glyphicon-file pointer' data-toggle='tooltip' title='Файлик'></span></a>  {$childe['qualitative']}</td>";
                        }
                        else {
                            echo "<td class='{$class} little clip'>{$childe['qualitative']}</td>";
                        }
                        echo "<td class='little clip {$class}'>{$childe['times_per_month']}</td>
                        <td {$str}>{$childe['bulk']}</td>
                        <td {$str}>{$childe['region']}</td>
                        <td {$str}>{$childe['district']}</td>
                        <td class='little clip {$class}'>{$childe['address_delivery']}</td>
                        <td {$str}>{$childe['farm_name']}</td>
                        <td {$str}>{$deistvie}</td>
                        <td {$str}>{$vid_skidki}</td>
                        <td {$str}>{$childe['competitor_name']}</td>
                        <td {$str}>{$childe['price_konkur']}</td>
                        <td {$str}>{$vid_dostavki}</td>
                        <td {$str}'>{$uslovia_oplati}</td>
                        <td {$str}'>{$childe['dop_uslovia']}</td>
                        <td class='little clip {$class}'>{$where_price}</td>
                        <td class='{$class}'>{$childe['price_need']}</td>
                        <td class='little clip {$class}'>{$childe['notes']}</td>
                        <td class='reg_purchase' >{$stat_answer}</td>
                        <td class='reg_purchase' >{$childe['komment']}</td>
                        <td class='reg_purchase' >{$childe['purchase_date_answer']}</td>
                        <td class='reg_purchase little clip' >{$childe['purchase_answer']}</td>";
         if ($zak_log) { echo "<td class='reg_purchase' >{$childe['purchase_price_delivery']}</td>
                         <td class='reg_purchase' >{$childe['purchase_exportation_from']}</td>"; }
        if ($zak) { echo "<td class='reg_purchase' >{$childe['purchase_margin']}</td>"; } 
        echo "<td class='reg_purchase' >{$childe['purchase_price_available_days']}</td>
                    <td class='reg_sales' >{$sdelka}</td>
                    <td class='reg_sales little clip' >{$childe['sales_result']}</td>
                    <td>
                        <details style='width:300px;'>
                            <summary>Коммуникации</summary>
                            {$childe['chat']}
                        </details>
                    </td>
                </tr>";
            }
        }
    }#foreach
}#if empti
?>
</tbody>
</table>

<!-- HTML-код модального окна для создания/удаления-->
<div id="myModal" class="modal fade">
    <div class="modal-dialog modal-large" style="width:70%;" >
        <div class="modal-content">
        <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title"></h4>
            </div>
        <!-- Основное содержимое модального окна -->
            <div class="modal-body" id="modal-body">
            </div>
        </div>
    </div>
</div>

<script> 
$(document).ready(function(){    
   /* $('#myModal').on('show.bs.modal', function (event) {
        var form = document.getElementById('form');
        if (form !== null) {
            form.parentNode.removeChild(form);
        }
        // получить кнопку, которая его открыло
        var button = $(event.relatedTarget);
        var type = $(this).attr("id");
        console.log(type);
        // извлечь информацию из атрибута data-content
        var id_reg = button.data('content');
         $.ajax({
            //console.log("IN AJAX "); 
            url : "form",
            type: "POST",
            data: {id_reg:id_reg},
            success: function(html){
               $('#modal-body').append(html);
            }
        });   
    });*/
    
    $('.glyphicon-pencil').click(function() {
        $("#modal-body").html('');
        var id_reg = $(this).attr('id');
        $.ajax({
            url : "form_reg",
            type: "POST",
            data: {id_reg:id_reg},
            success: function(html){
               $('#modal-body').append(html);
               $('#myModal').modal('show');
            }
        });   
    });

    $('.glyphicon-list-alt').click(function() {
        $("#modal-body").html('');
        var id_reg = $(this).attr('id');
        var type = 'copy';
        $.ajax({
            url : "form_reg",
            type: "POST",
            data: {id_reg:id_reg,
                        type:type},
            success: function(html){
               $('#modal-body').append(html);
               $('#myModal').modal('show');
            }
        });   
    });

    $('.open_modal').click(function() {
        $("#modal-body").html('');
        $.ajax({
            url : "form_reg",
            type: "POST",
            data: {},
            success: function(html){
               $('#modal-body').append(html);
               $('#myModal').modal('show');
            }
        });   
    });

    $('#myModal').on('hidden.bs.modal', function () {
        $("#modal-body").html('');
    });

    $('td.little').click(function() {
        if ($(this).hasClass('clip')) {
            $(this).removeClass('clip');
        }
        else {
            $(this).addClass('clip');
        }
    });

    $('#table_reestr').DataTable({
        scrollY: '70vh',
        "scrollX": true,
        scrollCollapse: true,

        "order": [[ 2, 'desc' ]],
        "displayLength": 50,
        "language": {
            url: '//cdn.datatables.net/plug-ins/1.10.16/i18n/Russian.json'
        },
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
    });
});
</script>
