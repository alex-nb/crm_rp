<?php
/*
 * Модель, работающая с таблицей acl_action.
 * PK - id
 * Unique - title, description
 * Содержит список доступных действий с объектами
 * 1 - Чтение
 * 2 - Редактирование
 * 3 - Удаление
 * @author aleks
 */
class acl_actionModel extends Model{
   public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'title' => 'Наименование',
            'description' => 'Описание',
        );
    }
    public function checkFields(){
        return array(
            'title' => 'Наименование',
            'description' => 'Описание',
        );
    }
    public function constrainsTable() {
        return array (
            'acl_dissallow' => 'action_code',
            'acl_premission' => 'action_code',
        );
    }
}
