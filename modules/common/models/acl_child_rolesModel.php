<?php
/*
 * Модель, работающая с таблицей acl_child_roles.
 * PK - id
 * Unique - parrent_code, childe_code
 * Содержит наследуемые друг от друга роли
 * @author aleks
 */
class acl_child_rolesModel extends Model {
    public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'parrent_code' => 'ИД родителя',
            'childe_code' => 'ИД наследника',
        );
    }
    public function checkFields(){
        return array(
            'parrent_code' => 'ИД родителя',
            'childe_code' => 'ИД наследника',
        );
    }
    
    public function constrainsTable() {
        return NULL;
    }
}
