<?php
/*
 * Модель, работающая с таблицей acl_dissallow.
 * PK - id
 * Unique -  user_id, object_code, action_code
 * Содержит исключения для пользователей
 * @author aleks
 */
class acl_dissallowModel extends Model {
    public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'user_id' => 'ИД пользователя',
            'object_code' => 'ИД объекта',
            'action_code' => 'ИД действия',
        );
    }
    public function checkFields(){
        return array(
            'user_id' => 'ИД пользователя',
            'object_code' => 'ИД объекта',
            'action_code' => 'ИД действия',
        );
    }
    
    public function constrainsTable() {
        return NULL;
    }
}
