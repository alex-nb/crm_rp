<?php
/*
 * Модель, работающая с таблицей acl_action.
 * PK - id
 * Unique - title, description
 * Содержит список объектов и их описания.
 * @author aleks
 */
class acl_objectModel extends Model {
   public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'title' => 'Наименование',
            'description' => 'Описание',
        );
    }
    public function checkFields(){
        return array(
            'title' => 'Наименование',
            'description' => 'Описание',
        );
    }
    
    public function constrainsTable() {
        return array(
            'acl_dissallow' => 'object_code',
            'acl_premission' => 'object_code',
        );
    }
}
