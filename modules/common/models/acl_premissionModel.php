<?php
/*
 * Модель, работающая с таблицей acl_premission.
 * PK - id
 * Unique -  object_code, role_code, action_code
 * Содержит права доступа (объект+действие над ним) для каждой роли
 * @author aleks
 */
class acl_premissionModel extends Model {
    public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'object_code' => 'ИД объекта',
            'role_code' => 'ИД роли',
            'action_code' => 'ИД действия',
        );
    }
    public function checkFields(){
        return array(
            'object_code' => 'ИД объекта',
            'role_code' => 'ИД роли',
            'action_code' => 'ИД действия',
        );
    }
    
    public function constrainsTable() {
        return NULL;
    }
}
