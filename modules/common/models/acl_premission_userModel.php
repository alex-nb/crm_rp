<?php
/*
 * Модель, работающая с таблицей acl_premission_user.
 * PK - id
 * Unique -  user_id, role_id
 * Содержит список ролей для пользователя
 * @author aleks
 */
class acl_premission_userModel extends Model {
    public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'user_id' => 'ИД пользователя',
            'role_id' => 'ИД роли',
        );
    }
    public function checkFields(){
        return array(
            'user_id' => 'ИД пользователя',
            'role_id' => 'ИД роли',
        );
    }
    
    public function constrainsTable() {
        return NULL;
    }
}
