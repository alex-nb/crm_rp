<?php
/*
 * Модель, работающая с таблицей acl_roles.
 * PK - id
 * Unique - title, description
 * Содержит список ролей
 * @author aleks
 */
class acl_rolesModel extends Model {
   public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'title' => 'Наименование',
            'description' => 'Описание',
        );
    }
    public function checkFields(){
        return array(
            'title' => 'Наименование',
            'description' => 'Описание',
        );
    }
    
    public function constrainsTable() {
        return array(
            'acl_child_roles' => 'parrent_code',
            'acl_child_roles' => 'childe_code',
            'acl_premission' => 'role_code',
        );
    }
}
