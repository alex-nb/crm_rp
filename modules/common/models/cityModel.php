<?php
/*
 * Таблица с городами
 *
 * @author aleks
 */

class cityModel extends Model{
    public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'name' => 'Наименование',
        );
    }
    public function checkFields(){
        return array(
            'name' => 'Наименование',
        );
    }
    
    public function constrainsTable() {
        return array(
            'firms' => 'id_legal_city',
            'firms' => 'id_fact_city',
            'firms_stock' => 'id_city',
        );
    }
}
