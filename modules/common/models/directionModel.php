<?php
/*
 * Модель, работающая с таблицей direction.
 * PK - id
 * FK - id_parrent (production_direction)
 * Unique - name
 * Содержит направления продаж. Иерархическая структура.
 * @author aleks
 */
class directionModel extends Model {
   public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'name' => 'Наименование',
            'id_parrent' => 'ИД родителя',
        );
    }
    public function checkFields(){
        return array(
            'name' => 'Наименование',
        );
    }
    
    public function constrainsTable() {
        return array(
            'direction' => 'id_parrent',
            'firms_direction' => 'id_direction',
            'production_direction' => 'id_direction',
        );
    }
}
