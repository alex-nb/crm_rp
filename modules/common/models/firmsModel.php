<?php

/*
 * Модель, работающая с таблицей firms
 * PK - id
 * Содержит информацию о сторонних организациях (поставщики, клиенты и т.д.)
 * @author aleks
 */

class firmsModel extends Model{
    public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'name' => 'Наименование',
            'inn' => 'ИНН',
            'ogrn' => 'ОГРН',
            'id_legal_city' => 'ИД города юридического',
            'legal_address' => 'Адресс юридический',
            'id_fact_city' => 'ИД города фактического',
            'fact_address' => 'Адресс фактический',
        );
    }
    public function checkFields(){
        return array(
            'inn' => 'ИНН',
        );
    }
    
    public function constrainsTable() {
        return array(
            'firms_making_spec' => 'id_firm',
            'firms_equipment_spec' => 'id_firm',
            'firms_direction' => 'id_firm',
            'production_info' => 'id_firm',
            'firms_stock' => 'id_firm',
            'firms_out_emps' => 'id_firm',
            'firms_type_firm' => 'id_firm',
        );
    }
}
