<?php
/*
 * Модель, работающая с таблицей firms_direction.
 * PK - id_firm, id_direction
 * FK - id_firm (firms), id_direction (direction)
 * В значение id_direction записывается самое "младшее" по иерархической структуре направление для каждой
 * фирмы из таблицы firms.
 * @author aleks
 */
class firms_directionModel extends Model{
    public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'id_firm' => 'ИД фирмы',
            'id_direction' => 'ИД младшего направления',
        );
    }
    public function checkFields(){
        return array(
            'id_firm' => 'ИД фирмы',
            'id_direction' => 'ИД младшего направления',
        );
    }
    
    public function constrainsTable() {
        return NULL;
    }
}
