<?php
/*
 * Модель, работающая с таблицей firms_equipment.
 * PK - id
 * FK - id_parrent (firms_equipment)
 * Таблица содержит типы уровня оснащения компании в иерархической структуре.
 * @author aleks
 */
class firms_equipmentModel extends Model{
    public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'name' => 'Наименование',
            'id_parrent' => 'ИД родителя',
        );
    }
    public function checkFields(){
        return array(
            'name' => 'Наименование',
        );
    }
    
    public function constrainsTable() {
        return array(
            'firms_equipment' => 'id_parrent',
            'firms_equipment_spec' => 'id_equip',
        );
    }
}
