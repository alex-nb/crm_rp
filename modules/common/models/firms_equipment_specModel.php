<?php
/*
 * Модель, работающая с таблицей firms_equipment_spec.
 * PK - id_firm, id_equip
 * FK - id_firm (firms), id_equip (firms_equipment)
 * В значение id_equip записывается самое "младшее" по иерархической структуре оснащение для каждой
 * фирмы из таблицы firms.
 * @author aleks
 */
class firms_equipment_specModel extends Model{
    public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'id_firm' => 'ИД фирмы',
            'id_equip' => 'ИД оснащения',
        );
    }
    public function checkFields(){
        return array(
            'id_firm' => 'ИД фирмы',
            'id_equip' => 'ИД оснащения',
        );
    }
    
    public function constrainsTable() {
        return NULL;
    }
}
