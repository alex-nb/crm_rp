<?php
/*
 * Модель, работающая с таблицей firms_making
 * PK - id
 * FK - id_parrent (firms_making)
 * Содержит наименования производства для фирм в иерархической структуре.
 * @author aleks
 */
class firms_makingModel extends Model{
    public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'name' => 'Наименование',
            'id_parrent' => 'ИД родителя',
        );
    }
    public function checkFields(){
        return array(
            'name' => 'Наименование',
        );
    }
    
    public function constrainsTable() {
        return array(
            'firms_making' => 'id_parrent',
            'firms_making_spec' => 'id_make',
        );
    }
}
