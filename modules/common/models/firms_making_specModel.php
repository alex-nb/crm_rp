<?php
/*
 * Модель, работающая с таблицей firms_making_spec.
 * PK - id_firm, id_make
 * FK - id_firm (firms), id_make (firms_making)
 * В значение id_make записывается самое "младшее" по иерархической структуре производство для каждой
 * фирмы из таблицы firms.
 * @author aleks
 */
class firms_making_specModel extends Model{
    public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'id_firm' => 'ИД фирмы',
            'id_make' => 'ИД производства',
        );
    }
    public function checkFields(){
        return array(
            'id_firm' => 'ИД фирмы',
            'id_make' => 'ИД производства',
        );
    }
    
    public function constrainsTable() {
        return NULL;
    }
}

