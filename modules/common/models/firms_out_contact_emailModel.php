<?php
/*
 * Модель, работающая с таблицей firms_out_email.
 * PK - id
 * FK - id_out_emp (firms_out_emps)
 * Таблица содержит контактные данные всех сотрудников из сторонних организаций.
 * @author aleks
 */
class firms_out_contact_emailModel extends Model{
    public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'id_out_emp' => 'ИД сотрудника',
            'email' => 'Email',
        );
    }
    public function checkFields(){
        return array(
            'email' => 'Email',
        );
    }
    
    public function constrainsTable() {
        return NULL;
    }
}
