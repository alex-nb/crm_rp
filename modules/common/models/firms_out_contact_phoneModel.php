<?php
/*
 * Модель, работающая с таблицей firms_out_contact.
 * PK - id
 * FK - id_out_emp (firms_out_emps)
 * Таблица содержит контактные данные всех сотрудников из сторонних организаций.
 * @author aleks
 */
class firms_out_contact_phoneModel extends Model{
    public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'id_out_emp' => 'ИД сотрудника',
            'phone' => 'Телефон',
        );
    }
    public function checkFields(){
        return array(
            'phone' => 'Телефон',
        );
    }
    
    public function constrainsTable() {
        return NULL;
    }
}
