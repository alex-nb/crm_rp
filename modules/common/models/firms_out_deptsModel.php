<?php
/*
 * Модель, работающая с таблицей firms_out_depts.
 * PK - id
 * Таблица содержит все возможные отделы в сторонних организациях.
 * @author aleks
 */
class firms_out_deptsModel extends Model{
    public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'name' => 'Наименование',
        );
    }
    public function checkFields(){
        return array(
            'name' => 'Наименование',
        );
    }
    
    public function constrainsTable() {
        return array(
            'firms_out_emps' => 'id_out_dept',
        );
    }
}
