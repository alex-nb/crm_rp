<?php
/*
 * Модель, работающая с таблицей firms_out_emps.
 * PK - id
 * FK - id_out_pos (firms_out_position), id_firms (firms)
 * Таблица содержит контактные данные всех сотрудников из сторонних организаций.
 * @author aleks
 */
class firms_out_empsModel extends Model {
    public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'id_out_pos' => 'ИД должности',
            'id_out_dept' => 'ИД отдела',
            'id_firm' => 'ИД фирмы',
            'last_name' => 'Фамилия',
            'first_name' => 'Имя',
            'second_name' => 'Отчество',
            'phone' => 'Телефон',
            'email' => 'Email',
        );
    }
    public function checkFields(){
        return array(
        );
    }
    
    public function constrainsTable() {
        return NULL;
    }
    
    /*public function constrainsTable() {
        return array(
            'firms_out_contact_phone' => 'id_out_emp',
            'firms_out_contact_email' => 'id_out_emp',
        );
    }*/
}
