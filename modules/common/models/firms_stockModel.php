<?php
/*
 * Модель, работающая с таблицей firms_stock.
 * PK - id
 * FK - id_firm (firms), id_city (city)
 * Содержит адреса складов фирм.
 * @author aleks
 */
class firms_stockModel extends Model{
    public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'id_firm' => 'ИД фирмы',
            'id_city' => 'ИД города склада',
            'address' => 'Адресс склада',
        );
    }
    public function checkFields(){
        return array(
            'id_firm' => 'ИД фирмы',
            'id_city' => 'ИД города склада',
        );# Не уникальны в базе
    }
    
    public function constrainsTable() {
        return array(
            'production_place_out' => 'id_stock',
        );
    }
}
