<?php
/*
 * Модель, работающая с таблицей firms_type
 * PK - id
 * Содержит информацию о типах сторонних организаций (поставщики, клиенты и т.д.)
 * @author aleks
 */

class firms_typeModel extends Model{
    public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'name' => 'Наименование',
        );
    }
    public function checkFields(){
        return array(
            'name' => 'Наименование',
        );
    }
    
    public function constrainsTable() {
        return array(
            'firms_type_firm' => 'id_type',
        );
    }
}
