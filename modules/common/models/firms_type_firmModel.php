<?php
/*
 * Модель, работающая с таблицей firms_type_firm.
 * PK - id
 * FK - id_firm (firms), id_type (firms_type)
 * Таблица содержит тип контрагента для каждой организации
 * @author aleks
 */
class firms_type_firmModel extends Model{
    public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'id_firm' => 'ИД фирмы',
            'id_type' => 'ИД типа',
        );
    }
    public function checkFields(){
        return array(
            'id_firm' => 'ИД фирмы',
            'id_type' => 'ИД типа',
        );# Не уникальны в базе
    }
    
    public function constrainsTable() {
        return NULL;
    }
}
