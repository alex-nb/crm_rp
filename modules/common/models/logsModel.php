<?php
/*
 * Модель, работающая с таблицей logs
 * PK - id
 * Содержит информацию о создании/изменении полей в таблицах пользователями.
 * @author aleks
 */
class logsModel extends Model{
    public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'name_table' => 'Наименование таблицы',
            'id_user' => 'ИД пользователя',
            'action' => 'Действие',
            'id_in_table' => 'ИД изменяемого поля',
        );
    }
    public function checkFields(){
        return array(
            'id' => 'ИД',
        );
    }
    
    public function constrainsTable() {
        return NULL;
    }
}
