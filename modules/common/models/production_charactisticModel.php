<?php
/*
 * Модель, работающая с таблицей production_charactistic.
 * PK - id
 * Unique - name
 * Содержит наименования всех возможных характеристик для товара.
 * @author aleks
 */
class production_charactisticModel extends Model {
    public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'name' => 'Наименование',
            'poss_value' => 'Возможные значения',
        );
    }
    public function checkFields(){
        return array(
            'name' => 'Наименование',
        );
    }
    
    public function constrainsTable() {
        return array(
            'production_charactistic_group' => 'id_char',
            'production_info_char' => 'id_char',
        );
    }
}
