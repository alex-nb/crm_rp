<?php
/*
 * Модель, работающая с таблицей production_charactistic_group.
 * PK - id_prod, id_char
 * FK - id_prod (production_name), id_char (production_charactistic)
 * Unique - poss_value
 * Содержит id всех возможных значений для всех характеристик для каждого товара.
 * @author aleks
 */
class production_charactistic_groupModel extends Model{
    public function fieldsTable(){
        return array(
            'id_prod' => 'ИД товара',
            'id_char' => 'ИД характеристики',
        );
    }
    public function checkFields(){
        return array(
            'id_prod' => 'ИД товара',
            'id_char' => 'ИД характеристики',
        );
    }
    
    public function constrainsTable() {
        return NULL;
    }
}
