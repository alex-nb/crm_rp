<?php
/*
 * Модель, работающая с таблицей production_charactistic_value.
 * PK - id
 * Unique - name
 * Содержит наименования всех возможных значений для всех характеристик, предполагающий выбор.
 * @author aleks
 */
class production_charactistic_valueModel extends Model{
   public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'name' => 'Наименование',
        );
    }
    public function checkFields(){
        return array(
            'name' => 'Наименование',
        );
    }
    
    public function constrainsTable() {
        return NULL;
    }
}
