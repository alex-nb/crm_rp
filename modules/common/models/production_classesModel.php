<?php
/*
 * Модель, работающая с таблицей production_classes
 * PK - id
 * Unique - id_prod, id_class
 * Содержит наименования всех возможных характеристик для товара.
 * @author aleks
 */
class production_classesModel extends Model{
    public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'id_prod' => 'ИД товара',
            'id_class' => 'ИД класса',
        );
    }
    public function checkFields(){
        return array(
            'id_prod' => 'ИД товара',
            'id_class' => 'ИД класса',
        );
    }
    
    public function constrainsTable() {
        return NULL;
    }
}
