<?php
/*
 * Модель, работающая с таблицей production_direction.
 * PK - id
 * FK - id_prod (production_name), id_direction (production_direction)
 * В значение id_direction записывается самое "младшее" по иерархической структуре направление для каждого
 * товара из таблицы production_name.
 * @author aleks
 */
class production_directionModel extends Model {
    public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'id_prod' => 'ИД продукции',
            'id_direction' => 'ИД младшего направления',
        );
    }
    public function checkFields(){
        return array(
            'id_prod' => 'ИД продукции',
            'id_direction' => 'ИД младшего направления',
        );
    }
    
    public function constrainsTable() {
        return NULL;
    }
}
