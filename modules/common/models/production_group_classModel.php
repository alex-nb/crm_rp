<?php
/*
 * Модель, работающая с таблицей production_group_class.
 * PK - id
 * FK - id_parrent (production_group_class)
 * Содержит id всех возможных значений для всех характеристик для каждого товара.
 * @author aleks
 */
class production_group_classModel extends Model{
   public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'name' => 'Наименование',
            'id_parrent' => 'ИД родителя',
        );
    }
    public function checkFields(){
        return array(
            'name' => 'Наименование',
        );
    }
    
    public function constrainsTable() {
        return array(
            'production_group_class' => 'id_parrent',
            'production_classes' => 'id_class',
        );
    }
}
