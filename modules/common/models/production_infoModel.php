<?php
/*
 * Модель, работающая с таблицей production_info
 * PK - id_firm, id_prod
 * FK - id_firm (firms), id_prod (production_name), id_tare (production_tare), id_unit (production_unit)
 * Содержит базовую информацию о фасовке товара и его цены у разных поставщиков
 * @author aleks
 */

class production_infoModel extends Model {
    public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'id_firm' => 'ИД организации',
            'id_prod' => 'ИД товара',
            'id_tare' => 'ИД тары',
            'id_unit' => 'ИД единицы измерения',
            'value' => 'Объем/вес',
        );
    }
    public function checkFields(){
        return array(
            'id_firm' => 'ИД организации',
            'id_prod' => 'ИД товара',
            'id_tare' => 'ИД тары',
            'id_unit' => 'ИД единицы измерения',
            'value' => 'Объем/вес',
        );
    }
    
    public function constrainsTable() {
        return array(
            'production_place_out' => 'id_prod',
            'production_charactistic_group' => 'id_prod',
            'production_info_char' => 'id_prod',
            'request_production' => 'id_prod',
        );
    }
}
