<?php

/*
 * Модель, работающая с таблицей production_info_char.
 * PK - id, id_name, id_char. 
 * FK - id_char (production_charactistic), id_prod (production_info)
 * Cодержит id товара от поставщика, id его характеристики и значение этой характеристики.
 * В случае строгого ограничения - копируется name из production_charactistic_value, 
 * определяющийся через таблицу production_characteristic_group.
 * Если переменная доступна для свободного заполнения - значение содержит текст, введенный пользователем.
 * @author aleks
 */

class  production_info_charModel extends Model {
    public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'id_prod' => 'ИД товара',
            'id_char' => 'ИД характеристики',
            'value' => 'Значение',
        );
    }
    public function checkFields(){
        return array(
            'id_prod' => 'ИД товара',
            'id_char' => 'ИД характеристики',
        );
    }
    
    public function constrainsTable() {
        return NULL;
    }
}
