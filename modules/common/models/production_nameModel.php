<?php
/*
 * Модель, работающая с таблицей production_name.
 * PK - id
 * Unique - name
 * FK - id_class (production_group_class)
 * Содержит наименования всей продукции. 
 * @author aleks
 */
class production_nameModel extends Model {
   public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'name' => 'Наименование',
            'application' => 'Описание товара',
            'appearance' => 'Применение товара',
            'veterinary' => 'Ветеринарное свидетельство',
        );
    }
    public function checkFields(){
        return array(
            'name' => 'Наименование',
        );
    }
    
    public function constrainsTable() {
        return array(
            'production_info' => 'id_prod',
        );
    }
}
