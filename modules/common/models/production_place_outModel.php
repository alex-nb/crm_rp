<?php
/*
 * Модель, работающая с таблицей production_place_out
 * PK - id_firm, id_prod
 * FK - id_prod (production_info), id_stock (firms_stock)
 * Содержит информацию об адресах отгрузки для каждого товара у поставщиков
 * @author aleks
 */

class production_place_outModel extends Model{
    public function fieldsTable(){
        return array(
            'id_prod' => 'ИД товара',
            'id_stock' => 'ИД склада',
        );
    }
    public function checkFields(){
        return array(
            'id_prod' => 'ИД товара',
            'id_stock' => 'ИД склада',
        );
    }
    
    public function constrainsTable() {
        return NULL;
    }
}
