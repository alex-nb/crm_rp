<?php
/*
 * Модель, работающая с таблицей pproduction_tare.
 * PK - id
 * Unique - name, abbr
 * Содержит наименования и аббривиатуру для всех доступных тар для товаров.
 * @author aleks
 */
class production_tareModel extends Model {
   public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'abbr' => 'Аббривиатура',
            'name' => 'Наименование',
        );
    }
    public function checkFields(){
        return array(
            'name' => 'Наименование',
        );
    }
    
    public function constrainsTable() {
        return array(
            'production_info' => 'id_tare',
        );
    }
}
