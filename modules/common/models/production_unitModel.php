<?php
/*
 * Модель, работающая с таблицей production_unit.
 * PK - id
 * Unique - name, abbr
 * Содержит наименования и аббривиатуру для всех доступных едениц измерения товаров.
 * @author aleks
 */
class production_unitModel extends Model {
   public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'abbr' => 'Аббривиатура',
            'name' => 'Наименование',
        );
    }
    public function checkFields(){
        return array(
            'name' => 'Наименование',
        );
    }
    
    public function constrainsTable() {
        return array(
            'production_info' => 'id_unit',
        );
    }
}
