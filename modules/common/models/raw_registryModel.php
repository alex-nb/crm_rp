<?php

class raw_registryModel extends Model{
    public function fieldsTable(){
        return array(
            'id'  => 'ИД',
            'who' => 'Инициатор',
            'date_add' => 'Дата создания',
            'urgency'  => 'Срочность',
            'product' => 'Продукт',
            'qualitative' => 'Примечания по продукту',
            'ssilka' => 'Ссылка',
            'bulk' => 'Необх. Объем',
            'times_per_month' => 'Месячное потребление',
            'region' => 'Область',
            'district'  => 'Район',
            'address_delivery' => 'Адрес доставки',
            'farm_name' => 'Название хозяйства',
            //'s_to_take' => 'S от места забора',
            //'price_unit' => 'Цена прайсовая за единицу (в копейках)',
            //'price_delivery' => 'Стоимость доставки необходимого объема (в копейках)',
            //'price_delivery_per_unit' => 'Стоимость доставки за единицу (в копейках)',
            //'price_product_with_delivery_per_unit' => 'Стоимость продукции с доставкой за единицу (в копейках)',
            'deistvie' => 'Действие',
            'vid_skidki' => 'Вид скидки',
            //'discount_for_loyalty' => 'Необходимость скидки на лояльность',
            //'discount_for_competitor' => 'Необходимость скидки присутсвие конкурента',
            'competitor_name' => 'Конкурент (название)',
            'price_konkur' => 'Цена конкурента',
            'price_dostavka' => 'Вид доставки',
            //'competitor_conditions_price_per_unit' => 'Условия конкурента Цена за единицу (в копейках)',
            //'competitor_conditions_price_per_unit_with_delivery' => 'Условия конкурента Цена за единицу с доставкой (в копейках)',
            //'competitor_conditions_exportation' => 'Условия конкурента Самовывоз да/нет',
            //'competitor_conditions_exportation_from' => 'Условия конкурента Откуда везти',
            'uslovia_oplati' => 'Условия оплаты',
            'dop_uslovia' => 'Дополнительные условия',
            'task'  => 'Дать цену где',
            'price_need' => 'Цена',
            'notes'  => 'Примечание по задаче',
            'purchase' => 'Статус ответа',
            'komment' => 'Комментарий от закупщика',
            'purchase_date_answer' => 'Отдел снабжения - Дата ответа',
            'purchase_answer' => 'Отдел снабжения - Ответ',
            'purchase_price_delivery' => 'Отдел снабжения - Стоимость доставки (в копейках)',
            'purchase_exportation_from' => 'Отдел снабжения - Место забора',
            'purchase_margin' => 'Отдел снабжения - Планируемая валовая прибыль',
            'purchase_price_available_days'  => 'Отдел снабжения - Срок действия цены, дней',
            'sales_did_yn' => 'Отдел продаж - Сделка',
            'sales_result' => 'Отдел продаж - Комментарий',
            'chat' => 'Коммуникации отд. продаж и отд.снабжения',
            'id_parrent' => 'ИД родителя',
        );
    }
    public function checkFields(){
        return array(
            
        );
    }
    public function constrainsTable() {
        return NULL;
    }
}