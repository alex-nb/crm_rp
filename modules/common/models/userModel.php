<?php

/**
 * Description of User
 *
 * @author aleks
 */

class userModel extends Model {
    public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'username' => 'Логин',
            'email' => 'Емайл',
            'last_name' => 'Фамилия',
            'first_name' => 'Имя',
            'password' => 'Пароль',
            'second_name' => 'Отчество',
            'id_pos' => 'ИД должности',
            'id_dept' => 'ИД отдела',
            'sex' => 'Пол',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
            'token' => 'Ключ',
        );
    }
    public function checkFields(){
        return array(
            'last_name' => 'Фамилия',
            'first_name' => 'Имя',
            'second_name' => 'Отчество',
        );
    }
    public function constrainsTable() {
        return array(
            'acl_dissallow' => 'user_id',
            'acl_premission_user' => 'user_id',
        );
    }
}