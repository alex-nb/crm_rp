<?php

/**
 * Description of Dept
 *
 * @author aleks
 */
class user_deptModel extends Model {
    
    public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'name' => 'Наименование',
        );
    }
    public function checkFields(){
        return array(
            'name' => 'Наименование',
        );
    }
    
    public function constrainsTable() {
        return array(
            'user' =>'id_dept',
        );
    }
}
