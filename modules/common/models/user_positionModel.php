<?php

/**
 * Description of positionModel
 *
 * @author aleks
 */
class user_positionModel extends Model{
    public function fieldsTable(){
        return array(
            'id' => 'ИД',
            'name' => 'Наименование',
        );
    }
    public function checkFields(){
        return array(
            'name' => 'Наименование',
        );
    }
    
    public function constrainsTable() {
        return array(
            'user' => 'id_pos',
        );
    }
}
