<?php
require_once $_SERVER['DOCUMENT_ROOT']."/modules/common/models/userModel.php";

/**
 * Description of siteController
 *
 * @author aleks
 */
class siteController  extends Controller {
    function __construct() {
        parent::__construct();
        $this->model = new siteModel();
    }

    function index() {
            $this->view->generate('site','index.php', $this->userAuth);
    }
    
        function out() {
            $user = new userModel();
            $user->token = "1";
            $userAuth = $this->userAuth;
            $user->Update($userAuth['id']);
            header('Location: /site');
    }
    
}
